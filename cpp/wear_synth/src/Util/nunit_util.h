#pragma once

#if _MANAGED

namespace Util {
	namespace NUnit {

		// Simple yes/no dialog box wrapper
		bool UserQuestion(System::String^ question);

	} // namespace NUnit
} // namespace Util

#endif // _MANAGED