#include "precompile.h"
#include "traits.h"
#include "cvrt_matlab_opencv.h"

// Credit:
//
// The following image conversion code was adapted from downloads available at
// http://www.mathworks.co.uk/matlabcentral/fileexchange/20927
			
#pragma region HELPER FUNCTIONS

namespace Util { 
	namespace Convert {
		namespace Image { 

			// Convert grayscale mxArray* to IplImage*
			template<mxClassID CID>
			static IplImage* MxArray_IplImage_2d(const mxArray* const mxImgIn)
			{
				// Get type contained within mxArray
				typedef Util::Traits::MatlabArray<CID>::CT T;

				// Get dimensions
				const int M = static_cast<int>(mxGetM(mxImgIn));
				const int N = static_cast<int>(mxGetN(mxImgIn));

				// Create output OpenCV image
				IplImage* cvImgOut = cvCreateImage(cvSize(N, M), Util::Traits::MatlabArray<CID>::CV_DEPTH, 1);

				// Get pointers to image data
				T     *mxImgData = static_cast<T*>(mxGetData(mxImgIn));
				uchar *cvImgData = reinterpret_cast<uchar*>(cvImgOut->imageData);

				// Minimise dereferencing below by copying widthStep
				const int step = cvImgOut->widthStep;

				// Transpose Mex data into OpenCV data
				for (int x = 0; x < N; x++)
					for (int y = 0; y < M; y++)
						//*(T*)&cvImgData[y*step+x*sizeof(T)] = mxImgData[x*M+y];
						// Use memcpy to save from all that casting, just copy the damn memory!
						memcpy(&cvImgData[y*step+x*sizeof(T)], &mxImgData[x*M+y], sizeof(T));

				// Return OpenCV image
				return cvImgOut;
			}

			// Convert greyscale IplImage* to mxArray*
			template<int CV_TYPE>
			static mxArray* IplImage_MxArray_2d(const IplImage* const cvImgIn)
			{
				// Get Matlab class ID
				const mxClassID CID = Util::Traits::OpenCvImage<CV_TYPE>::CID;

				// Get mxArray element C type
				typedef Util::Traits::MatlabArray<CID>::CT T;

				// Get size
				CvSize size = cvGetSize(cvImgIn);

				// Create output OpenCV image
				mxArray *mxImgOut = mxCreateNumericMatrix(size.height, size.width, CID, mxREAL);

				// Get pointers to image data
				uchar *cvImgData = reinterpret_cast<uchar*>(cvImgIn->imageData);
				T     *mxImgData = static_cast<T*>(mxGetData(mxImgOut));

				// Minimise dereferencing below by copying widthStep
				const int step = cvImgIn->widthStep;

				// Transpose OpenCV data into Mex data
				for (int x = 0; x < size.width; x++)
					for (int y = 0; y < size.height; y++)
						//mxImgData[x*size.width+y] = *(T*)&cvImgData[y*step+x*sizeof(T)];
						// Use memcpy to save from all that casting, just copy the damn memory!
						memcpy(&mxImgData[x*size.width+y], &cvImgData[y*step+x*sizeof(T)], sizeof(T));

				// Return OpenCV image
				return mxImgOut;
			}
		}
	}
}

#pragma endregion

// Convert image from mxArray* to IplImage*
IplImage* Util::Convert::Image::ToIplImage(const mxArray* mxImgIn)
{
	const mxClassID MX_CID = mxGetClassID(mxImgIn);
	const mwSize MX_NDIM  = mxGetNumberOfDimensions(mxImgIn);

	if (MX_NDIM == 2)
	{
		switch (MX_CID)
		{
		case mxDOUBLE_CLASS:
			return MxArray_IplImage_2d<mxDOUBLE_CLASS>(mxImgIn);
		case mxSINGLE_CLASS:
			return MxArray_IplImage_2d<mxSINGLE_CLASS>(mxImgIn);
		case mxINT32_CLASS:
			return MxArray_IplImage_2d<mxINT32_CLASS>(mxImgIn);
		case mxINT16_CLASS:
			return MxArray_IplImage_2d<mxINT16_CLASS>(mxImgIn);
		case mxINT8_CLASS:
			return MxArray_IplImage_2d<mxINT8_CLASS>(mxImgIn);
		case mxUINT16_CLASS:
			return MxArray_IplImage_2d<mxUINT16_CLASS>(mxImgIn);
		case mxUINT8_CLASS:
			return MxArray_IplImage_2d<mxUINT8_CLASS>(mxImgIn);
		default:
			throw std::runtime_error("Image type not yet supported for conversion.");
		}
	}
	else
	{
		// TODO: Support RGB image images.
		throw std::runtime_error("Only greyscale images are currently supported for conversion.");
	}
}

// Convert image from mxArray* to IplImage*
mxArray* Util::Convert::Image::ToMxArray(const IplImage* cvImgIn)
{
	const int CV_TYPE = cvGetElemType(cvImgIn);

	switch (CV_TYPE)
	{
	case CV_64FC1:
		return IplImage_MxArray_2d<CV_64FC1>(cvImgIn);
	case CV_32FC1:
		return IplImage_MxArray_2d<CV_32FC1>(cvImgIn);
	case CV_32SC1:
		return IplImage_MxArray_2d<CV_32SC1>(cvImgIn);
	case CV_16SC1:
		return IplImage_MxArray_2d<CV_16SC1>(cvImgIn);
	case CV_16UC1:
		return IplImage_MxArray_2d<CV_16UC1>(cvImgIn);
	case CV_8UC1:
		return IplImage_MxArray_2d<CV_8UC1>(cvImgIn);
	case CV_8SC1:
		return IplImage_MxArray_2d<CV_8SC1>(cvImgIn);
	default:
		throw std::runtime_error("Image type not yet supported for conversion.");
	}
	// TODO: Support RGB image types.
}
