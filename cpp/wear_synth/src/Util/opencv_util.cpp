#include "precompile.h"
#include "opencv_util.h"
#include "types.h"

void Util::OpenCV::ShowImageModal(const std::string& title, const IplImage* img, const bool autoSize,
								  const int waitTime)
{
	cvNamedWindow(title.c_str(), ((autoSize) ? CV_WINDOW_AUTOSIZE : 0));
	cvShowImage(title.c_str(), img);
	cvWaitKey(waitTime);
	cvDestroyWindow(title.c_str());
}

