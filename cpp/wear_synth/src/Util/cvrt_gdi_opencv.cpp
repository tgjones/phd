#include "precompile.h"
#include "traits.h"
#include "cvrt_gdi_opencv.h"

#if _MANAGED
#pragma managed

using namespace System::Drawing;

#pragma region HELPER FUNCTIONS

namespace Util {
	namespace Convert {
		namespace Image {

			// Convert gresscale IplImage* to System::Drawing::Bitmap
			template<int CV_TYPE>
			static Bitmap^ IplImage_GdiBitmap_2d(const IplImage* cvImgIn)
			{
				// Create blank output image
				Bitmap^ gdiImgOut = gcnew Bitmap(cvImgIn->width, cvImgIn->height);

				// Lock pixels in system memory for direct access
				Imaging::BitmapData^ gdiBitmapData =
					gdiImgOut->LockBits(System::Drawing::Rectangle(0, 0, gdiImgOut->Width, gdiImgOut->Height),
					Imaging::ImageLockMode::WriteOnly, Util::Traits::OpenCvImage<CV_TYPE>::DOT_NET_TYPE);

				// Get pointers to image data
				uchar *gdiImgDat = reinterpret_cast<byte_t*>((gdiBitmapData->Scan0).ToPointer());
				uchar *cvImgDat   = reinterpret_cast<byte_t*>(cvImgIn->imageData);
				
				// Copy image data
				for (int x = 0; x < cvImgIn->width; x++)
					for (int y = 0; y < cvImgIn->height; y++)
						gdiImgDat[y * gdiBitmapData->Stride + x] = cvImgDat[y * cvImgIn->widthStep + x];

				// Unlock pixel data
				gdiImgOut->UnlockBits(gdiBitmapData);

				// Return new OpenCV image
				return gdiImgOut;
			}

			// Convert greyscale System::Drawing::Bitmap to IplImage*
			template<Imaging::PixelFormat DOT_NET_TYPE>
			static IplImage* GdiBitmap_IplImage_2d(Bitmap^% gdiImgIn)
			{
				// Create blank output image
				IplImage* cvImgOut = cvCreateImage(cvSize(gdiImgIn->Width, gdiImgIn->Height), 
					Util::Traits::ManagedType<DOT_NET_TYPE>::CV_DEPTH, 1);

				// Lock pixels in system memory for direct access
				Imaging::BitmapData^ gdiBitmapData = 
					gdiImgIn->LockBits(System::Drawing::Rectangle(0, 0, gdiImgIn->Width, gdiImgIn->Height), 
					Imaging::ImageLockMode::ReadOnly, DOT_NET_TYPE);

				// Get pointers to image data
				uchar *gdiImgDat = reinterpret_cast<byte_t*>((gdiBitmapData->Scan0).ToPointer());
				uchar *cvImgDat  = reinterpret_cast<byte_t*>(cvImgOut->imageData);

				// Copy image data
				for (int x = 0; x < gdiImgIn->Width; x++)
					for (int y = 0; y < gdiImgIn->Height; y++)
						cvImgDat[y * cvImgOut->widthStep + x] = gdiImgDat[y * gdiBitmapData->Stride + x];

				// Unlock pixel data
				gdiImgIn->UnlockBits(gdiBitmapData);

				// Return new OpenCV image
				return cvImgOut;
			}

		}
	}
}

#pragma endregion

// Convert IplImage* to System::Drawing::Bitmap^
Bitmap^ Util::Convert::Image::ToGdiBitmap(const IplImage* cvImgIn)
{
	const int CV_TYPE = cvGetElemType(cvImgIn);

	switch (CV_TYPE)
	{
	case CV_8UC1:
		return IplImage_GdiBitmap_2d<CV_8UC1>(cvImgIn);
	default:
		throw std::runtime_error("Only greyscale images are currently supported for conversion.");
	}
}

// Convert System::Drawing::Bitmap^ to IplImage*
IplImage* Util::Convert::Image::ToIplImage(Bitmap^% gdiImgIn)
{
	Imaging::PixelFormat DOT_NET_TYPE = gdiImgIn->PixelFormat;

	switch (DOT_NET_TYPE)
	{
	case Imaging::PixelFormat::Format8bppIndexed:
		return GdiBitmap_IplImage_2d<Imaging::PixelFormat::Format8bppIndexed>(gdiImgIn);
	default:
		throw std::runtime_error("Only greyscale images are currently supported for conversion.");
	}
}

#pragma unmanaged
#endif // _MANAGED
