#pragma once

// C Standard Library
#include <cstdlib>
#include <cstring>
#include <ctime>

// C++ Standard Library
#include <string>
#include <exception>
#include <stdexcept>
#include <iostream>
#include <sstream>

#if _MANAGED

// CLR Support Library
#include <msclr/marshal.h>
#include <msclr/marshal_cppstd.h>
using namespace msclr::interop;

// Unmanaged code default from now on
#pragma unmanaged

#endif // _MANAGED

// OpenCV
#include <opencv/cv.h>
#include <opencv/highgui.h>

// Matlab
#include <mex.h>
#include <engine.h>
