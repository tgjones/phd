#include "precompile.h"
#include "filesystem.h"

#include "MatlabEngine.h"

// Constructor starts Matlab engine
// --------------------------------
Util::Matlab::Engine::Engine(const std::string& initDir, bool visible)
{
	// Start wrapped engine (platform specific)
	#ifdef _WIN32
	if ((pEngine = engOpen(NULL)) == NULL)
	#else
	if ((pEngine = engOpen("\0")) == NULL)
	#endif
		throw std::runtime_error("Cannot startup Matlab engine.");

	#ifdef _DEBUG
	// Set debug variable inside engine
	this->SetBool("DEBUG", true);
	#endif

	// Set visibility of console window
	SetVisibility(visible);

	// Set init directory
	ChangeCurrentDir(initDir);
}

// Destructor stops Matlab engine
// ------------------------------
Util::Matlab::Engine::~Engine()
{
	if (engClose(pEngine) != 0)
		throw std::runtime_error("Cannot close down Matlab engine.");
}

// Copy constructor is needed to protect the wrapped Matlab engine pointer
// -----------------------------------------------------------------------
Util::Matlab::Engine::Engine(const Util::Matlab::Engine &)
{
	// TODO: Figure out a way of copying the workspace to allow copying.
	throw std::runtime_error("Copying of Matlab engine is not supported.");
}

// Evaluate command
// ----------------
void Util::Matlab::Engine::Eval(const std::string& cmd)
{
	// Set variable name for failure message
	const std::string FAIL_VAR = "EVAL_FAILURE";

	// Wrap command in try-catch to avoid hidden abort on error
	std::string fullCmd = "clear " + FAIL_VAR + ";try;" + cmd + ";catch e;" + FAIL_VAR + "=e.message;end;";

	// Execute command
	if (engEvalString(pEngine, fullCmd.c_str()) != 0)
		throw std::runtime_error("Fatal error evaluating Matlab command.");

	// Detect caught Matlab exception and rethrow as C++ exception
	if (this->Exists(FAIL_VAR))
		throw std::runtime_error(this->GetString(FAIL_VAR));
}

// Set general variable in workspace
// ---------------------------------
void Util::Matlab::Engine::Set(const std::string& name, mxArray *var)
{
	if (engPutVariable(pEngine, name.c_str(), var) != 0)
		throw std::runtime_error("Cannot put variable '" + name + "' into to Matlab workspace.");
}

// Get general variable from workspace
// -----------------------------------
mxArray* Util::Matlab::Engine::Get(const std::string& name)
{
	mxArray* var;
	if ((var = engGetVariable(pEngine, name.c_str())) == NULL)
		throw std::runtime_error("Cannot get variable '" + name + "' from Matlab workspace.");
	return var;
}

// Check if variable exists in workspace
// -------------------------------------
bool Util::Matlab::Engine::Exists(const std::string& name)
{
	return (engGetVariable(pEngine, name.c_str())) != NULL ? true : false;
}

// Set boolean variable to workspace 
// ---------------------------------
void Util::Matlab::Engine::SetBool(const std::string& name, bool var)
{
	this->Set(name, mxCreateLogicalScalar(var));
}

// Get boolean variable from workspace
// -----------------------------------
bool Util::Matlab::Engine::GetBool(const std::string& name)
{
	mxArray* mxVar = this->Get(name);
	if (mxIsLogicalScalar(mxVar))
		return mxIsLogicalScalarTrue(mxVar);
	else
		throw std::runtime_error("Variable '" + name + "' is not of Matlab type logical.");
}

// Set string variable from workspace
// ----------------------------------
void Util::Matlab::Engine::SetString(const std::string& name, std::string& var)
{
	this->Set(name, mxCreateString(var.c_str()));
}

// Get string variable from workspace
// ----------------------------------
std::string Util::Matlab::Engine::GetString(const std::string& name)
{
	mxArray* mxVar = this->Get(name);
	if (mxIsChar(mxVar))
		return std::string(mxArrayToString(mxVar));
	else
		throw std::runtime_error("Variable '" + name + "' is not of Matlab type char.");
}

// Check console visibility
// ------------------------
bool Util::Matlab::Engine::IsVisible()
{
	bool isVisible;
	if (engGetVisible(pEngine, &isVisible) != 0)
		throw std::runtime_error("Cannot get visibility of Matlab engine.");
	return isVisible;
}

// Set console visibility
// ----------------------
void Util::Matlab::Engine::SetVisibility(bool visibility)
{
	if (engSetVisible(pEngine, visibility) != 0)
		throw std::runtime_error("Cannot set visibility of Matlab engine.");
}

// Run Matlab script (simple command wrapper)
// ------------------------------------------
void Util::Matlab::Engine::RunScript(const std::string& scriptPath)
{	
	this->Eval("run('" + scriptPath + "')");
}

// Change current directory (simple command wrapper)
// ------------------------------------------
void Util::Matlab::Engine::ChangeCurrentDir(const std::string& dir)
{	
	this->Eval("cd('" + dir + "')");
}

// Add path (simple command wrapper)
// ------------------------------------------
void Util::Matlab::Engine::AddPath(const std::string& path)
{	
	this->Eval("addpath('" + path + "')");
}

// Reset workspace (simple command wrapper)
// ----------------------------------------
void Util::Matlab::Engine::Clear()
{
	this->Eval("clear all;close all;");
}