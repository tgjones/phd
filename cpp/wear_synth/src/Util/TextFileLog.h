#pragma once
#ifndef __RESEARCH_UTIL_LOGGING_TEXT_FILE_LOG_H__
#define __RESEARCH_UTIL_LOGGING_TEXT_FILE_LOG_H__

#include <string>
#include <map>
#include <iostream>

#include "Log.h"

namespace Util {
	namespace Logging {
		class TextFileLog
		{
		public:

			std::ostream Debug();
			std::ostream Info();
			std::ostream Warn();
			std::ostream Error();
			std::ostream Fatal();
		};
	}
}



#endif // __RESEARCH_UTIL_LOGGING_TEXT_FILE_LOG_H__
