#pragma once

#include <string>
#include <mex.h>
#include <engine.h>

namespace Util {
	namespace Matlab {

		// TODO: Managed methods

		class Engine
		{
		private:
			::engine *pEngine;

		public:

#ifdef _DEBUG
			// Debug default constructor shows console
			Engine(const std::string& initDir = ".", bool visible = true);
#else
			// Release default constructor hides console
			Engine(const std::string& initDir = ".", bool visible = false);
#endif
			// Destructor
			virtual ~Engine();

			// Copy constructor
			Engine(const Engine& old);

			// Evaluate Matlab command
			virtual void Eval(const std::string& cmd);

			// General variable functions
			// TODO: Convert these functions to use Util::Matlab::Array instead of mxArray
			virtual void Set(const std::string& name, mxArray *var);
			virtual mxArray* Get(const std::string& name);
			virtual bool Exists(const std::string& name);

			// Boolean variable wrapper functions
			virtual void SetBool(const std::string& name, bool var);
			virtual bool GetBool(const std::string& name);

			// String variable wrapper functions
			virtual void SetString(const std::string& name, std::string& var);
			virtual std::string GetString(const std::string& name);

			// TODO: Other types

			/*
			// Int8 numeric variable wrapper functions
			virtual void SetInt8(const std::string& name, const int8_t var);
			virtual int8_t GetInt8(const std::string& name);

			// Int16 numeric variable wrapper functions
			virtual void SetInt16(const std::string& name, const int16_t var);
			virtual int16_t GetInt16(const std::string& name);

			// Int32 numeric variable wrapper functions
			virtual void SetInt32(const std::string& name, const int32_t var);
			virtual int32_t GetInt32(const std::string& name);

			// Int64 numeric variable wrapper functions
			virtual void SetInt64(const std::string& name, const int64_t var);
			virtual int64_t GetInt64(const std::string& name);

			// Uint8 numeric variable wrapper functions
			virtual void SetUint8(const std::string& name, const uint8_t var);
			virtual uint8_t GetUint8(const std::string& name);

			// Uint16 numeric variable wrapper functions
			virtual void SetUint16(const std::string& name, const uint16_t var);
			virtual uint16_t GetUint16(const std::string& name);
	
			// Uint32 numeric variable wrapper functions
			virtual void SetUint32(const std::string& name, const uint32_t var);
			virtual uint32_t GetUint32(const std::string& name);

			// Uint64 numeric variable wrapper functions
			virtual void SetUint64(const std::string& name, const uint64_t var);
			virtual uint64_t GetUint64(const std::string& name);
			*/

			// Engine visibility
			virtual bool IsVisible();
			virtual void SetVisibility(bool visible);

			// Convenience Eval wrappers
			virtual void RunScript(const std::string& scriptPath); // 'run' command
			virtual void ChangeCurrentDir(const std::string& dir); // 'cd' command
			virtual void AddPath(const std::string& path);         // 'addpath' command
			virtual void Clear();								   // 'clear' command
		};
	}
}