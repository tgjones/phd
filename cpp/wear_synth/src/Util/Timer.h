#pragma once

#include <ctime>

namespace Util {
		
	class Timer
	{
	private:
		clock_t startTime;
		double	elapsed;

	public:
		inline Timer() : startTime(clock()), elapsed(0.0) { }
		double Last() const 
		{ 
			return elapsed; 
		}
		inline void Start() 
		{ 
			startTime = clock();
			elapsed = 0.0;
		}
		inline double Stop()
		{
			elapsed = (static_cast<double>(clock()) - startTime) / CLOCKS_PER_SEC;
			return elapsed;
		}
		inline static void Block(double seconds)
		{
			double blockStartTime = clock();
			while (clock() <= blockStartTime + (seconds * CLOCKS_PER_SEC));
		}
	};
}