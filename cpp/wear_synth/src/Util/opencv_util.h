#pragma once

#include <opencv/cv.h>
#include "traits.h"

namespace Util {
	namespace OpenCV {

		// Quick image popup
		void ShowImageModal(const std::string& title, const IplImage* img, const bool autoSize = true, 
			const int waitTime = 0);

	}
}

