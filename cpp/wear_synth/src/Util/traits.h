#pragma once

// Credit:
//
// This code was modified and extended from downloads available at
// http://www.mathworks.co.uk/matlabcentral/fileexchange/20927

#include <opencv/cv.h>
#include <mex.h>
#include "types.h"

namespace Util {
	namespace Traits {

		#pragma region NATIVE TYPE TRAITS

		/**
		 * NATIVE TYPE TRAITS 
		 */
		template<typename T>
		struct NativeType {};

		// FLOATING POINT TYPES
		 
		template<>
		struct NativeType<float64_t> {
			static const mxClassID CID = mxDOUBLE_CLASS;
		};

		template<>
		struct NativeType<const float64_t> {
			static const mxClassID CID = mxDOUBLE_CLASS;
		};

		template<>
		struct NativeType<float32_t> {
			static const mxClassID CID = mxSINGLE_CLASS;
		};

		template<>
		struct NativeType<const float32_t> {
			static const mxClassID CID = mxSINGLE_CLASS;
		};

		// UNSIGNED INTEGER TYPES 

		template<>
		struct NativeType<uint64_t> {
			static const mxClassID CID = mxUINT64_CLASS;
		};

		template<>
		struct NativeType<const uint64_t> {
			static const mxClassID CID = mxUINT64_CLASS;
		};

		template<>
		struct NativeType<uint32_t> {
			static const mxClassID CID = mxUINT32_CLASS;
		};

		template<>
		struct NativeType<const uint32_t> {
			static const mxClassID CID = mxUINT32_CLASS;
		};

		template<>
		struct NativeType<uint16_t> {
			static const mxClassID CID = mxUINT16_CLASS;
		};

		template<>
		struct NativeType<const uint16_t> {
			static const mxClassID CID = mxUINT16_CLASS;
		};

		template<>
		struct NativeType<uint8_t> {
			static const mxClassID CID = mxUINT8_CLASS;
		};

		template<>
		struct NativeType<const uint8_t> {
			static const mxClassID CID = mxINT8_CLASS;
		};

		// SINGED INTEGER TYPES

		template<>
		struct NativeType<int64_t> {
			static const mxClassID CID = mxINT64_CLASS;
		};

		template<>
		struct NativeType<const int64_t> {
			static const mxClassID CID = mxINT64_CLASS;
		};

		template<>
		struct NativeType<int32_t> {
			static const mxClassID CID = mxINT32_CLASS;
		};

		template<>
		struct NativeType<const int32_t> {
			static const mxClassID CID = mxINT32_CLASS;
		};

		template<>
		struct NativeType<int16_t> {
			static const mxClassID CID = mxINT16_CLASS;
		};

		template<>
		struct NativeType<const int16_t> {
			static const mxClassID CID = mxINT16_CLASS;
		};

		template<>
		struct NativeType<int8_t> {
			static const mxClassID CID = mxINT8_CLASS;
		};

		template<>
		struct NativeType<const int8_t> {
			static const mxClassID CID = mxINT8_CLASS;
		};

		// OTHER TYPES
		#ifdef _NATIVE_WCHAR_T_DEFINED

		template<>
		struct NativeType<wchar_t> {
			static const mxClassID CID = mxCHAR_CLASS;
		};

		template<>
		struct NativeType<const wchar_t> {
		  static const mxClassID CID = mxCHAR_CLASS;
		};

		#endif // _NATIVE_WCHAR_T_DEFINED
		#pragma endregion

		#pragma region MANAGED TYPE TRAITS
		#if _MANAGED
		/**
		 * MANAGED TYPE TRAITS
		 */

		// TODO: More managed traits!

		template<System::Drawing::Imaging::PixelFormat>
		struct ManagedType {};

		template<>
		struct ManagedType<System::Drawing::Imaging::PixelFormat::Format8bppIndexed> {
			static const int CV_DEPTH = IPL_DEPTH_8U;
		};

		#endif // _MANAGED
		#pragma endregion

		#pragma region MATLAB ARRAY TRAITS
		/**
		 * MATLAB ARRAY TRAITS
		 */

		template<mxClassID T>
		struct MatlabArray {};

		// FLOATING POINT TYPES

		template<>
		struct MatlabArray<mxDOUBLE_CLASS> {
			typedef float64_t CT;
			static const int CV_DEPTH = IPL_DEPTH_64F;
		};

		template<>
		struct MatlabArray<mxSINGLE_CLASS> {
			typedef float32_t CT;
			static const int CV_DEPTH = IPL_DEPTH_32F;
		};

		// UNSINGED INTEGER TYPES

		template<>
		struct MatlabArray<mxUINT64_CLASS> {
			typedef uint64_t CT;
		};

		template<>
		struct MatlabArray<mxUINT32_CLASS> {
			typedef uint32_t CT;
		};

		template<>
		struct MatlabArray<mxUINT16_CLASS> {
			typedef uint16_t CT;
			static const int CV_DEPTH = IPL_DEPTH_16U;
		};

		template<>
		struct MatlabArray<mxUINT8_CLASS> {
			typedef uint8_t CT;
			static const int CV_DEPTH = IPL_DEPTH_8U;
		};

		// SIGNED INTEGER TYPES

		template<>
		struct MatlabArray<mxINT8_CLASS> {
			typedef int8_t CT;
			static const int CV_DEPTH = IPL_DEPTH_8S;
		};

		template<>
		struct MatlabArray<mxINT16_CLASS> {
			typedef int16_t CT;
			static const int CV_DEPTH = IPL_DEPTH_16S;
		};

		template<>
		struct MatlabArray<mxINT32_CLASS> {
			typedef int32_t CT;
			static const int CV_DEPTH = IPL_DEPTH_32S;
		};

		template<>
		struct MatlabArray<mxINT64_CLASS> {
		  typedef uint64_t CT;
		};

		#pragma endregion

		#pragma region OPENCV IMAGETRAITS
		/**
		 * OPENCV IMAGE TRAITS
		 */

		template<int>
		struct OpenCvImage {};

		template<>
		struct OpenCvImage<IPL_DEPTH_64F> {
			typedef float64_t CT;
			static const mxClassID CID = mxDOUBLE_CLASS;
		};

		template<>
		struct OpenCvImage<IPL_DEPTH_32F> {
			typedef float32_t CT;
			static const mxClassID CID = mxSINGLE_CLASS;
		};

		template<>
		struct OpenCvImage<IPL_DEPTH_8U> {
			typedef uint8_t CT;
			static const mxClassID CID = mxUINT8_CLASS;
		};

		/**
		 * Traits used to map OpenCV types to Matlab classes and OpenCV depths.
		 */

		template<>
		struct OpenCvImage<CV_64FC1> {
			static const mxClassID CID = mxDOUBLE_CLASS;
			static const int CV_DEPTH  = IPL_DEPTH_64F;
		};

		template<>
		struct OpenCvImage<CV_32FC1> {
			static const mxClassID CID = mxSINGLE_CLASS;
			static const int CV_DEPTH  = IPL_DEPTH_32F;
		};

		template<>
		struct OpenCvImage<CV_32SC1> {
			static const mxClassID CID = mxINT32_CLASS;
			static const int CV_DEPTH  = IPL_DEPTH_32S;
		};

		template<>
		struct OpenCvImage<CV_16SC1> {
			static const mxClassID CID = mxINT16_CLASS;
			static const int CV_DEPTH  = IPL_DEPTH_16S;
		};

		template<>
		struct OpenCvImage<CV_16UC1> {
			static const mxClassID CID = mxUINT16_CLASS;
			static const int CV_DEPTH  = IPL_DEPTH_16U;
		};

		template<>
		struct OpenCvImage<CV_8SC1> {
			static const mxClassID CID = mxINT8_CLASS;
			static const int CV_DEPTH  = IPL_DEPTH_8S;
		};

		template<>
		struct OpenCvImage<CV_8UC1> {
			static const mxClassID CID = mxUINT8_CLASS;
			static const int CV_DEPTH  = IPL_DEPTH_8U;
		#if _MANAGED
			static const System::Drawing::Imaging::PixelFormat DOT_NET_TYPE
				= System::Drawing::Imaging::PixelFormat::Format8bppIndexed;
		#endif
		};

		#pragma endregion

	}
}