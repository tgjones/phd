#pragma once

namespace Util {
	namespace Filesystem {

		#if _WIN32
		const char DIR_SEP = '\\';
		#else
		const char DIR_SEP = '/';
		#endif

		std::string GetFilename(const std::string& fullPath, bool stripExt = false);
		std::string GetDirectory(const std::string& fullPath);

	}
}