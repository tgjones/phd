#pragma once

#include <opencv/cv.h>

#if _MANAGED

namespace Util {
	namespace Convert {
		namespace Image {

			// Convert IplImage* to System::Drawing::Bitmap^
			System::Drawing::Bitmap^ ToGdiBitmap(const IplImage* cvImgIn);

			// Convert System::Drawing::Bitmap^ to IplImage*
			IplImage* ToIplImage(System::Drawing::Bitmap^% gdiImgIn);

		}
	}
}

#endif // _MANAGED
