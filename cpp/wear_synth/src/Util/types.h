#pragma once

// Floating point aliases
typedef float  float32_t;
typedef double float64_t;

// Signed integer size aliases
typedef signed char      int8_t;
typedef signed short     int16_t;
typedef signed long      int32_t;
typedef signed long long int64_t;

// Unsigned shorthand types
typedef unsigned char      uchar_t;
typedef unsigned short     ushort_t;
typedef unsigned int       uint_t;
typedef unsigned long      ulong_t;
typedef unsigned long long ulonglong_t;

// Unsigned integer size aliases
typedef uchar_t     uint8_t;
typedef ushort_t    uint16_t;
typedef ulong_t     uint32_t;
typedef ulonglong_t uint64_t;

// Memory block sizes
typedef uint8_t  byte_t;
typedef uint16_t word_t;
typedef uint32_t dword_t;
typedef uint64_t qword_t;
