/**
 * This file provides a wrapper for the mexFunction MEX entry point, allowing any function,
 * with the correct signature, to be used as the entry point instead.  This also means the entry
 * point can be referenced in an external header file and therefore included and ran by code
 * outside of Matlab.
 */
#pragma once

#include <exception>

#ifndef MEX_MAIN
#error "MEX_MAIN must be defined as the name of the Mex entry point before including mex_stub.h."
#endif

// Convert define to function pointer for parameter checking at compile-time
static void (*mex_main)(int, mxArray*[], int, const mxArray*[]) = &MEX_MAIN;

// Wrap function in default mex entry point and try-catch block
inline void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	// Run Mex function
	try 
	{
		#ifdef _DEBUG
		mexPrintf("Performance warning: This function has been compiled without optimizations.\n");
		#endif

		(*mex_main)(nlhs, plhs, nrhs, prhs); 
	}

	// Base class exception catch
	catch(std::exception const &e) 
	{ 
		mexErrMsgTxt(e.what()); 
	}

	// Catch all
	catch(...) 
	{ 
		mexErrMsgTxt("Unspecified failure.");	
	}
}