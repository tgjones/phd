#pragma once

// Utility classes
#include "MatlabEngine.h"
#include "MatlabArray.h"

// Utility functions
namespace Util {
	namespace Matlab {
		namespace Mex {

			// Check parameter count
			void CheckParameterCount(int nlhs, int nrhs, int min_nlhs, int min_nrhs);

		}
	}
}