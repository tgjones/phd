#include "precompile.h"
#include "filesystem.h"

std::string Util::Filesystem::GetFilename(const std::string& fullPath, bool stripExt) 
{
	// TODO: Error checking and exception throwing!

	std::string filename = "";

	size_t i = fullPath.rfind(DIR_SEP, fullPath.length());
	if (i != std::string::npos)
		filename = fullPath.substr(i + 1, fullPath.length() - i);

	if (stripExt)
	{
		i = filename.rfind('.', filename.length());
		if (i != std::string::npos)
			filename = filename.substr(0, filename.length() - (filename.length() - i));
	}

	return filename;
}

std::string Util::Filesystem::GetDirectory(const std::string& fullPath)
{
	// TODO: Error checking and exception throwing!
	std::string directory;

	size_t i = fullPath.rfind(DIR_SEP, fullPath.length());
	if (i != std::string::npos)
		directory = fullPath.substr(0, i);

	return directory;
}