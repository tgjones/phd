#pragma once

#include <mex.h>

namespace Util {
	namespace Matlab {

		/**
		 * A wrapper class to simplify access to mxArray.
		 * NOT FINISHED!
		 */
		template<typename T>
		class Array
		{
		private:

			// Member variables
			mxArray *m_ArrPtr;  // Pointer to wrapped mxArray
			T       *m_DataPtr; // Pointer to matrix data
			size_t  m_M, m_N;   // Matrix dimensions
			bool    m_delete;   // Flag indicating if memory is managed

		public:

			// TODO: Default constructor

			// TODO: Copy constructor

			// TODO: Assignment operator

			// TODO: const mxArray& copy constructor

			// mxArray* constructor
			Array(mxArray* arr) :
				m_ArrPtr(arr), m_DataPtr(static_cast<T*>(mxGetData(arr))),
				m_M(mxGetM(arr)), m_N(mxGetN(arr)), m_delete(false) { }

			// const mxArray* constructor
			Array(const mxArray* arr) :
				m_ArrPtr(const_cast<mxArray*>(arr)), 
				m_DataPtr(static_cast<T*>(mxGetData(arr))),
				m_M(mxGetM(arr)), m_N(mxGetN(arr)), m_delete(false) { }

			// Destructor
			virtual ~Array() 
			{
				if ((m_delete) && (m_ArrPtr != NULL)) mxFree(m_ArrPtr);

				// Might not need 'cause deleted by mxFree above?
				//if (m_DataPtr != NULL) free(m_DataPtr);
			}

			// mxArray pointer accessor
			mxArray* getArrPtr() const { return m_ArrPtr; }

			// Data pointer getter
			T* getDataPtr() const { return m_DataPtr; }

			// M getter
			size_t getM() const { return m_M; }

			// N getter
			size_t getN() const { return m_N; }

			// Element getter
			T getElement(size_t M, size_t N) const 
			{	
				return m_DataPtr[(N - 1) * m_M + (M - 1)];
			}
			 
			// Element setter
			void setElement(size_t M, size_t N, T value)
			{
				m_DataPtr[(N - 1) * m_M + (M - 1)] = value;
			}

			// All matrix setter
			void setAll(T value)
			{
				memset(m_DataPtr, value, m * n * sizeof(T));
			}
		};
	}
}