#pragma once

#include <sstream>

// General purpose classes
#include "Singleton.h"
#include "Timer.h"

// General purpose functions
namespace Util {
	namespace Convert {

		// Convert anything to a C++ string
		template<typename T>
		std::string ToString(const T var)
		{
			std::ostringstream oss;
			oss << var;
			return oss.str(); 
		}
	}
}