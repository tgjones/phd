#include "precompile.h"
#include "matlab_util.h"
#include "nunit_util.h"

#if _MANAGED
#pragma managed

using namespace NUnit::Framework;
using namespace System::Windows::Forms;

bool Util::NUnit::UserQuestion(System::String^ question)
{
	return (MessageBox::Show(question, "Test Assertion", 
		MessageBoxButtons::YesNo, MessageBoxIcon::Question) == DialogResult::Yes);
}

#pragma unmanaged
#endif // _MANAGED