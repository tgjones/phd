#include "precompile.h"
#include "general_util.h"
#include "matlab_util.h"

void Util::Matlab::Mex::CheckParameterCount(int nlhs, int nrhs, int min_nlhs, int min_nrhs)
{
	// Check input parameters
	if (nrhs < min_nrhs)
	{
		std::string msg = "Not enough input parameters, expected at least ";
		msg += Util::Convert::ToString(min_nrhs);
		msg += "."; 
		throw std::runtime_error(msg);
	}

	// Check output parameters
	if (nlhs < min_nlhs)
	{
		std::string msg = "Not enough output parameters, expected at least ";
		msg += Util::Convert::ToString(min_nlhs);
		throw std::runtime_error(msg);
	}
}