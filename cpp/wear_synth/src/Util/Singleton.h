#pragma once

namespace Util {
	template <typename T>
	class Singleton
	{
	private:
		static T& instance;
	protected:
		Singleton() {}
	public:
		static T& Instance() { return instance; }
	};
}