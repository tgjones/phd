#pragma once

#include <mex.h>
#include <opencv/cv.h>

namespace Util {
	namespace Convert {
		namespace Image {

			// Convert image from mxArray* to IplImage*
			IplImage* ToIplImage(const mxArray* mxImgIn);

			// Convert image from IplImage* to mxArray*
			mxArray* ToMxArray(const IplImage* cvImgIn);

		}
	}
}