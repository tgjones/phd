#pragma once

#if _MANAGED

namespace SimulateShoeWear {

	// Managed class which provides access to algorithms to .NET consumers.
	public ref class ClrWrapper
	{
	public:

		// Algorithms::SimulateShoeWear wrapper
		static System::Drawing::Bitmap^ ShoeWearForward(System::Drawing::Bitmap^ refImg, 
			System::Drawing::Bitmap^ pressureMap, System::UInt32^ steps);

		// Algorithms::SimulateShoeWear wrapper
		//static System::Drawing::Bitmap^ ShoeWearReverse(System::Drawing::Bitmap^ refImg, 
		//	System::Drawing::Bitmap^ pressureMap, System::UInt32^ steps);

	};
}

#endif // _MANAGED
