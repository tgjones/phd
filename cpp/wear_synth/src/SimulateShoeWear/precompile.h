#pragma once

// C Standard Library
#include <cmath>

// C++ Standard Library
#include <string>
#include <vector>
#include <set>

// Unmanaged code default from now on
#if _MANAGED
#pragma unmanaged
#endif

// OpenCV
#include <opencv/cv.h>