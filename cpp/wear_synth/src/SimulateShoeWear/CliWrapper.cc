#include "precompile.h"
#include "../UtilCPP/cvrt_gdi_opencv.h"
#include "../Algorithms/simulate_shoe_wear.h"
#include "CliWrapper.h"

#if _MANAGED

using namespace System::Drawing;
using namespace Util::Convert::Image;

Bitmap^ Algorithms::CliWrapper::SimulateShoeWear(Bitmap^ refImg, Bitmap^ pressureMap, System::UInt64^ steps)
{
	// Convert input images
	IplImage* cvRefImg      = ToIplImage(refImg);
	IplImage* cvPressureMap = ToIplImage(pressureMap);

	// Run native algorithm
	IplImage* cvWornImg = Algorithms::SimulateShoeWear(cvRefImg, cvPressureMap, static_cast<long>(*steps));

	// Convert output image
	Bitmap^ wornImg = ToGdiBitmap(cvWornImg);

	// Free IplImage pointers
	cvReleaseImage(&cvRefImg);
	cvReleaseImage(&cvPressureMap);
	cvReleaseImage(&cvWornImg);

	// Return worn image
	return wornImg;
}

#endif // _MANAGED
