#pragma once

#include <opencv/cv.h>
#include "../Util/types.h"
#include "../Util/dll_export.h"

namespace SimulateShoeWear {

	// Shared method prototype
	extern "C" DLL_EXPORT IplImage* ShoeWearBackward(IplImage *wornImgIn, IplImage *pressureMapIn, 
		const uint32_t timeBlocks);

}