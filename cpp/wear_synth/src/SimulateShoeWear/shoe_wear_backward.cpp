#include "precompile.h"
#include "../Util/general_util.h"
#include "../Util/opencv_util.h"
#include "shoe_wear_backward.h"

// Constants
static const uint32_t STREL_WIDTH = 3;
static const uint32_t STREL_HEIGHT = 5;
static const uint32_t STREL_ANCHOR_X = 2;
static const uint32_t STREL_ANCHOR_Y = 3;
static const uint32_t PRESSURE_ITERATIONS = 3;

// Macro for outputting timeblock message in debugging or demo
#if defined (_DEBUG)
#define BLOCK_PASS_MSG(i,j) std::string("[ Time Block: " + Util::Convert::ToString(i+1) + " | Wear Pass: " + Util::Convert::ToString(PRESSURE_ITERATIONS+1-j) + " ]")
#elif defined (_DEMO)
#define BLOCK_PASS_MSG(i,j) std::string("[ Time Block: " + Util::Convert::ToString(i+1) + " ]")
#endif

// Algorithm
IplImage* SimulateShoeWear::ShoeWearBackward(IplImage *wornImgIn, IplImage *pressureMapIn, const uint32_t timeBlocks)
{
#ifdef _DEBUG
	Util::OpenCV::ShowImageModal("Worn image input", wornImgIn);
	Util::OpenCV::ShowImageModal("Pressure map input", pressureMapIn);
#endif

	// Reusable variables
	uint32_t i, j, x, y;
	uint8_t *row_ptr_8u_a/*, *row_ptr_8u_b*/;
	float32_t *row_ptr_32f_a, *row_ptr_32f_b;

	// Convert reference image from integer to floating point if needed
	IplImage* wornImg = cvCreateImage(cvSize(wornImgIn->width, wornImgIn->height), IPL_DEPTH_32F, 1);
	if (wornImgIn->depth == IPL_DEPTH_8U)
	{
		// Convert to 32-bit floating point
		for (y = 0; y < static_cast<uint32_t>(wornImgIn->height); y++)
		{
			row_ptr_8u_a  = reinterpret_cast<uint8_t*>(wornImgIn->imageData + y * wornImgIn->widthStep);
			row_ptr_32f_a = reinterpret_cast<float32_t*>(wornImg->imageData + y * wornImg->widthStep);
			for (x = 0; x < static_cast<uint32_t>(wornImg->width); x++)
				row_ptr_32f_a[x] = static_cast<float32_t>(row_ptr_8u_a[x]) / 255;
		}
	}
	else wornImg = cvCloneImage(wornImgIn);
	// TODO: Simplify with cvConvert(wornImgIn/255, wornImg)

#ifdef _DEBUG
	Util::OpenCV::ShowImageModal("Worn image converted to FP", wornImg);
#endif

	// Convert pressure map from integer to floating point if needed
	IplImage* pressureMap = cvCreateImage(cvSize(pressureMapIn->width, pressureMapIn->height), IPL_DEPTH_32F, 1);
	if (pressureMapIn->depth == IPL_DEPTH_8U)
	{
		// Convert to 32-bit floating point
		for (y = 0; y < static_cast<uint32_t>(pressureMapIn->height); y++)
		{
			row_ptr_8u_a  = reinterpret_cast<uint8_t*>(pressureMapIn->imageData + y * pressureMapIn->widthStep);
			row_ptr_32f_a = reinterpret_cast<float32_t*>(pressureMap->imageData + y * pressureMap->widthStep);
			for (x = 0; x < static_cast<uint32_t>(pressureMap->width); x++)
				row_ptr_32f_a[x] = static_cast<float32_t>(row_ptr_8u_a[x]) / 255;
		}
	}
	else pressureMap = cvCloneImage(pressureMapIn);
	// TODO: Simplify with cvConvert(pressureMapIn/255, presureMap)

#ifdef _DEBUG
	Util::OpenCV::ShowImageModal("Pressure map converted to FP", pressureMap);
#endif

	// Identify unique pressure values (std::set.insert() does the de-duping for us :)
	std::set<float32_t> pressureValues;
	for (y = 0; y < static_cast<uint32_t>(pressureMap->height); y++)
	{
		row_ptr_32f_a = reinterpret_cast<float32_t*>(pressureMap->imageData + y * pressureMap->widthStep);
		for (x = 0; x < static_cast<uint32_t>(pressureMap->width); x++)
			pressureValues.insert(static_cast<float32_t>(row_ptr_32f_a[x]));
	}

	// Create pressure value window array from unique values
	bool* pressureWindows = new bool[PRESSURE_ITERATIONS];
	float32_t pressureWindowSize = 1.0f/static_cast<float32_t>(PRESSURE_ITERATIONS); 
	for (i = 0; i < PRESSURE_ITERATIONS; i++)
	{
		pressureWindows[i] = false;
		for (std::set<float32_t>::iterator itr = pressureValues.begin(); itr != pressureValues.end(); itr++)
			if ((*itr >= static_cast<float32_t>(i)*pressureWindowSize) && (*itr < static_cast<float32_t>(i+1)*pressureWindowSize))
				pressureWindows[i] = true;
	}

	// Create erosion kernel as per constants
	IplConvKernel* erodeKernel = cvCreateStructuringElementEx(STREL_WIDTH, STREL_HEIGHT, 
		STREL_ANCHOR_X, STREL_ANCHOR_Y, CV_SHAPE_ELLIPSE);

	// Calculate scale of image, based on PRESSURE_ITERATIONS
	float32_t scaleMultiplier = pow(2, static_cast<float32_t>(PRESSURE_ITERATIONS));
	uint32_t scaleX = static_cast<uint32_t>(floor(static_cast<float32_t>(wornImg->width) * scaleMultiplier + 0.5));
	uint32_t scaleY = static_cast<uint32_t>(floor(static_cast<float32_t>(wornImg->height) * scaleMultiplier + 0.5));

	// Final un-worn image is initially cloned from the start worn image
	IplImage* unWornImg = cvCloneImage(wornImg);
	
	// Cumulative wear image is scaled and cloned (implicit in cvResize) from the reference image
	IplImage* cumulativeUnWearImg = cvCreateImage(cvSize(scaleX, scaleY), wornImg->depth, wornImg->nChannels);
	cvResize(wornImg, cumulativeUnWearImg, CV_INTER_CUBIC);

	// Outer loop, one iteration per time block
	for (i = 0; i < timeBlocks; i++)
	{
		// Inner loop, one iteration per pressure application
		for (j = PRESSURE_ITERATIONS; j > 0; j--)
		{
			// Skip iteration if no pressure values are present for this layer
			if (!pressureWindows[j]) break;

			// Cumulativly apply erode to scaled image
			cvDilate(cumulativeUnWearImg, cumulativeUnWearImg, erodeKernel);

			// Scale the wear pass image back to normal size for masking
			IplImage* wearPassImg = cvCreateImage(cvSize(wornImg->width, wornImg->height), wornImg->depth, wornImg->nChannels);
			cvResize(cumulativeUnWearImg, wearPassImg, CV_INTER_CUBIC);

#ifdef _DEBUG
			Util::OpenCV::ShowImageModal("Cumulative un-wear image " + BLOCK_PASS_MSG(i,j), wearPassImg);
#endif	

			// Extract pressure mask (TODO: Save some memory with IPL_DEPTH_1U - not supported by cvThreshold)
			IplImage* pressureMask = cvCreateImage(cvSize(pressureMap->width, pressureMap->height), IPL_DEPTH_8U, 1);
			cvThreshold(pressureMap, pressureMask, (j-1)*pressureWindowSize, 255, CV_THRESH_BINARY_INV);

#ifdef _DEBUG
			Util::OpenCV::ShowImageModal("Pressure mask " + BLOCK_PASS_MSG(i,j), pressureMask);
#endif	

			// Mask out cumulative wear onto final wear image
			for (y = 0; y < static_cast<uint32_t>(pressureMask->height); y++)
			{
				row_ptr_8u_a  = reinterpret_cast<uint8_t*>(pressureMask->imageData + y * pressureMask->widthStep);
				row_ptr_32f_a = reinterpret_cast<float32_t*>(wearPassImg->imageData + y * wearPassImg->widthStep);
				row_ptr_32f_b = reinterpret_cast<float32_t*>(unWornImg->imageData + y * unWornImg->widthStep);
				for (x = 0; x < static_cast<uint32_t>(pressureMask->width); x++)
					if (row_ptr_8u_a[x] != 0) row_ptr_32f_b[x] = row_ptr_32f_a[x];
			}

#if defined(_DEBUG) || defined(_DEMO)
			Util::OpenCV::ShowImageModal("Un-worn image " + BLOCK_PASS_MSG(i,j), unWornImg);
#endif	

			// Clear pressure mask and wear pass image from memory
			cvReleaseImage(&pressureMask);
			cvReleaseImage(&wearPassImg);
		}

		// TODO: Slightly modify pressure map to model passage of time between blocks
	}

	// Clear memory of scaled up image
	cvReleaseImage(&cumulativeUnWearImg);

#if defined(_DEBUG) || defined(_DEMO)
	Util::OpenCV::ShowImageModal("Final (masked) un-worn image", unWornImg);
#endif

	// Return worn image
	return wornImg;
}