#include "precompile.h"

#if _MANAGED
#pragma managed

#include "../Util/cvrt_gdi_opencv.h"
#include "shoe_wear_forward.h"
//#include "shoe_wear_reverse.h"
#include "ClrWrapper.h"

using namespace System::Drawing;
using namespace Util::Convert::Image;

// Algorithms::SimulateShoeWear wrapper
Bitmap^ SimulateShoeWear::ClrWrapper::ShoeWearForward(Bitmap^ refImg, Bitmap^ pressureMap, System::UInt32^ steps)
{
	// Convert input images
	IplImage* cvRefImg      = ToIplImage(refImg);
	IplImage* cvPressureMap = ToIplImage(pressureMap);

	// Run native algorithm
	IplImage* cvWornImg = SimulateShoeWear::ShoeWearForward(cvRefImg, cvPressureMap, static_cast<long>(*steps));

	// Convert output image
	Bitmap^ wornImg = ToGdiBitmap(cvWornImg);

	// Free IplImage pointers
	cvReleaseImage(&cvRefImg);
	cvReleaseImage(&cvPressureMap);
	cvReleaseImage(&cvWornImg);

	// Return worn image
	return wornImg;
}

#pragma unmanaged
#endif // _MANAGED
