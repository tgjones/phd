#pragma once
#include "../Util/types.h"

namespace Test {
	namespace NUnit {
		namespace Config {

			// Settings for Mex file tests
			namespace Mex {
				static const char* MEX_DIR = "C:\\Research\\Code\\cpp\\bin";
			}

			// Settings for Image Conversion tests
			namespace ImgConvert {
				static const char* GREYSCALE_IMG_FILE = "C:\\Research\\Imagery\\FFS100_new\\FFS039.jpg";
			}

			// Settings for Shoe Wear Simulation tests
			namespace SimulateShoeWear {
				static const char*    T0 = "C:\\Research\\Matlab\\images\\FFS039.jpg";
				static const char*    Tx = "C:\\Research\\Matlab\\images\\FFS039_synth_wear.png";
				static const char*    P  = "C:\\Research\\Matlab\\images\\P_01_512x512.png";
				static const uint32_t t  = 4;
			}
		}
	}
}