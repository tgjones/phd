#include "precompile.h"
#include "config.h"
#include "SimulateShoeWearNativeFixture.h"
#include "../SimulateShoeWear/shoe_wear_forward.h"
#include "../SimulateShoeWear/shoe_wear_backward.h"

using namespace System::Drawing;
using namespace Test::NUnit::Config::SimulateShoeWear;

void Test::NUnit::SimulateShoeWearNativeFixture::TestWearForward()
{
	// Context for marshaling
	marshal_context ctx;

	// Read reference image off disk
	IplImage* refImg = cvLoadImage(T0, CV_LOAD_IMAGE_GRAYSCALE);
	if (!refImg) Assert::Fail("Reference image cannot be read.");

	// Read pressure map off disk
	IplImage* pressureMap = cvLoadImage(P, CV_LOAD_IMAGE_GRAYSCALE);
	if (!pressureMap) Assert::Fail("Pressure map cannot be read.");

	// Run algorithm using native entry point
	IplImage* wornImg = SimulateShoeWear::ShoeWearForward(refImg, pressureMap, t);
}

void Test::NUnit::SimulateShoeWearNativeFixture::TestWearBackward()
{
	Assert::Fail("Test not implemented.");
}
