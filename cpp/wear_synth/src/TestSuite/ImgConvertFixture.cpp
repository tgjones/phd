#include "precompile.h"
#include "config.h"
#include "../Util/types.h"
#include "../Util/cvrt_gdi_opencv.h"
#include "../Util/cvrt_matlab_opencv.h"
#include "../Util/matlab_util.h"
#include "../Util/opencv_util.h"
#include "../Util/general_util.h"
#include "ImgConvertFixture.h"

using namespace System::Drawing;
using namespace Util::Convert::Image;
using namespace Test::NUnit::Config::ImgConvert;

void Test::NUnit::ImgConvertFixture::TearDown()
{
	// Insert pause to get allow Matlab COM to catch up
	Util::Timer::Block(0.5);
}

void Test::NUnit::ImgConvertFixture::IplImageToMxArrayGreyscale()
{
	marshal_context ctx;

	// Read reference image into OpenCV
	IplImage* cvBefore = cvLoadImage(GREYSCALE_IMG_FILE, CV_LOAD_IMAGE_GRAYSCALE);

	// Convert to Matlab
	mxArray* mxAfter = ToMxArray(cvBefore);

#if _DEBUG
	// Show before image using OpenCV
	Util::OpenCV::ShowImageModal("Before ToMxArray()", cvBefore);

	// Start Matlab engine
	Util::Matlab::Engine matlab;

	// Load converted image and show
	matlab.Set("I", mxAfter);
	matlab.Eval("imshow(I,[]); title('After ToMxArray()');");
#endif
	
	// Check before and after dimensions
	if ((cvBefore->width != mxGetN(mxAfter)) || 
		(cvBefore->height != mxGetM(mxAfter)))
		Assert::Fail("Image dimensions do not match");

	// Get data pointers
	uchar  *cvBeforeDat = reinterpret_cast<uchar*>(cvBefore->imageData);
	byte_t *mxAfterDat  = static_cast<byte_t*>(mxGetData(mxAfter));

	// Check before and after image data
	for (int x = 0; x < cvBefore->width; x++)
		for (int y = 0; y < cvBefore->height; y++)
			if (cvBeforeDat[y * cvBefore->widthStep + x] != mxAfterDat[x * cvBefore->width + y])
				Assert::Fail("Data in before and after images does not match.");
}

void Test::NUnit::ImgConvertFixture::MxArrayToIplImageGreyscale()
{
	// Start Matlab engine
	Util::Matlab::Engine matlab(".");

	// Read reference image into Matlab
	matlab.Eval("I=imread('" + std::string(GREYSCALE_IMG_FILE) + "');");
	mxArray* mxBefore = matlab.Get("I");

	// Convert to OpenCV
	IplImage* cvAfter = ToIplImage(mxBefore);

#if _DEBUG
	// Show loaded image using OpenCV
	// TODO: This does not get destroyed automatically
	Util::OpenCV::ShowImageModal("After ToIplImage()", cvAfter);

	// Show loaded image
	matlab.Eval("imshow(I,[]); title('Before ToIplImage()');");
#endif

	// Check before and after dimensions
	if ((mxGetM(mxBefore) != cvAfter->height) ||
		(mxGetN(mxBefore) != cvAfter->width))
		Assert::Fail("Image dimensions do not match");

	// Get data pointers
	uchar  *cvAfterDat = reinterpret_cast<uchar*>(cvAfter->imageData);
	byte_t *mxBeforeDat  = static_cast<byte_t*>(mxGetData(mxBefore));

	// Check before and after image data
	for (unsigned int x = 0; x < mxGetN(mxBefore); x++)
		for (unsigned int y = 0; y < mxGetM(mxBefore); y++)
			if (mxBeforeDat[x * mxGetN(mxBefore) + y] != cvAfterDat[y * cvAfter->widthStep + x])
				Assert::Fail("Data in before and after images does not match.");
}

void Test::NUnit::ImgConvertFixture::CliGdiBitmapToIplImageGreyscale()
{
	// Read test image into Bitmap
	Bitmap^ dotNetBefore = 
		gcnew Bitmap(marshal_as<System::String^>(GREYSCALE_IMG_FILE));

	// Convert to OpenCV
	IplImage* cvAfter = ToIplImage(dotNetBefore);

#if _DEBUG
	// TODO: Show before image using .NET

	// Show after image using OpenCV
	Util::OpenCV::ShowImageModal("After ToIplImage()", cvAfter);
#endif

	// Check before and after dimensions
	if ((dotNetBefore->Height != cvAfter->height) ||
		(dotNetBefore->Width != cvAfter->width))
		Assert::Fail("Image dimensions do not match");

	// TODO: Actual data check (see other tests above, use Scan0)
}

void Test::NUnit::ImgConvertFixture::IplImageToCliGdiBitmapGreyscale()
{
	marshal_context ctx;

	// Read reference image into OpenCV
	IplImage* cvBefore = cvLoadImage(GREYSCALE_IMG_FILE, CV_LOAD_IMAGE_GRAYSCALE);

	// Convert to Bitmap
	Bitmap^ dotNetAfter = ToGdiBitmap(cvBefore);

#if _DEBUG
	// Show before image using OpenCV
	Util::OpenCV::ShowImageModal("Before ToGdiBitmap()", cvBefore);

	// TODO: Show after image using .NET
#endif

	// Check before and after dimensions
	if ((cvBefore->height != dotNetAfter->Height) ||
		(cvBefore->width  != dotNetAfter->Width))
		Assert::Fail("Image dimensions do not match");

	// TODO: Actual data check (see other tests above)
}
