#pragma once

#ifndef _MANAGED
#error TestSuite is a pure C++/CLI project and depends on .NET assemblies, compile with MSVC using the /clr switch.
#endif

// C++ Standard Library
#include <string>

// CLR Support Library
#include <msclr/marshal.h>
#include <msclr/marshal_cppstd.h>
using namespace msclr::interop;

// OpenCV
#pragma unmanaged
#include <opencv/cv.h>
#include <opencv/highgui.h>
#pragma managed

// NUnit
using namespace NUnit::Framework;