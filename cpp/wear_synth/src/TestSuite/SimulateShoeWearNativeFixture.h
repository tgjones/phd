#pragma once
#include "SimulateShoeWearFixture.h"
#include "../Util/types.h"

namespace Test {
	namespace NUnit {

		[TestFixture]
		[Category("Native Functions")]
		ref class SimulateShoeWearNativeFixture : public SimulateShoeWearFixture
		{
		public:

			[Test]
			virtual void TestWearForward() override;

			[Test]
			virtual void TestWearBackward() override;
		};
	}
}