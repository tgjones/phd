#pragma once
#include "../Util/types.h"

namespace Test {
	namespace NUnit {

		ref class SimulateShoeWearFixture abstract
		{
		public:

			[Test]
			virtual void TestWearForward() = 0;

			[Test]
			virtual void TestWearBackward() = 0;
		};
	}
}