#pragma once
#include "SimulateShoeWearFixture.h"
#include "../Util/types.h"
#include "../Util/matlab_util.h"

namespace Test {
	namespace NUnit {

		[TestFixture]
		[Category("Mex Functions")]
		ref class SimulateShoeWearMexFixture : public SimulateShoeWearFixture
		{
		protected:
			Util::Matlab::Engine* pMatlab;

		public:
			
			[TestFixtureSetUp]
			virtual void TestFixtureSetUp();

			[TestFixtureTearDown]
			virtual void TestFixtureTearDown();

			[Test]
			virtual void TestWearForward() override;

			[Test]
			virtual void TestWearBackward() override;
		};
	}
}