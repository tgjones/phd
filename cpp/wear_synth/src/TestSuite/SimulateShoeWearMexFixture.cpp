#include "precompile.h"
#include "config.h"
#include "SimulateShoeWearMexFixture.h"
#include "../Util/general_util.h"
#include "../Util/matlab_util.h"
#include "../SimulateShoeWear/shoe_wear_forward.h"
#include "../SimulateShoeWear/shoe_wear_backward.h"

using namespace Test::NUnit::Config::Mex;
using namespace Test::NUnit::Config::SimulateShoeWear;

/**
 * Sets up Matlab engine.
 */
void Test::NUnit::SimulateShoeWearMexFixture::TestFixtureSetUp()
{
	try
	{
		pMatlab = new Util::Matlab::Engine(MEX_DIR);
	}
	catch (std::exception e)
	{
		Assert::Fail("Matlab engine failed to start: " + marshal_as<System::String^>(e.what()));
	}
}

/**
 * Kills off Matlab engine.
 */
void Test::NUnit::SimulateShoeWearMexFixture::TestFixtureTearDown()
{
	try
	{
		delete pMatlab;
	}
	catch (std::exception e)
	{
		Assert::Fail("Error shutting down Matlab engine: " + marshal_as<System::String^>(e.what()));
	}
}

/**
 * Test wear synthesis forward in time.
 *
 * T --------------
 *   | --------> |
 *  T0     t     Tt
 *
 */

void Test::NUnit::SimulateShoeWearMexFixture::TestWearForward()
{
	try 
	{
		// Load reference image, pressure map and 1000s of steps
		pMatlab->Eval("T0=imread('" + std::string(T0) + "')");
		pMatlab->Eval("P=imread('" + std::string(P) + "')");
		// TODO: Use engine.SetUint8() when its implemented.
		pMatlab->Eval("t=" + std::string(Util::Convert::ToString(t)));

		// Run Matlab command
#ifdef _DEBUG
		pMatlab->Eval("Tt=shoe_wear_forward_d(T0,P,t)");
#else
		pMatlab->Eval("Tt=shoe_wear_forward(T0,P,t)");
#endif

		// Check for the existance of output Tt
		if (!pMatlab->Exists("Tt"))
			Assert::Fail("Output variable Tt does not exist.");
	}
	catch (std::runtime_error e)
	{
		// Fail on a Runtime Error (Util::Matlab::Engine can throw these)
		Assert::Fail(marshal_as<System::String^>(e.what()));
	}
}

/**
 * Test wear synthesis backward in time.
 *
 * T ---------------------
 *   |     | <--------- |
 *  T0   Tx-t     t     Tx
 *
 */
void Test::NUnit::SimulateShoeWearMexFixture::TestWearBackward()
{
	try 
	{
		// Load reference image, pressure map and 1000s of steps
		pMatlab->Eval("T0=imread('" + std::string(T0) + "')");
		pMatlab->Eval("Tx=imread('" + std::string(Tx) + "')");
		pMatlab->Eval("P=imread('" + std::string(P) + "')");
		// TODO: Use engine.SetUint8() when its implemented.
		pMatlab->Eval("t=" + std::string(Util::Convert::ToString(t)));

		// Run Matlab command
#ifdef _DEBUG
		pMatlab->Eval("Tt=simulate_shoe_wear_d(T0,Tx,P,t)");
#else
		pMatlab->Eval("Tt=simulate_shoe_wear(T0,Tx,P,t)");
#endif

		// Check for the existance of output Tt (which is really Tx - t)
		if (!pMatlab->Exists("Tt"))
			Assert::Fail("Output variable Tt (Tx - t) does not exist.");
	}
	catch (std::runtime_error e)
	{
		// Fail on a Runtime Error (Util::Matlab::Engine can throw these)
		Assert::Fail(marshal_as<System::String^>(e.what()));
	}
}
