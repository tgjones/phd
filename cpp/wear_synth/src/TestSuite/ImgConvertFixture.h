#pragma once

// TODO: Make this an abstract base class (like SimulateShoeWearFixture) and seperate off
// tests for Greyscale, floating point etc. (at the moment we are only testing 1 type)

namespace Test {
	namespace NUnit {

		[TestFixture]
		[Category("Functional Tests")]
		[Category("Utility Library")]
		ref class ImgConvertFixture
		{
		public:

			[TearDown]
			void TearDown();

			[Test]
			void IplImageToMxArrayGreyscale();

			[Test]
			void MxArrayToIplImageGreyscale();

			[Test]
			void CliGdiBitmapToIplImageGreyscale();

			[Test]
			void IplImageToCliGdiBitmapGreyscale();
	 
		};
	}
}