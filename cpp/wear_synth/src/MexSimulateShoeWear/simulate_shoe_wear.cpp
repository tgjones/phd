#include <mex.h>
#include "simulate_shoe_wear.h"
#include "../Util/traits.h"
#include "../Util/cvrt_matlab_opencv.h"
#include "../Util/matlab_util.h"
#include "../SimulateShoeWear/shoe_wear_forward.h"
#include "../SimulateShoeWear/shoe_wear_backward.h"

// Minimum parameters
const int MIN_NLHS = 1; // W
const int MIN_NRHS = 3; // R, P, s

using namespace Util::Convert::Image;

// Wear shoe forward in time
void MexFunctions::ShoeWearForward(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	// Check parameter count
	Util::Matlab::Mex::CheckParameterCount(nlhs, nrhs, MIN_NLHS, MIN_NRHS);

	// Extract input images and convert to OpenCV format
	IplImage* T0 = ToIplImage(prhs[0]);
	IplImage* P = ToIplImage(prhs[1]);

	// Extract simulated step amount
	long t = static_cast<long>(mxGetScalar(prhs[2])); // Truncate fraction

	// Execute OpenCV algorithm
	IplImage* Tt = SimulateShoeWear::ShoeWearForward(T0, P, t);

	// Convert Tt to Matlab format and set as output parameter
	plhs[0] = ToMxArray(Tt);
}

// Wear shoe backward in time
void MexFunctions::ShoeWearBackward(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[])
{
	// Check parameter count
	Util::Matlab::Mex::CheckParameterCount(nlhs, nrhs, MIN_NLHS, MIN_NRHS);

	// Extract input images and convert to OpenCV format
	IplImage* R = Util::Convert::Image::ToIplImage(prhs[0]);
	IplImage* P = Util::Convert::Image::ToIplImage(prhs[1]);

	// Extract simulated step amount
	long s = static_cast<long>(mxGetScalar(prhs[2])); // Truncate fraction

	// Execute OpenCV algorithm
	IplImage* W = SimulateShoeWear::ShoeWearBackward(R, P, s);

	// Convert W to Matlab format and set as output parameter
	plhs[0] = Util::Convert::Image::ToMxArray(W);
}