#pragma once

#include <mex.h>

namespace MexFunctions {

	void ShoeWearForward(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
	void ShoeWearBackward(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);

}