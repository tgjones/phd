#ifndef COMMON_TYPES_H_
#define COMMON_TYPES_H_

#pragma region Integer aliases

// Unsigned shorthand types
typedef unsigned char      uchar_t;
typedef unsigned short     ushort_t;
typedef unsigned int       uint_t;
typedef unsigned long      ulong_t;
typedef unsigned long long ulonglong_t;

// Integer size aliases
typedef uchar_t     uint8_t;
typedef ushort_t    uint16_t;
typedef ulong_t     uint32_t;
typedef ulonglong_t uint64_t;

// Memory block sizes
typedef uint8_t  byte_t;
typedef uint16_t word_t;
typedef uint32_t dword_t;
typedef uint64_t qword_t;

#pragma endregion

// Nicer OpenCV aliases
typedef IplImage image_t;
typedef CvMat    matrix_t;

// Image formats
typedef enum 
{
	IMAGE_FORMAT_UNKNOWN,
	IMAGE_FORMAT_UNSUPPORTED,
	IMAGE_FORMAT_JPEG,
	IMAGE_FORMAT_TIFF,
	IMAGE_FORMAT_PNG
} image_format_t;

#endif // COMMON_TYPES_H_