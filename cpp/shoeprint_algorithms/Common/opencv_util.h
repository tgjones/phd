#ifndef COMMON_OPENCV_UTIL_H_
#define COMMON_OPENCV_UTIL_H_

// OpenCV
#include <cv.h>
#include <highgui.h>

#pragma region Easy image popup

// Easy image popup
// ^^^^^^^^^^^^^^^^
inline void image_popup(const std::string& title, const image_t* img, const int wait_time = 0)
{
	cvNamedWindow(title.c_str(), CV_WINDOW_AUTOSIZE);
	cvShowImage(title.c_str(), img);
	cvWaitKey(wait_time);
	cvDestroyWindow(title.c_str());
}

#pragma endregion

#pragma region FFT shift for images

// TODO: Implement fft_shift(CvArr*) overload

// FFT shift for images
// ^^^^^^^^^^^^^^^^^^^^
template <typename T>
void fft_shift(image_t* img)
{
	// TODO: Implement default arguments to allow passing through of some values and therefore avoiding
	//       the slow(ish) dereferencing and devides b

	// Dereference x and y
	uint_t width  = img->width;
	uint_t height = img->height;

	// Check that the image is a proper square (x and y are equal)
	if (width != height)
		throw std::runtime_error("Cannot perform FFT shift on an image of unequal dimensions");

	// Dereference some other stuff
	uint_t step   = img->widthStep;
	byte_t* data  = reinterpret_cast<byte_t*>(img->imageData);

	// Get half x and y by bitshifting right (same as divide by 2 but faster)
	uint_t half_width  = width >> 1;
	uint_t half_height = height >> 1;

	// Other variables used in algorithm
	T* img_row_ptr = NULL;
	T* new_row_ptr = NULL;
	T* tmp_row_ptr = reinterpret_cast<T*>(malloc(width * sizeof(T)));
	T tmp_value;
	uint_t x, y;

	// Swap vertically
	for (y = 0; y < height; y++)
	{
		img_row_ptr = reinterpret_cast<T*>(data + y * step);
		for (x = 0; x < half_width; x++)
		{
			tmp_value                   = img_row_ptr[x];
			img_row_ptr[x]              = img_row_ptr[x + half_width];
			img_row_ptr[x + half_width] = tmp_value;
		}
	}

	// Swap horizontally
	for (y = 0; y < half_height; y++)
	{
		img_row_ptr = reinterpret_cast<T*>(data + y * step);
		new_row_ptr = reinterpret_cast<T*>(data + (half_height + y) * step);
		for (x = 0; x < width; x++) tmp_row_ptr[x] = img_row_ptr[x];
		for (x = 0; x < width; x++) img_row_ptr[x] = new_row_ptr[x];
		for (x = 0; x < width; x++) new_row_ptr[x] = tmp_row_ptr[x];
	}
}

#pragma endregion

#endif // COMMON_OPENCV_UTIL_H_