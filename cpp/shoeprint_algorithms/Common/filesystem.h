#ifndef COMMON_FILESYSTEM_H_
#define COMMON_FILESYSTEM_H_

// Boost Filesystem
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/convenience.hpp>

#include "types.h"

namespace fs = boost::filesystem;

#ifdef _WINDOWS_
const char DIR_SEP = '\\';
#else
const char DIR_SEP = '/';
#endif

// Strips a filename from a path, with or without extension
std::string get_filename(const fs::path& path, bool strip_extention = false);

// Strips the filename from a path but returns the directory not the filename
fs::path get_directory(const fs::path& path);

// Scans directory for images and returns a list with filenames in
std::list<fs::path>* get_image_paths(const fs::path& path);

// Gets an image format from the extension
image_format_t get_image_format(const fs::path& path);

// Boolean wrapper around GetImageFormat()
bool is_supported_image(const fs::path& path);

// Load single image data from disk
image_t* read_image(const fs::path& path);

// Read multiple files, basically a std::transform wrapper for ReadImage().
std::list<image_t*>* read_images(const std::list<fs::path>& image_files);

// Scan directory for image files, basically just wraps GetImageFilenames().
std::list<image_t*>* read_images(const fs::path& image_dir);

// Writes image data to disk, this function has the following defaults:
//   fileName  = ""    - Will pick a unique filename automatically
//   overwrite = false - If the fileName is specified then the function can be
//                       forced to overwrite a current image if there is one.
void write_image(const image_t& img, const fs::path& file_name, bool overwrite = false);

#endif // COMMON_FILESYSTEM_H_