#ifndef COMMON_SIMPLE_IO_H_
#define COMMON_SIMPLE_IO_H_

#if defined(_WIN32) || defined(_WIN64)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

#include "filesystem.h"

#pragma region Confirmation message

// Confirmation message
// ^^^^^^^^^^^^^^^^^^^^
inline bool confirm(const std::string& msg, const std::string& title = "Confirmation")
{
	// TODO: Implement confirm()
}

#pragma endregion

#pragma region Information message

// Information message
// ^^^^^^^^^^^^^^^^^^^
inline void info(const std::string& msg, const std::string& title = "INFO", bool block = false)
{
	// Construct one-line output
	std::stringstream ss;
	ss << title << ": " << msg << std::endl;

	// Output warning to stdout
	std::cout << ss.str();

#if defined(_WINDOWS_) && defined(_DEBUG)
	// Output message in Visual Studio (or other Windows debugger) debug window
	OutputDebugString(ss.str().c_str());
#endif

#if defined(_WINDOWS_)
	// Display an error dialog box in Windows
	if (block) MessageBox(NULL, msg.c_str(), title.c_str(), MB_OK | MB_ICONINFORMATION);
#else
	// Just wait for a keypress in other OSs
	if (block) 
	{
		char ch;
		std::cin << ch;
	}
#endif
}

#pragma endregion

#pragma region Warning message

// Warning message
// ^^^^^^^^^^^^^^^
inline void warning(const std::string& msg, const std::string& title = "WARNING", bool block = false)
{
	// Construct one-line output
	std::stringstream ss;
	ss << title << ": " << msg << std::endl;

#if defined (_DEBUG)
	// Output warning to stdout
	std::cout << ss.str();
#else
	// Output warning to stderr
	std::cerr << ss.str();
#endif

#if defined(_WINDOWS_) && defined(_DEBUG)
	// Output message in Visual Studio (or other Windows debugger) debug window
	OutputDebugString(ss.str().c_str());
#endif

#if defined(_WINDOWS_)
	// Display an error dialog box in Windows
	if (block) MessageBox(NULL, msg.c_str(), title.c_str(), MB_OK | MB_ICONWARNING);
#else
	// Just wait for a keypress in other OSs
	if (block) 
	{
		char ch;
		std::cin << ch;
	}
#endif
}

#pragma endregion

#pragma region Error message

// Error message
// ^^^^^^^^^^^^^
inline void error(const std::string& msg, const std::string& title = "ERROR")
{
	// Construct one-line output
	std::stringstream ss;
	ss << title << ": " << msg << std::endl;

#if defined (_DEBUG)
	// Output error to stdout
	std::cout << ss.str();
#else
	// Output error to stderr
	std::cerr << ss.str();
#endif

#if defined(_WINDOWS_) && defined(_DEBUG)
	// Output message in Visual Studio (or other Windows debugger) debug window
	OutputDebugString(ss.str().c_str());
#endif

#if defined(_WINDOWS_)
	// Display an error dialog box in Windows
	MessageBox(NULL, msg.c_str(), title.c_str(), MB_OK | MB_ICONERROR);
#endif
}

#pragma endregion

#pragma region Debug message

#ifdef _DEBUG

// Inline function used by macros
inline void __debug(const std::string file, const uint_t line, const std::string& msg)
{
	// Construct one-line output
	std::stringstream ss;
	ss << "DEBUG: line " << line << " in " << get_filename(file) << ": " << msg << std::endl;
	
	// Output debug message
	std::cout << ss.str();

#if defined(_WINDOWS_)
	// Output message in Visual Studio (or other Windows debugger) debug window
	OutputDebugString(ss.str().c_str());
#endif
}
// File and line wrapper
#define debug(msg) __debug(__FILE__, __LINE__, msg)

#else // _DEBUG not defined

#define debug(msg) // Preprocess out any calls to debug

#endif

#pragma endregion

#endif // COMMON_BASIC_OUTPUT_H_