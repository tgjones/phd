#ifndef COMMON_TIMER_H_
#define COMMON_TIMER_H_

class timer
{
private:
	clock_t start_time;
	double	elapsed;

public:
	inline timer() : start_time(clock()), elapsed(0.0) { }
	double last() const 
	{ 
		return elapsed; 
	}
	inline void start() 
	{ 
		start_time = clock();
		elapsed = 0.0;
	}
	inline double stop()
	{
		elapsed = (static_cast<double>(clock()) - start_time) / CLOCKS_PER_SEC;
		return elapsed;
	}
};

#endif // COMMON_TIMER_H_