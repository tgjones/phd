#include "precompile.h"
#include "filesystem.h"
#include "types.h"

// Boost Filesystem
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/convenience.hpp>

namespace fs = boost::filesystem;

#pragma region Get filename

std::string get_filename(const fs::path& path, bool strip_extension) 
{
	std::string path_str = path.native_file_string();
	std::string filename = "";

	size_t i = path_str.rfind(DIR_SEP, path_str.length());
	if (i != std::string::npos)
		filename = path_str.substr(i + 1, path_str.length() - i);

	if (strip_extension)
	{
		i = filename.rfind('.', filename.length());
		if (i != std::string::npos)
			filename = filename.substr(0, filename.length() - (filename.length() - i));
	}

	return filename;
}

#pragma endregion

#pragma region Get directory

fs::path get_directory(const fs::path& path)
{
	std::string path_str = path.native_file_string();
	fs::path directory;

	size_t i = path_str.rfind(DIR_SEP, path_str.length());
	if (i != std::string::npos)
		directory = fs::path(path_str.substr(0, i));

	return directory;
}

#pragma endregion

#pragma region Get image paths

std::list<fs::path>* get_image_paths(const fs::path& path)
{
	std::list<fs::path>* file_names = NULL; // Instantiated on-demand below

	if (!fs::exists(path))
		throw std::runtime_error("GetImageFilenames: '" + path.native_file_string() + "' does not exist.");

	if (!fs::is_directory(path))
		throw std::runtime_error("GetImageFilenames: '" + path.native_file_string() + "' is not a directory.");

	fs::directory_iterator end;
	for (fs::directory_iterator itr(path); itr != end; ++itr)
	{
		if (is_supported_image(*itr))
		{
			if (file_names == NULL)
				file_names = new std::list<fs::path>();

			file_names->push_back(itr->path().native_file_string());
		}
	}

	return file_names; // Still NULL if no images were found
}

#pragma endregion

#pragma region Get image format

image_format_t get_image_format(const fs::path& path)
{
	// Check for the existance of the path
	if (!fs::exists(path))
		throw std::runtime_error("GetImageFormat: '" + path.native_file_string() + "' does not exist.");

	// Check the path is not a directory
	if (fs::is_directory(path))
		throw std::runtime_error("GetImageFormat: '" + path.native_file_string() + "' is a directory.");
	
	// Get extension of file
	std::string ext = fs::extension(path);

	// If extension is empty cycle loop (first character is always '.')
	if (ext.size() < 1)
		return IMAGE_FORMAT_UNKNOWN;

	// Convert to lowercase
	std::transform(ext.begin(), ext.end(), ext.begin(), tolower);

	// If a JPEG image is found
	if ((ext == ".jpg") || (ext == ".jpeg"))
		return IMAGE_FORMAT_JPEG;

	// If a TIFF image is found
	else if ((ext == ".tif") || (ext == ".tiff"))
		return IMAGE_FORMAT_TIFF;

	// If a PNG image is found
	else if (ext == ".png")
		return IMAGE_FORMAT_PNG;

	// Another type of file
	else
		return IMAGE_FORMAT_UNSUPPORTED;
}

#pragma endregion

#pragma region Is supported image

bool is_supported_image(const fs::path& path)
{
	if (fs::is_directory(path))
		return false;

	image_format_t image_format = get_image_format(path);
	return ((image_format != IMAGE_FORMAT_UNKNOWN) && (image_format != IMAGE_FORMAT_UNSUPPORTED));
}

#pragma endregion

#pragma region Read image

image_t* read_image(const fs::path& path)
{
	// Check for the existance of the path
	if (!fs::exists(path))
		throw std::runtime_error("read_image: '" + path.native_file_string() + "' does not exist.");

	// Check the path is not a directory
	if (fs::is_directory(path))
		throw std::runtime_error("read_image: '" + path.native_file_string() + "' is a directory.");

	// Attempt to read image off disk
	image_t* img = cvLoadImage(path.native_file_string().c_str(), CV_LOAD_IMAGE_GRAYSCALE);

	return img;
}

#pragma endregion

#pragma region Read images from list

std::list<image_t*>* read_images(const std::list<fs::path>& image_files)
{
	std::list<image_t*>* image_data = NULL;
	if (!image_files.empty())
	{
		image_data = new std::list<image_t*>();
		image_t* img = NULL;
		for (std::list<fs::path>::const_iterator itr = image_files.begin(); itr != image_files.end(); ++itr)
		{
			img = read_image(*itr);
			if (img != NULL)
				image_data->push_back(img);
		}
	}
	return image_data;
}

#pragma endregion

#pragma region Read images from directory

std::list<image_t*>* read_images(const fs::path& image_dir)
{
	std::list<fs::path>* image_files = get_image_paths(image_dir);
	
	if (image_files == NULL)
		throw std::runtime_error("read_images: No images found in '" + image_dir.native_file_string() + "'.");

	std::list<image_t*>* image_data = read_images(*image_files);
	
	delete image_files;
	return image_data;
}

#pragma endregion

#pragma region Write image

void write_image(const image_t& img, const fs::path& file_name, bool overwrite)
{
	fs::path dir = get_directory(file_name);

	// Create containing directory if required
	// TODO: Check that whole directory chain is created
	if (!fs::exists(dir))
		fs::create_directory(dir);

	// Either remove old file or throw error if file already exists
	if (fs::exists(file_name) && overwrite)
		fs::remove(file_name);
	else if (fs::exists(file_name))
		throw std::runtime_error("write_image: file '" + file_name.native_file_string() + "' already exists");

	// Write image to disk
	cvSaveImage(file_name.native_file_string().c_str(), &img);
}

#pragma endregion