#ifndef COMMON_UTIL_H_
#define COMMON_UTIL_H_

#include "types.h"

#pragma region Size of an array macro (countof)

// countof(x) macro - returns size of dynamic array
// Credit: http://blogs.msdn.com/the1/archive/2004/05/07/128242.aspx
template <typename T, size_t N>
char (&_ArraySizeHelper(T(&array)[N]))[N];
#define countof(array) (sizeof(_ArraySizeHelper(array)))

#pragma endregion

#pragma region Convert anything to a string (to_string)

// t_string - convert anything to a string
// Credit: http://notfaq.wordpress.com/2006/08/30/c-convert-int-to-string/
template <typename T>
inline std::string to_string (const T& t)
{
	std::stringstream ss;
	ss << t;
	return ss.str();
}

#pragma endregion

// Macro that implements the abs value of an FFTW complex number
#define FFTW_ABS_COMPLEX(cmplx) sqrt(cmplx[0]*cmplx[0] + cmplx[1]*cmplx[1])

#endif // COMMON_UTIL_H_