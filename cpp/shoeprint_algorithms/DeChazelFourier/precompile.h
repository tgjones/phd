#ifndef DE_CHAZEL_FOURIER_PRECOMPILE_H_
#define DE_CHAZEL_FOURIER_PRECOMPILE_H_

#if defined(_MSC_VER) && (_MSC_VER >= 800)
#define _CRT_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#pragma warning(disable: 4258, 4068)
#endif

// C libraries
#include <cstdlib>
#include <cstddef>

// C++ libraries
#include <string>
#include <iostream>
#include <list>

// Boost Array
#include <boost/array.hpp>

// Boost Filesystem
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/convenience.hpp>

// Boost Program Options
#include <boost/program_options.hpp>

// OpenCV
#include <cv.h>
#include <highgui.h>

// FFTW
#include <fftw3.h>

#endif // DE_CHAZEL_FOURIER_PRECOMPILE_H_