#ifndef DE_CHAZEL_FOURIER_ALGORITHM_H_
#define DE_CHAZEL_FOURIER_ALGORITHM_H_

#include "precompile.h"
#include "../common/types.h"

namespace fs = boost::filesystem;

namespace de_chazel_fourier {
	
	// Constants
	const uint_t RESAMPLE_SIZE  = 512; // Must be even
	const int ROTATION_LIMIT    = 30;  // Range of -ROTATION_LIMIT to ROTATION_LIMIT
	const double MASK_HIGH_FREQ = 0.5; // Frequencies higher than this will be masked away
	const double MASK_LOW_FREQ  = 0.1; // Frequencies lower than this will be masked away

	// Compile-time arithmetic
	const uint_t HALF_RESAMPLE_SIZE = RESAMPLE_SIZE / 2;
	const uint_t TOTAL_PIXELS       = RESAMPLE_SIZE * RESAMPLE_SIZE;
	const uint_t IMG_ARR_SIZE       = (ROTATION_LIMIT * 2) + 1; // Size of rotated_img_arr_t typedef
	const double R_CEILING          = (MASK_HIGH_FREQ * RESAMPLE_SIZE) * (MASK_HIGH_FREQ * RESAMPLE_SIZE);
	const double R_FLOOR	        = (MASK_LOW_FREQ * RESAMPLE_SIZE) * (MASK_LOW_FREQ * RESAMPLE_SIZE);

	// Typedefs
	typedef boost::array<image_t*, IMG_ARR_SIZE> rotated_img_arr_t;
	typedef std::map<fs::path, double> match_map_t;
	typedef std::pair<fs::path, double> match_map_pair_t;

	// Transform image
	image_t* transform(const image_t* img_in);

	// Generate + and - rotated images for preprocessing 
	rotated_img_arr_t* rotate(const image_t* img_in);

	// Preprocess images
	void preprocess(const std::list<fs::path>& unprocessed_img_files, const fs::path& cache_dir);

	// Match two transformed images
	match_map_t match(const fs::path& query_img_file, const std::list<fs::path>& unprocessed_img_files, const fs::path& cache_dir);
}

#endif // DE_CHAZEL_FOURIER_ALGORITHM_H_