#include "precompile.h"
#include "algorithm.h"
#include "common.h"

namespace fs = boost::filesystem;

#pragma region Transform

// Transform
// ^^^^^^^^^
image_t* de_chazel_fourier::transform(const image_t* img_in)
{
#ifdef _POPUPS
	image_popup("Transform Input", img_in);
#endif

#ifdef _TIMERS
	timer timer1;
#endif

	// Loop variables
	register uint_t i, x, y;

	// Initialise images used in algorithm
	image_t* img_8u  = cvCreateImage(cvSize(RESAMPLE_SIZE, RESAMPLE_SIZE), IPL_DEPTH_8U, 1);
	image_t* img_32f = cvCreateImage(cvSize(RESAMPLE_SIZE, RESAMPLE_SIZE), IPL_DEPTH_32F, 1);

	// Dereference image width steps
	uint_t step_8u  = img_8u->widthStep;
	uint_t step_32f = img_32f->widthStep;

	// Dereference pointers to image data
	byte_t* img_dat_8u  = reinterpret_cast<byte_t*>(img_8u->imageData);
	byte_t* img_dat_32f = reinterpret_cast<byte_t*>(img_32f->imageData);

	// Row pointers for use in pointer arithmetic
	uchar* img_row_8u  = NULL;
	float* img_row_32f = NULL;

	// TODO: Remove scan border
	
	// Wrong size, scale required
	if ((img_in->height != RESAMPLE_SIZE) || (img_in->width != RESAMPLE_SIZE))
	{
		uint_t width, height;
		double scale_factor;

		// image will likely be too tall so get scaled width
		if (img_in->height != RESAMPLE_SIZE)
		{
			scale_factor = static_cast<float>(RESAMPLE_SIZE) / static_cast<float>(img_in->height);
			height = RESAMPLE_SIZE;
			width = static_cast<uint_t>(static_cast<float>(img_in->width) * scale_factor);
		}

		// If width is still too much (unlikely) then scale down further
		if (width > RESAMPLE_SIZE)
		{
			scale_factor = static_cast<float>(RESAMPLE_SIZE) / static_cast<float>(img_in->width);
			height = static_cast<uint_t>(static_cast<float>(img_in->height) * scale_factor);
			width = RESAMPLE_SIZE;
		}

		// Create temporary image to correct size and do resize
		CvSize size = cvSize(width, height);
		image_t* img_scale = cvCreateImage(size, IPL_DEPTH_8U, 1);
		cvResize(img_in, img_scale);

		// Set final image entirely to white
		cvSet(img_8u, cvScalar(255));

		// Determine start and end x and y
		uint_t half_width  = width >> 1;
		uint_t half_height = height >> 1;
		uint_t start_row   = HALF_RESAMPLE_SIZE - half_height;
		uint_t start_col   = HALF_RESAMPLE_SIZE - half_width;
		uint_t end_row     = HALF_RESAMPLE_SIZE + half_height;
		uint_t end_col     = HALF_RESAMPLE_SIZE + half_width;

		// Copy scaled image onto white canvas
		byte_t* img_scale_dat = reinterpret_cast<byte_t*>(img_scale->imageData);
		uint_t img_scale_step = img_scale->widthStep;
		uchar* img_scale_row  = NULL;
		uint_t img_scale_x, img_scale_y;
		for (y = start_row, img_scale_y = 0; y < end_row; y++, img_scale_y++)
		{
			img_row_8u    = static_cast<uchar*>(img_dat_8u + y * step_8u);
			img_scale_row = static_cast<uchar*>(img_scale_dat + img_scale_y * img_scale_step);
			for (x = start_col, img_scale_x = 0; x < end_col; x++, img_scale_x++)
				img_row_8u[x] = img_scale_row[img_scale_x];
		}

		// Free temporary image from memory
		cvReleaseImage(&img_scale);
	}

	// Presized correctly, just copy input to algorithm image directly
	else cvCopy(img_in, img_8u);

#ifdef _POPUPS
	image_popup("Scaled & Padded", img_8u);
#endif

	// Noise removal (values below 5 are reduced to 0)
	cvThreshold(img_8u, img_8u, 5, 5, CV_THRESH_TOZERO);

#ifdef _POPUPS
	image_popup("Noise Removed", img_8u);
#endif

	// Find mean pixel value
	float mean_pixel_value = static_cast<float>(cvMean(img_8u));

	// Substract mean from each pixel value and store in pMeanData
	for (y = 0; y < RESAMPLE_SIZE; y++)
	{
		img_row_8u  = static_cast<uchar*>(img_dat_8u + y * step_8u);
		img_row_32f = reinterpret_cast<float*>(img_dat_32f + y * step_32f);
		for (x = 0; x < RESAMPLE_SIZE; x++)
			img_row_32f[x] = static_cast<float>(img_row_8u[x]) - mean_pixel_value;
	}

	// Free 8bit image from memory as we'll only be working with the 32bit one from now on
	cvReleaseImage(&img_8u);

#ifdef _TIMERS
	timer1.start();
#endif

	// Initialise FFTW input and output structures
    fftwf_complex* fft_dat_in_out  = static_cast<fftwf_complex*>(fftwf_malloc(sizeof(fftwf_complex) * TOTAL_PIXELS));

	// Copy image data from image_t into FFTW structure for processing
	for (y = 0, i = 0; y < RESAMPLE_SIZE; y++)
	{
		img_row_32f = reinterpret_cast<float*>(img_dat_32f + y * step_32f);
		for (x = 0; x < RESAMPLE_SIZE; x++, i++)
		{
			fft_dat_in_out[i][0] = img_row_32f[x];
			fft_dat_in_out[i][1] = 0;
		}
	}
	
	// Execute 2D FFTW algorithm (SINGLE PRECISION)
	fftwf_plan fft_plan = fftwf_plan_dft_2d(RESAMPLE_SIZE, RESAMPLE_SIZE, fft_dat_in_out, fft_dat_in_out, FFTW_FORWARD, FFTW_ESTIMATE);
	fftwf_execute(fft_plan);

	// Copy FFT data back into image
	for (y = 0, i = 0; y < RESAMPLE_SIZE; y++)
	{
		img_row_32f = reinterpret_cast<float*>(img_dat_32f + y * step_32f);
		for (x = 0; x < RESAMPLE_SIZE; x++, i++)
		{
			img_row_32f[x] = static_cast<float>(FFTW_ABS_COMPLEX(fft_dat_in_out[i]));
		}
	}

	// Free FFTW data
	fftwf_destroy_plan(fft_plan);
	fftwf_free(fft_dat_in_out);	

#ifdef _TIMERS
	timer1.stop();
	info("FFTW execution took " + to_string(timer1.last()) + " seconds.");
#endif

	// Line frequencies up properly
	fft_shift<float>(img_32f);

	// Logarithm and normalise
	cvLog(img_32f, img_32f);
	cvNormalize(img_32f, img_32f, 1, 0, CV_MINMAX);

#ifdef _POPUPS
	image_popup("FFT Normalised Output", img_32f);
#endif

	// Mask off unneccesary frequencies to black
	float r;
	for (y = 0; y < RESAMPLE_SIZE; y++)
	{
		img_row_32f = reinterpret_cast<float*>(img_dat_32f + y * step_32f);
		for (x = 0; x < RESAMPLE_SIZE; x++)
		{
			// Calculate R
			r = static_cast<float>(((x - HALF_RESAMPLE_SIZE) * (x - HALF_RESAMPLE_SIZE)) 
				+ ((y - HALF_RESAMPLE_SIZE) * (y - HALF_RESAMPLE_SIZE)));

			// Set pixel to black if its outside the range we care about
			if ((r < R_FLOOR) || (r > R_CEILING)) img_row_32f[x] = 0;
		}
	}

#ifdef _POPUPS
 	image_popup("Masked FFT", img_32f);
#endif

	return img_32f;
}

#pragma endregion

#pragma region Rotate

// Rotate
// ^^^^^^
de_chazel_fourier::rotated_img_arr_t* de_chazel_fourier::rotate(const image_t* img_in)
{
	int width	= img_in->width;
	int height	= img_in->height;
	CvSize size = cvSize(width, height);
	register int i, r;

	// Return array that will hold rotated images
	rotated_img_arr_t* rotated_img_arr = new rotated_img_arr_t();
	// NOTE: Do I need to initialise every image?
	for (i = 0; i < IMG_ARR_SIZE; i++)
		(*rotated_img_arr)[i] = cvCreateImage(size, IPL_DEPTH_32F, 1);

	// Initialse rotation matrix and image center
   	CvMat* rotation_matrix = cvCreateMat(2, 3, CV_MAT32F);
	CvPoint2D32f center = cvPoint2D32f(width >> 1, height >> 1);

	// Rotate and finally store in return array
	for (r = -ROTATION_LIMIT, i = 0; r <= ROTATION_LIMIT; r++, i++)
	{
		// Skip 0 degress (just copy and apply mask directly)
		if (r == 0)
		{
			cvCopy(img_in, (*rotated_img_arr)[i]);
		}

		// For all other angles
		else
		{
			// Determine affine matrix based on angle and apply
			cv2DRotationMatrix(center, r, 1, rotation_matrix);
			cvWarpAffine(img_in, (*rotated_img_arr)[i], rotation_matrix);
		}
#ifdef _POPUPS
		image_popup("Rotated by " + to_string(r) + " degrees", (*rotated_img_arr)[i], 20);
#endif
	}

	// Clean memory
	cvReleaseMat(&rotation_matrix);

	return rotated_img_arr;
}

#pragma endregion

#pragma region Preprocess

// Preprocess
// ^^^^^^^^^^
void de_chazel_fourier::preprocess(const std::list<fs::path>& unprocessed_img_files, const fs::path& cache_dir)
{
#ifdef _TIMERS
	timer timer1, timer2, timer3;
	double transform_time = 0;
	double rotation_time = 0;
	double disk_time = 0;
	timer1.start();
	float iterations = 0.0;
#endif

	// Error if cacheDir exists but is not a directory
	if (fs::exists(cache_dir) && !fs::is_directory(cache_dir))
		throw std::runtime_error("Precache: Cache path exists but is not a directory.");

	// Create cache directory if it does not exist
	if (!fs::exists(cache_dir))
		fs::create_directory(cache_dir);

	// Temporary storage
	image_t* transformed_img				   = NULL;
	std::list<fs::path>* transformed_img_files = NULL;
	rotated_img_arr_t* rotated_img_arr         = NULL;

	// Return list with preprocessed image directories 
	std::list<fs::path> preprocessed_img_dirs;

	// Precache unprocessed images
	fs::path transformed_img_cache_dir;
	fs::path transformed_img_filename;
	int r;
	for (std::list<fs::path>::const_iterator itr = unprocessed_img_files.begin();
		itr != unprocessed_img_files.end(); ++itr)
	{

#ifdef _TIMERS
		timer2.start();
#endif

		transformed_img_cache_dir = cache_dir / get_filename(itr->native_file_string(), true);

		// Directory for processed images exists so assume preprocessing has taken place
		if (fs::exists(transformed_img_cache_dir))
		{
			if (fs::is_directory(transformed_img_cache_dir))
			{
				debug("Precache directory '" + transformed_img_cache_dir.native_file_string()
					+ "' found so assuming '" + itr->native_file_string() + "' has already been preprocessed");

				transformed_img_files = get_image_paths(transformed_img_cache_dir);

				// Error if no images were found
				if (transformed_img_files == NULL)
					throw std::runtime_error("Directory '" 
						+ transformed_img_cache_dir.native_file_string()
						+ "' exists but does not contain any images.");

				// Check count of detected images
				if (transformed_img_files->size() < IMG_ARR_SIZE)
					throw std::runtime_error("Directory '"
						+ transformed_img_cache_dir.native_file_string()
						+ "' exists but does not contain enough images.");

				// Check filenames for correct image
				bool found;
				for (std::list<fs::path>::const_iterator itr = transformed_img_files->begin();
					itr != transformed_img_files->end(); ++itr)
				{
					found = false;
					r = -ROTATION_LIMIT;
					for (r = -ROTATION_LIMIT; r <= ROTATION_LIMIT; r++)
						if (get_filename(*itr, true) == to_string(r))
						{
							found = true;
							break;
						}

					if (!found)
						throw std::runtime_error("Directory '"
							+ transformed_img_cache_dir.native_file_string()
							+ "' exists but does not contain a complete set of preprocessed images.");
				}

				delete transformed_img_files;
				transformed_img_files = NULL;
				// TODO: Refactor so that individual angles can be processed, rather than all at once.
			}

			// If the filename exists but is not a directory then there is a name-clash type problem
			else
				throw std::runtime_error("precache: '" + transformed_img_cache_dir.native_file_string() 
					+ "' is not a directory.");
		}

		// Preprocessed directory does not exist so preprocessing is required
		else
		{
			debug("No precache directory found for '" + itr->native_file_string() + "' so preprocessing of image is starting...");

#ifdef _TIMERS
			timer3.start();
#endif

			// Image transformation
			transformed_img = transform(read_image(*itr));

#ifdef _TIMERS
			transform_time += timer3.stop();
			info("Transformation of '" + itr->native_file_string() + "' took " + to_string(timer3.last()) + " seconds.");
			timer3.start();
#endif

			// Generate rotation variants
			rotated_img_arr = rotate(transformed_img);

#ifdef _TIMERS
			rotation_time += timer3.stop();
			info("Generating rotation variants of '" + itr->native_file_string() + "' took " + to_string(timer3.last()) + " seconds.");
			timer3.start();
#endif

			// Save transformed images to disk
			r = -ROTATION_LIMIT;
			for (rotated_img_arr_t::iterator itr = rotated_img_arr->begin();
				itr != rotated_img_arr->end(); ++itr, r++)
			{
				transformed_img_filename = transformed_img_cache_dir / std::string(to_string(r) + ".png");
				write_image(**itr, transformed_img_filename);
				cvReleaseImage(&*itr);
			}

#ifdef _TIMERS
			disk_time += timer3.stop();
			info("Writing transformed rotation variants of '" + itr->native_file_string() + "' to disk took " + to_string(timer3.last()) + " seconds.");
			iterations++;
#endif

			delete rotated_img_arr;
			rotated_img_arr = NULL;
		}

#ifdef _TIMERS
		timer2.stop();
		info("Precaching of '" + itr->native_file_string() + "' took " + to_string(timer3.last()) + " seconds.");
#endif
			
		// By now we can assume everything has been successful and add the directory to the return list
		preprocessed_img_dirs.push_back(transformed_img_cache_dir);

	}

#ifdef _TIMERS
	timer1.stop();
	info("Preprocessing finished in " + to_string(timer1.last()) + " seconds.");
	info("Total recorded time spent transforming:        " + to_string(transform_time) + " seconds.");
	info("Total recorded time spent rotating:            " + to_string(rotation_time) + " seconds.");
	info("Total recorded time spent writing to disk:     " + to_string(disk_time) + " seconds.");
#endif
}

#pragma endregion

#pragma region Match

// Match
// ^^^^^
de_chazel_fourier::match_map_t de_chazel_fourier::match(const fs::path& query_img_file, const std::list<fs::path>& unprocessed_img_files, const fs::path& cache_dir)
{
	// Firstly preprocess reference images
	preprocess(unprocessed_img_files, cache_dir);

	// Transform query image
	image_t* transformed_query_img = transform(read_image(query_img_file));

	// Loop variables
	register int x, y, i;

	// Create map that will hold results
	match_map_t result_map;
	
	// For each preprocessed image
	fs::path reference_img_cache_dir;

	// Query image stuff
	byte_t* query_img_dat = reinterpret_cast<byte_t*>(transformed_query_img->imageData);
	uint_t query_img_step = transformed_query_img->widthStep;
	float* query_img_row = NULL;

	// Get mean of query image
	double query_img_mean = cvMean(transformed_query_img);

	// Get standard deviation of query image
	double query_img_std = 0.0;
	for (y = 0; y < RESAMPLE_SIZE; y++)
	{
		query_img_row = reinterpret_cast<float*>(query_img_dat + y * query_img_step);
		for (x = 0; x < RESAMPLE_SIZE; x++)
			query_img_std += pow(query_img_row[x] - query_img_mean, 2.0);
	}
	query_img_std = sqrt(query_img_std / TOTAL_PIXELS);

	// Standardise variance across query image
	for (y = 0; y < RESAMPLE_SIZE; y++)
	{
		float* query_img_row = reinterpret_cast<float*>(query_img_dat + y * query_img_step);
		for (x = 0; x < RESAMPLE_SIZE; x++)
			query_img_row[x] = static_cast<float>((query_img_row[x] - query_img_mean) / query_img_std);
	}

	// 8bit Reference image stuff (set in loop)
	image_t* reference_img_8u    = NULL;
	byte_t* reference_img_8u_dat = NULL;
	uchar* reference_img_8u_row  = NULL;
	uint_t reference_img_8u_step;

	// 32bit reference image stuff
	image_t* reference_img_32f = cvCreateImage(cvSize(RESAMPLE_SIZE, RESAMPLE_SIZE), IPL_DEPTH_32F, 1);
	byte_t* reference_img_32f_dat = reinterpret_cast<byte_t*>(reference_img_32f->imageData);
	float* reference_img_32f_row = NULL;
	uint_t reference_img_32f_step = reference_img_32f->widthStep;

	// Generic reference image stuff
	fs::path reference_img_filename;
	double reference_img_mean, reference_img_std;

	// Correlation coefficients
	double correlation, correlation_max;

	for (std::list<fs::path>::const_iterator itr = unprocessed_img_files.begin(); itr != unprocessed_img_files.end(); ++itr)
	{
		// Construct precache directory for image
		reference_img_cache_dir = cache_dir / get_filename(itr->native_file_string(), true);

		// Assume directory exists because preprocess is run above
		correlation_max = 0.0;
		for (i = -ROTATION_LIMIT; i <= ROTATION_LIMIT; i++)
		{
			fs::path reference_img_path = reference_img_cache_dir / std::string(to_string(i) + ".png");
			reference_img_8u = read_image(reference_img_path);

			if (reference_img_8u->nChannels != 1)
				throw std::runtime_error("Reference image '" + reference_img_path.native_file_string() + "' is not a single channel image.");

			// Dereference reference image stuff
			reference_img_8u_dat  = reinterpret_cast<byte_t*>(reference_img_8u->imageData);
			reference_img_8u_step = reference_img_8u->widthStep;

			// Convert to 32-bit floating point
			for (y = 0; y < RESAMPLE_SIZE; y++)
			{
				reference_img_8u_row  = reinterpret_cast<uchar*>(reference_img_8u_dat + y * reference_img_8u_step);
				reference_img_32f_row = reinterpret_cast<float*>(reference_img_32f_dat + y * reference_img_32f_step);
				for (x = 0; x < RESAMPLE_SIZE; x++)
					reference_img_32f_row[x] = static_cast<float>(reference_img_8u_row[x]) / 255;
			}

			// Get reference image mean
			reference_img_mean = cvMean(reference_img_32f);

			// Get reference image standard deviation
			reference_img_std = 0.0;
			for (y = 0; y < RESAMPLE_SIZE; y++)
			{
				reference_img_32f_row = reinterpret_cast<float*>(reference_img_32f_dat + y * reference_img_32f_step);
				for (x = 0; x < RESAMPLE_SIZE; x++)
					reference_img_std += pow(reference_img_32f_row[x] - reference_img_mean, 2.0);
			}
			reference_img_std = sqrt(reference_img_std / TOTAL_PIXELS);

			// Standardise variance across query image
			for (y = 0; y < RESAMPLE_SIZE; y++)
			{
				reference_img_32f_row = reinterpret_cast<float*>(reference_img_32f_dat + y * reference_img_32f_step);
				for (x = 0; x < RESAMPLE_SIZE; x++)
					reference_img_32f_row[x] = static_cast<float>((reference_img_32f_row[x] - reference_img_mean) / reference_img_std);
			}

			// Calculate correlation coefficient
			correlation = 0.0;
			for (y = 0; y < RESAMPLE_SIZE; y++)
			{
				query_img_row         = reinterpret_cast<float*>(query_img_dat + y * query_img_step);
				reference_img_32f_row = reinterpret_cast<float*>(reference_img_32f_dat + y * reference_img_32f_step);
				for (x = 0; x < RESAMPLE_SIZE; x++)
					correlation += query_img_row[x] * reference_img_32f_row[x];
			}
			correlation /= TOTAL_PIXELS;
			
			// Save if its higher than the highest coefficient so far
			if (correlation > correlation_max)
				correlation_max = correlation;
		}

		// Add result to result map
		result_map.insert(result_map.end(), match_map_pair_t(*itr, correlation_max));

		debug("Matching of '" + query_img_file.native_file_string() + "' with " + itr->native_file_string()
			+ "' returned a correlation coefficient of " + to_string(correlation_max) + ".");
	}

	// TODO: Sort results
	//std::sort(result_map.begin(), result_map.end());

	cvReleaseImage(&transformed_query_img);

	return result_map;
}

#pragma endregion
