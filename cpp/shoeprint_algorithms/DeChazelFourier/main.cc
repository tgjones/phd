#include "precompile.h"
#include "common.h"
#include "algorithm.h"

namespace po = boost::program_options;

static const char* PRECACHE_DIR = "precache";

// Entry point for DeChazelFourier
int main(int argc, char* argv[])
{
	timer _timer;
	try 
	{
		// General options source
		po::options_description desc("General options");
		desc.add_options()
			("help,h",                                "Produce (this) help message.")
			("query,q",     po::value<std::string>(), "Query image to run the algorithm.")
			("reference,r", po::value<std::string>(), "Directory containing reference image files.")
			("cache,c",     po::value<std::string>(), "Directory either containing or to contain cached images.")
			;

		// Parse command line and store in vm
		po::variables_map vm;
		// TODO: Wrap po::store() in try, catch and deal with invalid argument
		po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
		po::notify(vm);

		// Print help message and exit
		if (vm.count("help") > 0)
		{
			std::cout << desc << std::endl;
			return EXIT_SUCCESS;
		}

		// Check for query image
		fs::path query_file = "";
		if (vm.count("query") > 0)
		{
			query_file = vm["query"].as<std::string>();
		
			// Check that query image exists and is not a directory
			if ((!fs::exists(query_file)) || (fs::is_directory(query_file)))
				throw std::runtime_error("Invalid query image '" + query_file.native_file_string() + "'");
		}
		else
			info("No query image specified, preprocessing-only mode selected.");

		// If input directory is not specified then use default
		fs::path ref_dir;
		if (vm.count("reference") > 0)
			ref_dir = vm["reference"].as<std::string>();
		else
			throw std::runtime_error("Invalid reference database directory.");

		// Check that reference directory exists and is a directory
		if ((!fs::exists(ref_dir)) || (!fs::is_directory(ref_dir)))
			throw std::runtime_error("Invalid reference database directory '" + ref_dir.native_file_string() + "'");

		// If input directory is not specified then use default
		std::string cache_dir;
		if (vm.count("cache") > 0)
			cache_dir = vm["cache"].as<std::string>();
		else
		{
			cache_dir = PRECACHE_DIR;
			if (!fs::exists(cache_dir))
				fs::create_directory(cache_dir);
		}

		// Search for image files in source directory
		std::list<fs::path>* ref_image_files = get_image_paths(ref_dir);

		// If no images are found then exit with error
		if (ref_image_files == NULL)
			throw std::runtime_error("No images were found in '" + ref_dir.native_file_string() + "'");

		// If query file specified then perform match
		if (query_file != "")
		{
			de_chazel_fourier::match(query_file, *ref_image_files, cache_dir);
			// TODO: Output match results
		}

		// If no query image specified then just do preprocess
		else de_chazel_fourier::preprocess(*ref_image_files, cache_dir);

		return EXIT_SUCCESS;
	}
	catch (std::runtime_error& e)
	{
		error(e.what(), "Runtime Error");
		return EXIT_FAILURE;
	}
	catch (std::exception& e)
	{
		error(e.what(), "Exception");
		return EXIT_FAILURE;
	}
}