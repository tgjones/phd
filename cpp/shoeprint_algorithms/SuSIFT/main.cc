#include "precompile.h"
#include "common.h"
#include "algorithm.h"

// Guard against compiling outside of MEX
// TODO: Work out how to build MEX functions in MSVC
#ifndef MATLAB_MEX_FILE
#error Currently SuSIFT can only be compiled with the 'mex' utility.
#endif

// Entry point for MEX
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

}