#ifndef SU_SIFT_PRECOMPILE_H_
#define SU_SIFT_PRECOMPILE_H_

#if defined(_MSC_VER) && (_MSC_VER >= 800)
#define _CRT_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
//#pragma warning(disable: 4258, 4068)
#endif

// MEX
#include <mex.h>

// C libraries
#include <cstdlib>
#include <cstddef>

// C++ libraries
#include <string>
#include <iostream>
#include <list>

// OpenCV
#include <cv.h>
#include <highgui.h>

// VLFeat
#include <vl/sift.h>

#endif // SU_SIFT_PRECOMPILE_H_