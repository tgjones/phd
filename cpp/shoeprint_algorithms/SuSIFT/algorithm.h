#ifndef SU_SIFT_ALGORITHM_H_
#define SU_SIFT_ALGORITHM_H_

#include "precompile.h"
#include "../common/types.h"

namespace su_sift {
	
	// Constants

	// Compile-time arithmetic

	// Typedefs

	// Transform image

	// Generate + and - rotated images for preprocessing 

	// Preprocess images

	// Match two transformed images
}

#endif // SU_SIFT_ALGORITHM_H_