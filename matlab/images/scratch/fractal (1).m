% 4.2.2 in "Cuda by Example"

function im = fractal(dims, cpl, scale, threshold)
    xdim = dims(1);
    ydim = dims(2);
    im = zeros(ydim, xdim, 3);
    
    for y = 1 : ydim
        for x = 1 : xdim
            im(y,x,1) = julia(x,y,dims, cpl, scale, threshold);
            im(y,x,2:3) = 0;
        end
    end
    
end % function fractal

function val = julia(x, y, dims, cpl, scale, threshold)
    jx = scale * (dims(1)/2 - x) / (dims(1)/2);
    jy = scale * (dims(2)/2 - y) / (dims(2)/2);
    
    a = complex(jx, jy);
    val = 0;
    for i = 1 : 200
        a = a * a + cpl;
        %if abs(a) > threshold
            %return;
        %end
    end % for i
    val = a;%1;
end % function julia