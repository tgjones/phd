function [trans_x, trans_y] = micro_registration(A,B,range,pixel_thresh)
% This function implements a brute-force approach to manual image
% registration under the assumption the two images are already very nearly
% registered and we just need to 'fine-tune' it on a local basis.
%
% The algorithm expects A and B to be of the same size, and obviously range
% has to be a sensible value too. As a rule of thumb try and make the
% images as large as possible and the range as small as possible.
%
% The algorithm takes the following form:
%
%  1. For every possible spatial combination of A overlayed on B within
%  range pixels do the following steps.
%  2. Compute the Otsu graylevel to binarise the two images.
%  3. AND the two binary images.
%  4. Count the pixels, the offset used when we obtained the maximum count
%  is the registered position.
%
% The main drawback of this simplistic approach is that potentially the
% function could actually return more than one possible answer, if the
% pixel counts are the same. This is a deficiency that could potentially be
% solved with some kind of alternative sub-pixel approach.

% Confirm A and B are the same size
if (size(A) ~= size(B))
    error('A and B need to be the same size');
end

% Save image sizes with friendly names
image_height = size(A,1);
image_width  = size(A,2);

% Compute Otsu thresholds for later
base_thresh    = graythresh(A)*255;
overlay_thresh = graythresh(B)*255;

% Get binary block for counting pixels
base_bw    = A > base_thresh;
overlay_bw = B > overlay_thresh;

% Only perform computations on blocks with information in A or B
if (sum(base_bw(:)) >= pixel_thresh) && (sum(overlay_bw(:)) >= pixel_thresh)

    % Compute maximum image size taking into account the range
    window_height = image_height+(2*range);
    window_width  = image_width+(2*range);

    % Generate zero-padded base image (with A in the centre)
    base = zeros(window_height,window_width);
    base(range+1:range+image_height,range+1:range+image_width) = A;

    % Generate output grid for all the measurements
    trans = zeros(range*2,range*2);

    % Iterate through all possible positions within the range
    for m = 1:range*2
        for n = 1:range*2

            % Generate padded overlay image (with A in the current position)
            overlay = zeros(window_height,window_width);
            overlay(m:m+image_height-1,n:n+image_width-1) = B;

            % Binarise the base and the overlay
            base_bw    = base > base_thresh;
            overlay_bw = overlay > overlay_thresh;

            % AND the binary images together
            combined_bw = base_bw&overlay_bw;

            % Count the number of pixels and save in output
            trans(m,n) = sum(combined_bw(:));
        end
    end

    % Get the indices for the maximum pixel counts
    [t,ind] = max(trans(:));
    [m,n]   = ind2sub(size(trans),ind);

    % Convert the offset back to the original coordinate space
    trans_x = n-range-1;
    trans_y = m-range-1;
    
% Empty blocks return {0,0} as offset
else
    
    trans_x = 0;
    trans_y = 0;
    
end
