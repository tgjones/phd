function show_wear_data(I,block_size,wear_measures)
% This function takes a 2D array of measures and visualises them using a
% MATLAB colour map

% Get block rows and columns
block_rows = size(wear_measures,1);
block_cols = size(wear_measures,2);

% Pad image with white to fit block multiple
tmp = I;
I = ones(block_rows*block_size,block_cols*block_size)*255;
I(1:size(tmp,1),1:size(tmp,2)) = tmp;

% Normalise the wear measures for the sake of visualisation
wear_measures = wear_measures./max(max(wear_measures));

% Create overlay
overlay = uint8(zeros(block_rows*block_size,block_cols*block_size));

% Iterate through spatial block in overlay
idx_m = 0;
for m = 1:block_size:block_rows*block_size
    idx_m = idx_m + 1;
    idx_n = 0;
    for n = 1:block_size:block_cols*block_size
        idx_n = idx_n + 1;
        
        % Shade block according to wear measure for block
        overlay(m:m+block_size-1,n:n+block_size-1) = ...
            ones(block_size,block_size).*wear_measures(idx_m,idx_n)*255;
    end
end

% Display images in the same figure
figure;
imshow(I);
hold on;
h = imshow(overlay,hot);
set(h, 'AlphaData', 128);
title('Wear Measure Visualisation');

% Overlay grid-lines
draw_grid_lines(block_rows,block_cols,block_size);

end

