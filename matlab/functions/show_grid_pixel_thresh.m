function show_grid_pixel_thresh(I, block_size, thresh)

% Check threshold is an integer
if mod(thresh,1) ~= 0
    error('Threshold must be a whole number');
end
    
% Calculate block X and Y counts based on smallest image dimensions
block_rows = idivide(int32(size(I,1)), block_size, 'ceil');
block_cols = idivide(int32(size(I,2)), block_size, 'ceil');

% Extend image (with white) to make sure all blocks fit
tmp = I;
I = ones(block_rows*block_size,block_cols*block_size)*255;
I(1:size(tmp,1),1:size(tmp,2)) = tmp;

% Generate binary image
BW = I < graythresh(I);

% Generate empty array the size of the original image
mask = zeros(size(I,1),size(I,2));

% Iterate through blocks
for m = 1:block_size:block_rows*block_size
    for n = 1:block_size:block_cols*block_size
        
        % Get number of pixels in block
        num_pixels = sum(sum(BW(m:m+block_size-1,n:n+block_size-1)));
        
        % Draw a white block on the mask if there are enough pixels
        if num_pixels > thresh
            mask(m:m+block_size-1,n:n+block_size-1) = ones(block_size,block_size)*255;
        end
    end
end

% Display images in the same figure
figure;
imshow(I);
hold on;
h = imshow(mask);
set(h, 'AlphaData', 128);
title('Thresholded Blocks');

% Overlay grid-lines
draw_grid_lines(block_rows,block_cols,block_size);

end

