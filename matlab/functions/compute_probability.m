function probability = compute_probability( ...
    reference_curves, crime_scores, suspect_scores, ...%weight_map, ...
    delta_time_slots, time_slots)

% reference_curves : curves that are treated as the reference dataset for
%                    the class the suspect shoe belongs to (see
%                    generate_gt_curves for test data)
% crime_scores     : normalised crime scores from crime scene capture
% suspect_scores   : normalised wear scores from suspect capture
% weight_map       : weight map derived from capture data
% delta_time_slots : time slots that have elapsed between the crime and
%                    the suspect capture, set to 1 for testing purposes

% CHECK crime_scores AND suspect_scores ARE THE SAME SIZE
rows = size(crime_scores,1);
cols = size(crime_scores,2);

% Compute crime scores distances from curves
distances = zeros(size(crime_scores,1),size(crime_scores,2), ...
    time_slots);
for row = 1:rows
    for col = 1:cols
        for T = 1:time_slots
            if T < 2
                distances(row,col,T) = 0;
            else
                distances(row,col,T) = ...
                    abs( ...
                        feval(reference_curves{row,col},T) - ...
                        feval(reference_curves{row,col},1) - ...
                        suspect_scores(row,col));
            end 
        end
    end
end

% Take minimum ('best fit') crime distances
min_dist_time = zeros(size(distances,1), ...
    size(distances,2));
min_dist_val = zeros(size(distances,1), ...
    size(distances,2));
for row = 1:rows
    for col = 1:cols
        [min_dist_val(row,col),min_dist_time(row,col)] = ...
            min(distances(row,col,2:time_slots));
        
    end
end
min_dist_time = min_dist_time-1;

i = 0;

% Combine into probability, applying weight as we go
%count = 0;
%total = 0;
%for row = 1:rows
%    for col = 1:cols
%       if min_distances > 0
%          count = count + 1;
%          total = total + difference;%(difference * weight_map(row,col));
%       end
%    end 
%end
%probability = total/count;
    
end

