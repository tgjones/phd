function show_interleaved_data(tmpA,tmpB)

% TODO: Check tmpA and tmpB dimensions are the same

% Get dimensions from spatial component
block_rows   = size(tmpA.spatial_blocks,1);
block_cols   = size(tmpA.spatial_blocks,2);
block_size = size(tmpA.spatial_blocks,3);

% Initialise interleaved images
interleaved_fft   = zeros(block_rows*block_size,block_cols*block_size*2);
interleaved_ridge = zeros(block_rows*block_size,block_cols*block_size*2);

% Construct interleaved FFT output
for m = 1:block_rows
    for n = 1:block_cols*2
   
        % For odd numbers use a block from A
        if mod(n,2) == 1

            % Paste in FFT
            interleaved_fft((m-1)*block_size+1:(m-1)*block_size+block_size, ...
                (n-1)*block_size+1:(n-1)*block_size+block_size) = ...
                reshape(tmpA.fft_blocks(m,(n+1)/2,:,:),block_size,block_size);

            % Paste in ridges
            interleaved_ridge((m-1)*block_size+1:(m-1)*block_size+block_size, ...
                (n-1)*block_size+1:(n-1)*block_size+block_size) = ...
                reshape(tmpA.ridge_blocks(m,(n+1)/2,:,:),block_size,block_size);
            
        % For even numbers use a block from B
        else
            
            % Paste in FFT
            interleaved_fft((m-1)*block_size+1:(m-1)*block_size+block_size, ...
                (n-1)*block_size+1:(n-1)*block_size+block_size) = ...
                reshape(tmpB.fft_blocks(m,n/2,:,:),block_size,block_size);
            
            % Paste in ridges
            interleaved_ridge((m-1)*block_size+1:(m-1)*block_size+block_size, ...
                (n-1)*block_size+1:(n-1)*block_size+block_size) = ...
                reshape(tmpB.ridge_blocks(m,n/2,:,:),block_size,block_size);

        end
    end
end

% New figure to display interleaved data
figure;

% Draw interleaved FFT
subplot(1,2,1);
imshow(interleaved_fft,[]);
title('Interleaved FFT');
hold on;

% Horizontal lines
m = 0;
for k = 1:block_size:block_rows*block_size
    m = m+1;
    text(block_size/8,double(k)+(block_size/3), ... 
        num2str(m),'BackgroundColor',[1 1 1]);
    x = [1 block_cols*block_size*2];
    y = [k k];
    plot(x,y,'Color','w','LineStyle','-');
    plot(x,y,'Color','k','LineStyle',':');
end

% Vertical lines
n = 0;
for k = 1:block_size*2:block_cols*block_size*2
    n = n+1;
    text(double(k)+(block_size/8), block_size/3, ...
        num2str(n),'BackgroundColor',[1 1 1]);
    x = [k k];
    y = [1 block_rows*block_size];
    plot(x,y,'Color','w','LineStyle','-');
    plot(x,y,'Color','k','LineStyle',':');
end

% Draw interleaved ridges
subplot(1,2,2);
imshow(interleaved_ridge);
title('Interleaved Ridge');
hold on;

% Horizontal lines
m = 0;
for k = 1:block_size:block_rows*block_size
    m = m+1;
    text(block_size/8,double(k)+(block_size/3), ... 
        num2str(m),'BackgroundColor',[1 1 1]);
    x = [1 block_cols*block_size*2];
    y = [k k];
    plot(x,y,'Color','w','LineStyle','-');
    plot(x,y,'Color','k','LineStyle',':');
end

% Vertical lines
n = 0;
for k = 1:block_size*2:block_cols*block_size*2
    n = n+1;
    text(double(k)+(block_size/8), block_size/3, ...
        num2str(n),'BackgroundColor',[1 1 1]);
    x = [k k];
    y = [1 block_rows*block_size];
    plot(x,y,'Color','w','LineStyle','-');
    plot(x,y,'Color','k','LineStyle',':');
end

end

