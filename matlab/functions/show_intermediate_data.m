function show_intermediate_data(intermediate_data)
% This function takes the intermediate data struct from the wear_data
% function and visualises the three components (spatial, FFT and ridge) in
% a subplot.

% Get dimensions from spatial component
block_rows   = size(intermediate_data.spatial_blocks,1);
block_cols   = size(intermediate_data.spatial_blocks,2);
block_height = size(intermediate_data.spatial_blocks,3);
block_width  = size(intermediate_data.spatial_blocks,4);

% Ensure blocks are square
if block_width == block_height
    block_size = block_width;
else
    error('Blocks do not have equal dimensions');
end

% Underlays for visualisation
spatial = zeros(block_rows*block_size,block_cols*block_size);
fourier = zeros(block_rows*block_size,block_cols*block_size);
ridge   = false(block_rows*block_size,block_cols*block_size);

% (Re)construct spatial underlay
for m = 1:block_rows
    for n = 1:block_cols
        spatial((m-1)*block_size+1:(m-1)*block_size+block_size, ...
            (n-1)*block_size+1:(n-1)*block_size+block_size) = ...
            reshape(intermediate_data.spatial_blocks(m,n,:,:), ... 
                block_size,block_size);
    end
end

% Construct FFT underlay
for m = 1:block_rows
    for n = 1:block_cols
        fourier((m-1)*block_size+1:(m-1)*block_size+block_size, ...
            (n-1)*block_size+1:(n-1)*block_size+block_size) = ...
            reshape(intermediate_data.fft_blocks(m,n,:,:), ...
                block_size,block_size);
    end
end

% Construct ridge underlay
for m = 1:block_rows
    for n = 1:block_cols
        ridge((m-1)*block_size+1:(m-1)*block_size+block_size, ...
            (n-1)*block_size+1:(n-1)*block_size+block_size) = ...
            reshape(intermediate_data.ridge_blocks(m,n,:,:), ...
                block_size,block_size);
    end
end

% New figure for raw data
figure;

% Draw spatial vis
subplot(1,3,1);
imshow(spatial,[]);
draw_grid_lines(block_rows,block_cols,block_size);
title('Spatial');

% Draw FFT vis
subplot(1,3,2);
imshow(fourier);
draw_grid_lines(block_rows,block_cols,block_size);
title('FFT');

% Draw ridge vis
subplot(1,3,3);
imshow(ridge);
draw_grid_lines(block_rows,block_cols,block_size);
title('Ridges');

end

