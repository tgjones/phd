function candidate_score_distances = best_fit_distance(ground, ...
    crime_score,suspect_score,delta_time,time_slot_size,time_slot_count)

%% DEPRECATED

%%%% Pre-compute these

% Normalise ground truth
ground = (ground-2)/3;

% Create time column vector for each ground truth entry
T = (0:1:size(ground)-1)';

% Describe function to fit to ground truth data, the function
func = fittype({'exp(-x)','1'},'coefficients',{'a','c'});

% Fit a curve to the ground truth
curve = fit(T,ground,func);

%%%%

% Score difference
delta_score = abs(suspect_score - crime_score);

% Create vector for holding candidate scores
candidate_score_distances = zeros(time_slot_count);

% For each time slot
for i = 0:time_slot_count
    
    % Calculate time slot T0
    T0 = i * time_slot_size;

    % Get crime scene score at time slot T0
    curve_crime_score = feval(curve,T0);
    
    % Calculate candidate time
    T1 = T0 + delta_time;
    
    % Get candidate suspect score at T1
    curve_suspect_score = feval(curve,T1);
    
    % Calculate and save candidate score
    candidate_score_distances(i) = ...
        curve_suspect_score - curve_crime_score - delta_score;
    
end

end