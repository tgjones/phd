function score_distance = compute_curve_score_distance(curve, T, ...
    delta_time, suspect_score)

% T - delta_time must be >= 1
% CHECK THIS OR PRINT ERROR

% Get candidate suspect score at T1
score_distance = feval(curve,T) - feval(curve,T-delta_time) - ...
    suspect_score;
   