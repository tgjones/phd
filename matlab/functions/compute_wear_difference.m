function [diff,state] = compute_wear_difference(measures_A, measures_B)
% This function takes two data-sets output by wear_measure() and calculates
% the wear difference, which currently is literally A-B

% Get the largest dimensions
max_rows = max(size(measures_A,1),size(measures_B,1));
max_cols = max(size(measures_A,2),size(measures_B,2));

% Pad out measure arrays with zeros to maximum size
tmp_A = zeros(max_rows,max_cols);
tmp_B = zeros(max_rows,max_cols);
tmp_A(1:size(measures_A,1),1:size(measures_A,2)) = measures_A;
tmp_B(1:size(measures_B,1),1:size(measures_B,2)) = measures_B;
measures_A = tmp_A;
measures_B = tmp_B;

% Create state struct
state = struct;
state.both   = false(max_rows,max_cols); % A > 0, B > 0
state.a_only = false(max_rows,max_cols); % A > 0, B == 0
state.b_only = false(max_rows,max_cols); % A == 0, B > 0
state.empty  = false(max_rows,max_cols); % A == 0, B == 0

% Save flags
state.both   = (measures_A > 0)&(measures_B > 0);
state.a_only = (measures_A > 0)&(measures_B == 0);
state.b_only = (measures_A == 0)&(measures_B > 0);
state.empty  = (measures_A == 0)&(measures_B == 0);

% Compute wear differences
diff = measures_A-measures_B;

end

