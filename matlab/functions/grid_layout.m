function grid_layout(I, block_size, scale)

% Set default scale
if ~exist('scale','var')
    scale = 1;
end

% Firstly scale image if required
if scale ~= 1
    I = imresize(I, scale);
end
    
% Calculate block X and Y counts
block_rows = idivide(int32(size(I,1)), block_size, 'ceil');
block_cols = idivide(int32(size(I,2)), block_size, 'ceil');

% Extend image (with white) to make sure all blocks fit
tmp = I;
I = ones(block_rows*block_size,block_cols*block_size)*255;
I(1:size(tmp,1),1:size(tmp,2)) = tmp;

% Show grid layout
figure;
imshow(I,'Border','tight');
title('Grid layout');
draw_grid_lines(block_rows,block_cols,block_size);

end

