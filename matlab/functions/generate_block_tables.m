function [block_tables, min_block_table] = ...
    generate_block_tables(reference_curves, suspect, ...
    time_slots, delta_time)

% CHECK reference_curves X and Y are same size as suspect X and Y
block_tables = zeros(size(suspect,1),size(suspect,2),time_slots);

for row = 1:size(suspect,1)
    for col = 1:size(suspect,2)
        for T = 1:delta_time:time_slots
            if T < 2
                
                block_tables(row,col,T) = 0;
                
            else
                
                block_tables(row,col,T) = ...
                    abs(compute_curve_score_distance(reference_curves{row,col}, ...
                    T, delta_time, suspect(row,col)));
            end 
        end
    end
end

min_block_table = zeros(size(suspect,1),size(suspect,2));

for row = 1:size(suspect,1)
    for col = 1:size(suspect,2)

        min_block_table(row,col) = min(block_tables(row,col,2:time_slots));
        
    end
end

end

