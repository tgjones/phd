function result = Fourier_shoe3(I,DB_path,fmin,fmax,Succ_rot,stp,w_min,w_max,method)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          THIS IS AN UPDATE OF "Fourier_shoe2.m"   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Matching an input image I against a database (specified by 
%  the path: "DB_path") of shoeprint images using de Chazal's algorithm (IEEEtansPAMI paper)  


%Inputs:
%-------
%   I: The input image (query image, can have any size and can be of class uint8 or double.).
%   DB_path: Database location -----> must end with: "\" ----> See example bellow.
%   fmin, fmax: freq max and freq min; for the pass band filter 
%              0 <= fmin < 1, 
%              0 < fmax <= 1,
%              fmin < fmax
%   Succ_rot: may have two values 0 or 1:
%              Succ_rot=0 : No rotations applied
%              Succ_rot=1 : rotations in "Stp" degrees increment in the range 
%                           of [w_min w_max] degrees will be applied to the PSD of
%                           the input image 'I'(see Chazal's paper: IEEEtansPAMI)using the
%                           interpolation method specified by "method" witch is
%                           a string that can have one of these values:
%                           'nearest', 'bilinear', 'bicubic'.


%Output:
%-------
%   result: is a structure array containing the names of the reference database
%   images sorted from the Best match to the worst.


%Size, Class and format Support:
%-------------------------------
%   Database images MUST have the FORMAT 'jpg' and the SIZE 512x512
%   The input image can have any size and  can be of class uint8 or double.
%   


%Parameters used in Chazal's paper:
%----------------------------------
%   fmin=0.1/300;
%   fmax=0.125;
%   Succ_rot=1;
%   stp=1;
%   w_min=-30;
%   w_max=30;
   


%Example
%-------
%        I = imread('FFS0031_p58.jpg');
%        DB_path = 'D:\Shoeprints\F&F-Shoeprints\';
%        result = Fourier_shoe3(I,DB_path,0.1/300,0.125,1,1,-30,30,'bicubic');
%        J1=imread([DB_path result(1).name]); % Top Match
%        J2=imread([DB_path result(2).name]); % Second best match
%        figure, imshow(I)
%        figure, imshow(J1)
%        figure, imshow(J2)


%Note:
%-----
%   The updates are:
%   1) More input arguments: stp, w_min, w_max, method.
%   2) The new mask's center is N12=fix(N/2)+1 instead of N12=(N+1)/2.
%   3) The new mask's radii are: r1=(fmin*N)^2 and r2=(fmax*N)^2,
%      where fmin and fmax are in 'cycles-per-pixel'. In the case of
%      Chazal's paper: fmin= 0.1 cycles-per-inch = (0.1/300)cycles-per-pixel
%                      fmax=0.125 cycles-per-pixel
%   4) Removing the invertion of the background (from white to black)---> No effect! 
%      Note that: The test codes and GUI (ShoeGB) still perform background
%      invertion (from white to black).



ss=size(DB_path,2);
%if DB_path(ss)~='\'
%    DB_path=[DB_path '\'];  % DB_path must end with '\'
%end
if DB_path(ss)~='\'
    error('Database path "DB_path" must end with: "\"')
end



N=512; % Reference images' dimension is  512x512






%**************************************************************************
%**************  Part A: Input image transformation   *********************
%**************************************************************************

Id=double(I);

%--------------- Resizing the query image (Resampling-to get NxN)----------
sim=size(Id);
if (sim(1)~=N)|(sim(2)~=N) 
    img=Id;
    mxi=max(max(img)); 

    sz=size(img);
    mxs=max(sz);
    X=sz(1);
    Y=sz(2);
    
    y1=fix((mxs-Y)/2)+1;
    y2=(y1-1)+Y;
    
    x1=fix((mxs-X)/2)+1;
    x2=(x1-1)+X;
    
    
    img2=mxi*ones(mxs,mxs);
    img2(x1:x2,y1:y2)=img;
    
    Id = imresize(img2,[N N]); 
    
end
%--------------------------------------------------------------------------

%-------- the mask (for filtering) ----------------------------------------
mask=zeros(N,N);
%N12=(N+1)/2;
N12=fix(N/2)+1;
%Rmax=N/sqrt(2);
r1=(fmin*N)^2;
r2=(fmax*N)^2;
for i=1:N
  for j=1:N
    ry=(i-N12)^2+(j-N12)^2;
    if (ry > r1)&(ry < r2)
     mask(i,j)=1;
    end
  end
end
%--------------------------------------------------------------------------


%Id=255-Id;% inverting the background (from white to black)---> No effect!
    
moy=mean(mean(Id));
Id=Id-moy;  % zero-mean shoeprint image

mspec=abs(fft2(Id));

%------to avoid zero values when using the Logarithm:---
for iii=1:N
    for jjj=1:N
        if mspec(iii,jjj)==0
            mspec(iii,jjj)=1;
        end
    end
end
%--------------------------------------------------------


mspec=fftshift(mspec);
mspec=log(mspec.^2);

rr=0;
if Succ_rot==1 % rotations in "Stp" degrees increment in the range of [w_min w_max] degrees 
                     
    mspec_rot=[];
    for w=w_min:stp:w_max
        rr=rr+1;
        mspecr=imrotate(mspec,w,method,'crop');
        mspecr=mspecr.*mask;% filtering
        
        moy=mean(mean(mspecr)); 
        sd=sqrt(mean(mean((mspecr-moy).^2)));  
        mspecr=(mspecr-moy)/sd; 

        mspec_rot(:,:,rr)=mspecr;
        
    end
    mspec=mspec_rot;
    
else % No rotations 
    rr=rr+1;
    mspec=mspec.*mask;% filtering 
    moy=mean(mean(mspec)); 
    sd=sqrt(mean(mean((mspec-moy).^2))); % 
    mspec=(mspec-moy)/sd; 
end

%***************************  End of part A  ******************************
%**************************************************************************




%**************************************************************************
%*****   Part B: Transformations of the reference images + Matching  ******
%**************************************************************************

images=dir([DB_path '*.jpg']);
sz=size(images);
M=sz(1); %Data base Size 
c=[];
for i=1:M 
  img_name=images(i).name;
  img=double(imread([DB_path img_name]));
      
  %img=255-img; % inverting the background (from white to black)---> No effect!
  moy=mean(mean(img));
  img=img-moy;  % zero-mean shoeprint image 
  imgf=abs(fft2(img));
  
  %-------to avoid zero values when using the Logarithm----
  for iii=1:N
      for jjj=1:N
          if imgf(iii,jjj)==0
              imgf(iii,jjj)=1;
          end
      end
  end
  %--------------------------------------------------------
   
  imgf=fftshift(imgf);
  imgf=log(imgf.^2);
  imgf=imgf.*mask;% filtering 
      
  moy=mean(mean(imgf)); 
  sd=sqrt(mean(mean((imgf-moy).^2)));
  imgf=(imgf-moy)/sd; 
  
  for irr=1:rr
      cr(irr)=(sum(sum(imgf.*mspec(:,:,irr))))/(N*N); % correlation
  end
  
  c(i)=max(cr);

end
%***************************  End of part B  ******************************
%**************************************************************************




%**************************************************************************
%*****************   Part C: Generation of the output list  ***************
%**************************************************************************
result=struct('name', {});
minc=min(c)-1;
for j=1:M
    maxc=max(c);
    for i=1:M
         if c(i)==maxc
             result(j).name=images(i).name;
             c(i)=minc;
             break
         end
    end
end
%***************************  End of part C  ******************************
%**************************************************************************

