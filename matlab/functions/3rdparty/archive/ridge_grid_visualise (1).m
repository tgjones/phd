function ridge_grid_visualise(data)

figure;

% Get dimensions
blockXNum   = size(data,2);
blockYNum   = size(data,1);
blockWidth  = size(data,4);
blockHeight = size(data,3);

% Create black background
output = zeros(blockYNum*blockHeight,blockXNum*blockWidth);

% Iterate through blocks
M = 0;
N = 0;
for i = 1:blockHeight:blockYNum*blockHeight
    M = M+1;
    N = 0;
    for j = 1:blockWidth:blockXNum*blockWidth
        N = N+1;
        
        output(i:i+blockHeight-1,j:j+blockWidth-1) = ridge_grid_binary(data,N,M);
        
    end
end

% Show the generated image
imshow(output);

% Draw the grid lines
hold on;
coord_x = 0;
coord_y = 0;
for k = 1:blockWidth:blockXNum*blockWidth
    coord_x = coord_x+1;
    text(double(k)+(blockWidth/8), blockWidth/3, ...
        num2str(coord_x),'BackgroundColor',[1 1 1]);
    x = [k k];
    y = [1 blockYNum*blockHeight];
    plot(x,y,'Color','w','LineStyle','-');
    plot(x,y,'Color','k','LineStyle',':');
end
for k = 1:blockHeight:blockYNum*blockHeight
    coord_y = coord_y+1;
    text(blockHeight/8,double(k)+(blockHeight/3), ... 
        num2str(coord_y),'BackgroundColor',[1 1 1]);
    x = [1 blockXNum*blockWidth];
    y = [k k];
    plot(x,y,'Color','w','LineStyle','-');
    plot(x,y,'Color','k','LineStyle',':');
end

end

