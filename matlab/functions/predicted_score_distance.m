function calibrated_score_distance = predicted_score_distance(ground,...
    crime_score,suspect_score,delta_time)

% NOTE: This function assumes *UNnormalised* ground truth and 
% *calibrated* wear scores

 % Normalise ground truth
ground = (ground-2)/3;
 
% Create time column vector for each ground truth entry
T = (0:1:size(ground)-1)';

% Describe function to fit to ground truth data, the function
func = fittype({'exp(-x)','1'},'coefficients',{'a','c'});

% Fit a curve to the ground truth
curve = fit(T,ground,func);

% Find time that the crime scene score represents on the curve (if any)
root_curve = @(time) curve(time) - crime_score;
T_crime = fzero(root_curve,0);

% Special case if crime_score > max(curve)
if (isnan(T_crime))

    % All we can do is set T_crime to be max(T) -delta_time
    T_crime = max(T) - delta_time;

end
  
% Go forward in time by delta_time [max(T) when crime_score > max(curve)]
T_predicted = T_crime + delta_time;

% Get the curve score at the predicted time from
predicted_score = feval(curve,T_predicted);

% Calculate the score distance
calibrated_score_distance = abs(predicted_score - suspect_score);

end

