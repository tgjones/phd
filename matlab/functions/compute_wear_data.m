function output_data = compute_wear_data(I,block_size,pixel_thresh,peak_delta)
% TODO: Update docs
%
% THIS COMPUTES __DETAIL__ NOT WEAR MEASURES, NEED TO SUBTRACT TWO
% ADJACENT TIME SLOTS FOR ACTUAL RELATIVE WEAR AMOUNT
%
% This is the full implementation of the wear detection algorithm, and
% combines the following stages into one function:
%
% 1. Divide the spatial image into blocks, padding where necessary
% 2. Using some thresholding, flag 'empty' blocks
% 3. Compute the FFT of 'non-empty' blocks
% 4. Disregard FFT data below a threshold
% 5. Perform some Gaussian smoothing on FFT
% 6. Detect peaks in the two dimensions of the FFT
% 7. Convert these peaks to ridges using a neighborhood operation
%
% After these steps have been computed we save all the data in the struct
% 'intermediate_data' for wear measure processing. Each element of the 
% struct is a 4D array (block row, block column, 2D image). The struct has 
% the following contents:
%
%  - spatial_blocks : 'I' divided into blocks (grayscale)
%  - fft_blocks     : FFT PSD outputs for each image block (grayscale)
%  - ridge_blocks   : Ridge pixels from the FFT blocks (binary)
% 
% This data is then used to compute the individual wear measures for each
% spatial block and saved in 'wear_measures', a 3D array (block row, block
% column, measure). 
%
% To compute the measures, we use a 'distance_map' which is a 2D array the
% same size and one of the blocks. The simplist of these could be all ones,
% (but obviously this makes the normalisation a bit pointless) but equally
% a radial map could be used, or an abitrary convolution of other 
% functions.
%
% The measure is calculated (and normalised) using the
% following formula:
%
%        sum[ij]( ridge_blocks[ij] * distance_map[ij] )
%        ----------------------------------------------
%                 sum[ij]( ridge_blocks[ij] )

% Calculate block counts
block_rows = idivide(int32(size(I,1)),block_size,'ceil');
block_cols = idivide(int32(size(I,2)),block_size,'ceil');

% Pad image to make sure all blocks fit
tmp = I;
I = zeros(block_rows*block_size,block_cols*block_size);
I(1:size(tmp,1),1:size(tmp,2)) = tmp;

% Generate binary image for thresholding later
BW = I > graythresh(I);

% Calculate centre pixel coordinates
centre = [floor(block_size/2), floor(block_size/2)];

% Create FFT weighting map and circle mask
weighting_map = zeros(block_size,block_size);
circle_mask   = false(block_size,block_size);
for m = 1:block_size
    for n = 1:block_size

        % Compute radius at this pixel
        r = sqrt((m-centre(1))^2+(n-centre(2))^2);
        
        % NOTE:
        % Circle mask, after study with inverse FFTs Danny thinks we should
        % make the radius of the mask the same of the block_size, currently
        % its half. This should encompass all the frequencies that make the
        % pattern, rather than throwing away too much information.
        % Did we experiment with this at the time? Was there a problem with
        % noisy peaks.
        
        % Set circle mask pixel
        if r < 0.5*(block_size/2)
            circle_mask(m,n) = true;
        end
        
        % Set weighting map pixel value
        weighting_map(m,n) = r^2;
    end
end

% Normalise FFT weighting map and 
weighting_map = weighting_map./max(max(weighting_map));

% Create output data struct
output_data = struct;
output_data.wear_measures   = zeros(block_rows, block_cols);
output_data.wear_angle_sd   = zeros(block_rows, block_cols);
output_data.edge_blocks     = zeros(block_rows, block_cols, block_size, block_size);
output_data.spatial_blocks  = zeros(block_rows, block_cols, block_size, block_size);
output_data.fft_blocks      = zeros(block_rows, block_cols, block_size, block_size);
output_data.ridge_blocks    = false(block_rows, block_cols, block_size, block_size);

% Iterate through blocks
for m = 1:block_rows
    for n = 1:block_cols

        % Get grayscale spatial block
        spatial_gray = I((m-1)*block_size+1:(m-1)*block_size+block_size, ...
            (n-1)*block_size+1:(n-1)*block_size+block_size);
        
        % Get binary block for counting pixels
        spatial_binary = BW((m-1)*block_size+1:(m-1)*block_size+block_size, ...
            (n-1)*block_size+1:(n-1)*block_size+block_size);

        % Only do the rest if we are not dealing with an 'empty' block
        if sum(sum(spatial_binary)) > pixel_thresh
            
            % Do Sobel edge detection
            [edges,~,gv,gh] = ...
                edge(spatial_binary,'sobel','nothinning');
            
            % Compute edge angles and convert to degrees
            angles = atan2(gh,gv)*(180/pi);
          
            % Do FFT
            fourier = log(fftshift(abs(fft2(spatial_gray))).^2); 
            
            % Normalise FFT
            fourier = fourier./max(max(fourier));
              
            % Smooth FFT
            fourier = imfilter(fourier,fspecial('gaussian',[5 5]));
 
            % Mask out ultra high frequency information
            fourier = circle_mask.*fourier; 
            
            % Attain the Otsu of the masked pixels
            inds = find(fourier); 
            fft_thresh = graythresh(fourier(inds));

            % Threshold out noise using Otsu threshold attained above, and
            % reduce values of remaining data accordingly
            fft_otsu_mask = fourier > fft_thresh;
            fourier = fourier.*fft_otsu_mask - fft_thresh*fft_otsu_mask; 
                        
            % Binary images for peak detection pixels
            peaks_rising  = false(block_size,block_size);
            peaks_falling = false(block_size,block_size); 
                        
            % Iterate through falling diagonal lines to detect rising peaks
            for d = 1:(2*block_size)-1
            
                % Initialise variables for peak detection
                peak_min = Inf;
                peak_max = -Inf;
                peak_find_max = true;
            
                for p = 1:block_size-abs(d-block_size)
               
                    % Calculate coordinate of pixel
                    row = max(1,block_size-d+1)+(p-1);
                    col = max(1,d-block_size+1)+(p-1);

                    % Get scalar element
                    value = fourier(row,col);
                    
                    % Update current maximum
                    if value > peak_max
                        peak_max = value;
                        peak_max_idx = p;
                    end
                     
                    % Update current minimum
                    if value < peak_min
                        peak_min = value; 
                        peak_in_idx = p;
                    end
                    
                    % Currently searching for a maximum
                    if peak_find_max
                        if value < peak_max-peak_delta
                            peaks_rising(row,col) = true;
                            peak_min = value;
                            peak_min_idx = p;
                            peak_find_max = false;
                        end
                        
                    % Currently searching for a minimum
                    else
                        if value > peak_min-peak_delta
                            % Could save 'valleys' here if needed
                            peak_max = value;
                            peak_max_idx = p;
                            peak_find_max = true;
                        end
                    end 
                end                
            end
            
            % Iterate through rising diagonal lines to detect falling peaks
            for d = 1:(block_size*2)-1
            
                % Initialise variables for peak detection
                peak_min = Inf;
                peak_max = -Inf;
                peak_find_max = true;
            
                for p = 1:block_size-abs(d-block_size)
                    
                    % Calculate coordinate of pixel
                    %row = abs(block_size-(max(1,block_size-d+1)+(p-1))+1);
                    row = min(d,block_size)-(p-1);
                    col = max(1,d-block_size+1)+(p-1);
                    
                    % Get scalar element
                    value = fourier(row,col);
                    
                    % Update current maximum
                    if value > peak_max
                        peak_max = value;
                        peak_max_idx = p;
                    end
                    
                    % Update current minimum
                    if value < peak_min
                        peak_min = value;
                        peak_min_idx = p;
                    end
                    
                    % Currently searching for a maximum
                    if peak_find_max
                        if value < peak_max-peak_delta
                            peaks_falling(row,col) = true;
                            peak_min = value;
                            peak_min_idx = p;
                            peak_find_max = false;
                        end
                        
                    % Currently searching for a minimum
                    else
                        if value > peak_min-peak_delta
                            % Could save 'valleys' here if needed
                            peak_max = value;
                            peak_max_idx = p;
                            peak_find_max = true;
                        end
                    end
                end
            end
             
            % TODO: Try a close with diagonal elements x 2
            
            % Specify (constant) ridge window block radius
            %ridge_window_radius = 2;
            
            % Filter out non-ridges (peaks without nearby peaks)
%             for o = 1:block_size
%                 for p = 1:block_size
%                     
%                     % Get horizontal window values
%                     window_horiz = peaks_horiz(max([o-ridge_window_radius 1]):min([o+ridge_window_radius block_size]), ...
%                         max([p-ridge_window_radius 1]):min([p+ridge_window_radius block_size]));
%                     
%                     % Get vertical window values
%                     window_vert = peaks_vert(max([o-ridge_window_radius 1]):min([o+ridge_window_radius block_size]), ...
%                         max([p-ridge_window_radius 1]):min([p+ridge_window_radius block_size]));
%                     
%                     % If we don't have any neighbours turn off the pixel
%                     if sum(sum(window_horiz)) == 1
%                         peaks_horiz(o,p) = false;
%                     end
%                     if sum(sum(window_vert)) == 1
%                         peaks_vert(o,p) = false;
%                     end
%                     
%                 end
%             end 

            % Open to filter out noise on the falling diagonals
            peaks_falling = ...
                imopen(peaks_falling, [0 1 0; 0 1 0; 0 0 1]) | ...
                imopen(peaks_falling, [1 0 0; 0 1 0; 0 0 1]) | ...
                imopen(peaks_falling, [1 0 0; 0 1 0; 0 1 0]) | ...
                imopen(peaks_falling, [1 0 0; 0 1 1; 0 0 0]);
            
            % Open to filter out noise on the rising diagonals
            peaks_rising = ...
                imopen(peaks_rising, [0 1 0; 0 1 0; 1 0 0]) | ...
                imopen(peaks_rising, [0 0 1; 0 1 0; 1 0 0]) | ...
                imopen(peaks_rising, [0 0 1; 0 1 0; 0 1 0]) | ...
                imopen(peaks_rising, [0 0 1; 1 1 0; 0 0 0]);
            
            % OR the horizontal and vertical ridges together
            ridges = peaks_falling|peaks_rising;                 

            % Save intermediate data
            output_data.edge_blocks(m,n,:,:)  = edges;
            output_data.fft_blocks(m,n,:,:)   = fourier;
            output_data.ridge_blocks(m,n,:,:) = ridges;
            
            % Compute the standard deviation of angles for the block
            output_data.wear_angle_sd(m,n) = std(angles(:));
                          
            % Compute final wear measure for block
            output_data.wear_measures(m,n) = ...
                sum(sum(ridges.*weighting_map)) / sum(sum(ridges));
            
        end
    end 
end