function curve = compute_gt_curve(ground)

% Normalise ground truth
ground = (ground-2)/3;

% Create time column vector for each ground truth entry
T = (0:1:size(ground)-1)';

% Describe function to fit to ground truth data, the function
func = fittype({'exp(-x)','1'},'coefficients',{'a','c'});

% Fit a curve to the ground truth
curve = fit(T,ground,func);

end

