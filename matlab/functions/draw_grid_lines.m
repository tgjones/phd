function draw_grid_lines(block_rows, block_cols, block_size)
% This function draws and labels grid lines on top of a figure, this helps
% when visualising block-based image processing algorithms.

hold on;

% Horizontal lines
m = 0;
for k = 1:block_size:block_rows*block_size
    m = m+1;
    text(block_size/8,double(k)+(block_size/3), ... 
        num2str(m),'BackgroundColor',[1 1 1]);
    x = [1 block_cols*block_size];
    y = [k k];
    plot(x,y,'Color','w','LineStyle','-');
    plot(x,y,'Color','k','LineStyle',':');
end

% Vertical lines
n = 0;
for k = 1:block_size:block_cols*block_size
    n = n+1;
    text(double(k)+(block_size/8), block_size/3, ...
        num2str(n),'BackgroundColor',[1 1 1]);
    x = [k k];
    y = [1 block_rows*block_size];
    plot(x,y,'Color','w','LineStyle','-');
    plot(x,y,'Color','k','LineStyle',':');
end

end

