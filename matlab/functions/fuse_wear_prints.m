function [C,intermediate_data] = fuse_wear_prints(A,B,block_size,mr_range,...
    pixel_thresh)

% Convert RGB to grayscale
if size(A,3) > 1
    A = rgb2gray(A);
end
if size(B,3) > 1
    B = rgb2gray(B);
end

% Find maximum dimensions of A and B
max_rows = max(size(A,1),size(B,1));
max_cols = max(size(A,2),size(B,2));

% Calculate block counts for maximum dimensions
block_rows = idivide(int32(max_rows),block_size,'ceil');
block_cols = idivide(int32(max_cols),block_size,'ceil');

% Re-calculate maximum dimensions
max_rows = block_rows*block_size;
max_cols = block_cols*block_size;

% Invert prints so contact pixels are represented by high values
A = 255-A;
B = 255-B;

% Zero-pad A to maximum size
tmp_A = zeros(max_rows,max_cols);
tmp_A(1:size(A,1),1:size(A,2)) = A;
A = tmp_A;

% Zero-pad B but add a border to allow for micro-registration
tmp_B = zeros(max_rows+mr_range*2,max_cols+mr_range*2);
tmp_B(mr_range+1:mr_range+size(B,1),mr_range+1:mr_range+size(B,2)) = B;
B = tmp_B;

% Create an array for the output image (C)
C = zeros(max_rows,max_cols);

% Create intermediate data struct
intermediate_data = struct;
intermediate_data.mask_diff      = zeros(block_rows,block_cols);
intermediate_data.mask_diff_norm = zeros(block_rows,block_cols);

% Iterate through blocks
for m = 1:block_rows
    for n = 1:block_cols
        
        % Extract raw blocks (taking into account B's border)
        block_A = A((m-1)*block_size+1:(m-1)*block_size+block_size,...
            (n-1)*block_size+1:(n-1)*block_size+block_size);
        block_B = B(mr_range+(m-1)*block_size+1:mr_range+(m-1)*block_size+block_size,...
            mr_range+(n-1)*block_size+1:mr_range+(n-1)*block_size+block_size);

        % Get local micro-registration offsets for block
        [x,y] = micro_registration(block_A,block_B,mr_range,pixel_thresh);

        % Extract offset image from original, bordered, B
        block_B = B(mr_range+(m-1)*block_size+1-y:mr_range+(m-1)*block_size+block_size-y,...
            mr_range+(n-1)*block_size+1-x:mr_range+(n-1)*block_size+block_size-x);
        
        % Get Otsu thresholds for A and B
        thresh_A = graythresh(block_A/255)*255;
        thresh_B = graythresh(block_B/255)*255;
        
        % Get binary version of the blocks
        binary_A = block_A > thresh_A;
        binary_B = block_B > thresh_B; 
        
        % Only do anything else if there are pixels we care about
%        if ((sum(binary_A(:)) >= pixel_thresh) && ...
%            (sum(binary_B(:)) >= pixel_thresh))           

            % Get masks for blocks from local Otsu
            mask_A = block_A > thresh_A;
            mask_B = block_B > thresh_B;

            % Compute mean graylevels of contact pixels
            mean_A = sum(sum(block_A.*mask_A))/sum(sum(mask_A));
            mean_B = sum(sum(block_B.*mask_B))/sum(sum(mask_B));

            % Now combine A and B as they are the 'same' brightness, as per
            % C = (1-mask).*A + mask.*B
            
            % Scale 'less dominant' (less contact pixels) brightness
            if (sum(mask_B(:)) > sum(mask_A(:)))

                % Get a mask of pixels that were in A but not B
                mask = mask_A&(1-mask_B);
                
                % Save number of pixels being copied from A to B (-ve)
                intermediate_data.mask_diff(m,n) = -sum(mask(:));
                
                % Save normalised version (against total contact pixels)
                intermediate_data.mask_diff_norm(m,n) = -(sum(mask(:))/sum(sum(mask_A|mask_B)));
                
                % Compute scale factor
                scale = mean_B/mean_A;
                block_C = (1-mask).*block_B + mask.*(block_A*scale);
                
            else
                
                % Get a mask of pixels that were in B but not A
                mask = mask_B&(1-mask_A);
                
                % Save number of pixels being copied from B to A (+ve)
                intermediate_data.mask_diff(m,n) = sum(mask(:));
                
                 % Save normalised version (against total contact pixels)
                intermediate_data.mask_diff_norm(m,n) = sum(mask(:))/sum(sum(mask_A|mask_B));
               
                % Compute scale factor
                scale = mean_A/mean_B;
                block_C = (1-mask).*block_A + mask.*(block_B*scale);
 
            end

            % Place fused block back in the original C image
            C((m-1)*block_size+1:(m-1)*block_size+block_size,...
                (n-1)*block_size+1:(n-1)*block_size+block_size) = block_C;
        
        % This particular block was 'empty'
%        else
            
            % Insert a zerod block into C
%            C((m-1)*block_size+1:(m-1)*block_size+block_size,...
%                (n-1)*block_size+1:(n-1)*block_size+block_size) = ...
%                zeros(block_size,block_size);
            
%        end 
    end
end

end

