function [xa, ya, xb, yb] = input_alignment_points(Ia, Ib)
    
    % Convert to grayscale
    if size(Ia,3) > 1
        Ia = rgb2gray(Ia);
    end
    if size(Ib,3) > 1
        Ib = rgb2gray(Ib);
    end

    % Get pivot points from image Ia
    figure; imshow(Ia);
    title('Please input two alignment points for image A...');
    [xa,ya] = ginput(2); close;

    % Get pivot points from image Ib
    figure; imshow(Ib);
    title('Please input the same two alignment points (in the same order) for image B...');
    [xb,yb] = ginput(2); close;

end