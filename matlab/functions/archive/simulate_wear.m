function W = simulate_wear_02( R, P, s )
%SIMULATE_WEAR Simulate wear of the shoesole.
%   This function accepts the reference image of a shoe pattern (R), by
%   reference image we mean noise-free and sampled prior to wear, and a
%   wear amount (s). s represents the number of 1000 steps the algorithm is
%   is to simulate.
%
%   Usage: W = SIMULATE_WEAR(R, s);
%
%     W: Worn image
%     R: Reference image
%     P: Pressure map
%     s: Wear amount in 1000s of steps

% Get dimensions
Rm = size(R,1);
Rn = size(R,2);

% Convert pressure map to double if need be
if isinteger(P)
    P = double(P)/255;
end

% Normalise pressure values
P = (P-min(min(P)))/(max(max(P))-min(min(P)));

% Get unique pressure layer scalars
l = unique(P);

% Get layer count (TODO: Actually detect 1 rather than using -1)
Lc = size(l,1)-1;

% Preallocate layer array
L = repmat(logical(false), [Rm, Rn, Lc]);

% Threshold layer masks
for i = 1:Lc
    L(:,:,i) = ~im2bw(P,l(i));
end

% Start with reference print on first pass
W = 255-R;

% Do erode pass for each layer
for i = 1:Lc
    
    % Copy worn image for processing
    Wc = W;
    
    % For every 50,000 steps do additional wear pass
    % TODO: This might need to be exponential
    for j = 1:(s/50)+1

        % Clsoe
        Wc = imdilate(Wc, strel('square', 4));
        Wc = imerode(Wc, strel('square', 4));
        
        % Erode more on the vertical (assumption on print orientation made)
        Wc = imerode(Wc, strel('rectangle', [3 2]));
        
        % Dilate back evenly so only a small amount of wear remains
        Wc = imdilate(Wc, strel('square', 2));
        
    end

    % Mask out wear to worn image
    W(L(:,:,i)) = Wc(L(:,:,i));    

end

% Blur to make process seem more natural
%f = fspecial('gaussian',10);
%W = imfilter(W,f);

