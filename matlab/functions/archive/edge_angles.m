function [ angles ] = edge_angles(I,blocks)

    % Assume blocks is a squre number
    % TODO: Test this
    % NOTE: depracated

    % Calculate dimensions of image, block and sub-block
    imageWidth  = size(I, 2);
    imageHeight = size(I, 1);
    blockWidth  = round(imageWidth / blocks / 2);
    blockHeight = round(imageHeight / blocks / 2);
    
    % Preallocate block angle array
    angles = zeros(blocks/2:blocks/2);
    
    % Iterate through image blocks
    for blockRow = 1 : blocks / 2
        for blockCol = 1 : blocks / 2
 
            % Get the block if image data
            block = I((blockRow-1)*blockHeight+1: blockRow*blockHeight+1, ...
                (blockCol-1)*blockWidth+1:blockCol*blockWidth+1);
            
            % Get sobel gradiant values of block
            [BW,thresh,gv,gh] = edge(block,'sobel');
            
            % Calculate and save predominant angle of block
            angles(blockRow,blockCol) = atan2(gv,gh);
      
        end
    end
end

