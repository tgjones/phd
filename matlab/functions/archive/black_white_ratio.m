function ratio = black_white_ratio( I )

white = sum(sum(I));
black = sum(sum(I == 0));

ratio = black / white;


end

