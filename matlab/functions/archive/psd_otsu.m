function [psd_trimmed,thresh] = psd_otsu(psd, thresh_offset)

% Default to no threshold offset
if ~exist('thresh_offset','var')
    thresh_offset = 0;
end

% Normalise PSD
psd = psd./max(max(psd));

% Get threshold value and mask
thresh = graythresh(psd) + thresh_offset;
mask = psd > thresh;

% Discard thresholded data
psd_trimmed = (psd .* mask) - thresh;