function L = logical_convhull(BW)
% LOGICAL_CONVHULL accepts a logical array and produces a polygon that
% encompasses all the 1's.

[r,c] = find(BW);
K = convhull(r,c);
L = [c(K), (r(K))];
