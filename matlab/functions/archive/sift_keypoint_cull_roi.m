function [ f, d ] = sift_keypoint_cull_roi( f, d, ROI )
% CULL_SIFT_KEYPOINTS_ROI parses the SIFT frames and discriptors passed in
% as arguments and culls any that lie outside the polygon defined in ROI.
%
% Usage:
%   [ f, d] = CULL_SIFT_KEYPOINTS_ROI( f, d, ROI )
%
%   f - SIFT frames to check (and cull)
%   d - SIFT descriptors to cull (based on Cartesian information in f)
%
% CULL_SIFT_KEYPOINTS_ROI is designed to work with data output by functions
% in VLFeat library (http://www.vlfeat.org/).

IN = inpolygon(f(1,:), f(2,:), ROI(:,1), ROI(:,2));

f = f(:,IN);
d = d(:,IN);

end

