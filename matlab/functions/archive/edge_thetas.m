function [ blockThetas ] = edge_thetas(I,blocks,thresh)

    % Assume blocks is a squre number
    % TODO: Test this

    % Calculate dimensions of image, block and sub-block
    imageWidth  = size(I, 2);
    imageHeight = size(I, 1);
    blockWidth  = round(imageWidth / (blocks / 2));
    blockHeight = round(imageHeight / (blocks / 2));
    
    % Preallocate block angle array
    blockThetas = zeros(blocks/2:blocks/2);
    
    % Iterate through image blocks
    for blockRow = 1 : blocks / 2
        for blockCol = 1 : blocks / 2
 
            % Get the block if image data
            block = I(blockRow-1*blockHeight:blockRow*blockHeight, ...
                blockCol-1*blockWidth:blockCol*blockWidth);
            
            % Get sobel gradiant values of block
            gh = imfilter(block,fspecial('sobel') /8,'replicate');
            gv = imfilter(block,fspecial('sobel')'/8,'replicate');
            
            % Threshold gradiant values
            gh = gh(gh > thresh);
            gv = gv(gv > thresh);
            
            % Calculate and save predominant angle of block
            blockThetas(blockRow,blockCol) = atan(gh/gv);
            
        end
    end
end

