function data = sift_grid_match(I1, I2, blockHeight, blockWidth, scale)

% Firstly scale images
I1 = imresize(I1, scale);
I2 = imresize(I2, scale);

% Calculate block X and Y counts based on smallest image dimensions
blockXNum = idivide(int32(min([size(I1,2) size(I2,2)])), blockWidth, 'floor');
blockYNum = idivide(int32(min([size(I1,1) size(I2,1)])), blockHeight, 'floor');

% Create output struct
data = struct;
data.match_scores = cell(blockYNum,blockXNum);
data.displacement = cell(blockYNum,blockXNum);
data.scale_change = cell(blockYNum,blockXNum);
data.orientation_change = cell(blockYNum,blockXNum);

% Loop compute SIFT
for i = 1:blockYNum
    for j = 1:blockXNum
        
        % Save blocks
        I1_block = I1((i-1)*blockHeight+1:(i-1)*blockHeight+blockHeight+1, ...
            (j-1)*blockWidth+1:(j-1)*blockWidth+blockWidth+1);
        I2_block = I2((i-1)*blockHeight+1:(i-1)*blockHeight+blockHeight+1, ...
            (j-1)*blockWidth+1:(j-1)*blockWidth+blockWidth+1);
        
        % Do SIFT feature detection
        [I1_frames,I1_descriptors] = vl_sift(im2single(I1_block));
        [I2_frames,I2_descriptors] = vl_sift(im2single(I2_block));
       
        % Get matches and scores
        [matches,scores] = vl_ubcmatch(I1_descriptors,I2_descriptors);
        
        % Save scores in output
        if ~isempty(scores)
            data.match_scores{i,j} = scores;
        end
        
        if ~isempty(matches)

            % Prune frame data to matched frames only
            I1_frames = I1_frames(:,matches(1,:));
            I2_frames = I2_frames(:,matches(2,:));

            % Get differences
            frame_diff = I2_frames-I1_frames;
            
            % Save displacement information (X and Y)
            data.displacement{i,j} = frame_diff(:,1:2);
            
            % Save scale change information
            data.scale_change{i,j} = frame_diff(:,3);
            
            % Sace orientation change information
            data.orientation_change{i,j} = frame_diff(:,4);
           
        end
    end
end

end


