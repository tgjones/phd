function P = estimate_pressure_map( R, f, b, t )
%SIMULATE_WEAR Simulate wear of the shoesole.
%   This function accepts the reference image of a shoe pattern (R), by
%   reference image we mean noise-free and sampled prior to wear, and a
%   wear amount (s). s represents the number of 1000 steps the algorithm is
%   is to simulate.
%
%   Usage: W = ESTIMATE_PRESSURE_MAP(R, D0);
%
%     P  : Greyscale pressure map
%
%     R  : Reference shoeprint image
%     f  : Gaussian filter variable
%     b  : Number of colour bands (not including background)
%     t  : Mask threshold
%
% Credit: The Guassian code in this function was taken from Gonzalez et al.

% Extract image dimensions
M = size(R,1);
N = size(R,2);
        
% Initilise meshgrid values
u = 0:(M - 1);
v = 0:(N - 1);

% Compute the indices for use in meshgrid
idx = find(u > M/2);
u(idx) = u(idx) - M;
idy = find(v > N/2);
v(idy) = v(idy) - N;

% Compute the meshgrid arrays
[V, U] = meshgrid(v, u);

% Compute the distances D(U, V)
D = sqrt(U.^2 + V.^2);

% Calculate Gaussian filter matrix
H = exp(-(D.^2)./(2*(f^2)));

% Obtain the FFT of the padded input
F = fft2(R, M, N);

% Perform filtering
P = real(ifft2(H.*F));

% Crop to original size
P = P(1:M, 1:N);

% Posterize to create banding
P = round((P-min(P(:))) / (max(P(:)) - min(P(:))) * (b));

% Contrast stretch and convert to integer
P = uint8(((P-min(min(P)))/(max(max(P))-min(min(P))))*255);

% Threshold reference to attain logical image
%BW = R < t;

% Use convhull to determine outer polygon
% TODO: Use concave hull for better 'hugging'
%[r,c] = find(BW);
%K = convhull(r,c);

% Generate mask from polygon
%H = poly2mask(c(K), r(K), N, M);

% Clip outer settings
%P = (P.*H) - (H * max(max(P)));


