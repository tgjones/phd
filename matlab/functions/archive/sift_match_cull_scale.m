function [ m_out ] = sift_match_cull_scale( fa, fb, m_in, thresh )
%SIFT_MATCH_CULL_SCALE Cull SIFT matches based on difference of scale.

THRESH = ~(fa(3,m_in(1,:)) + thresh > fb(3,m_in(2,:)) & ...
    (fa(3,m_in(1,:)) - thresh > fb(3,m_in(2,:))));

m_out = m_in(:,THRESH);

end

