function matches = harris_match(Ia,Ib,r_max,r_weight,s_max,s_weight,...
    o_max,o_weight)

    % Convert to grayscale
    if size(Ia,3) > 1
        Ia = rgb2gray(Ia);
    end
    if size(Ib,3) > 1
        Ib = rgb2gray(Ib);
    end

    % Extract features             
    fa = vl_sift(im2single(Ia));
    fb = vl_sift(im2single(Ib));
    
    % Manual input of alignment points
    [xa, xb, ya, yb] = input_alignment_points(Ia, Ib);

    % Infer transform between input points
    tform = cp2tform([xb(1) yb(1); xb(2) yb(2)], ...
        [xa(1) ya(1); xa(2) ya(2)], 'nonreflective similarity');
    
    % Transform Ib's features to match Ia
    [fb(1,:) fb(2,:)] = tformfwd(tform, fb(1,:), fb(2,:));
    
    % Initialise loop counters
    a_idx = 0;
    m_idx = 0;
    
    % For each feature in fa
    for a = fa(:,:)
        
        % Calculate distances of a from all features from b
        fb_r = sqrt(((fb(1,:)-a(1)).^2)+((fb(2,:)-a(2)).^2));
        
        % Get indices of r values that are less than the max
        FB_R_IDX = fb_r < r_max;
        %FB_R_IDX = fb_r < ((max(fb_r)-min(fb_r))/2);

        % Calculate difference in scale of a from all features from b
        fb_s = fb(3,:)-a(3);
        
        % Get indices of s values that are less than the max
        FB_S_IDX = fb_s < s_max;
        %FB_S_IDX = fb_s < ((max(fb_s)-min(fb_s))/2);
        
        % Calculate difference in orientation of a from all features from b
        fb_o = fb(4,:)-a(4);
        
        % Get indices of o values that are less than the max
        FB_O_IDX = fb_o < o_max;
        %FB_O_IDX = fb_o < ((max(fb_o)-min(fb_o))/2);
        
        % Filter (AND) above indices
        FB_IDX = FB_R_IDX & FB_S_IDX & FB_O_IDX;
        
        % Get actual index numbers
        fb_idx = find(FB_IDX);
        
        % If there are some matches
        if size(fb_idx) > 0
            m_idx=m_idx+1;
            
            % Calculate measures
            measures = (fb_r(FB_IDX).*r_weight)...
                .*(fb_s(FB_IDX).*s_weight)...
                .*(fb_o(FB_IDX).*o_weight);

            % Get highest measure
            % TODO: Might be the lowest!
            [measure,idx] = max(measures);

            % Save match
            matches(m_idx) = [a_idx, fb_idx(idx), measure];
        end
        
        % Increment index count
        a_idx=a_idx+1;
    end
end

