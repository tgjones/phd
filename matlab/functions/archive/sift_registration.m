function [ I ] = sift_registration( Ib, Iu )
% Ib: base image
% Iu: unregistered image
% I : Iu aligned with Ib

% Extract SIFT keypoints
[Ibf, Ibd] = vl_sift(im2single(Ib));
[Iuf, Iud] = vl_sift(im2single(Iu));

% Perform UBC descriptor match to establish unique matches based on
% descriptor information, and elimate the majority of false positives.
[matches, scores] = vl_ubcmatch(Ibd, Iud); % TODO: Tweak tolerance (default 1.5)

end

