function map = psd_weighting_map(type, M, N)

% Currently only odd sizes are supported (for centre pixel)
if (mod(M,2) == 0) || (mod(N,2) == 0)
    error('Currently only odd sizes are supported');
end

% Create output array
map = zeros(M, N);

% Calculate centre pixel coordinates
centre = [floor(M/2), floor(N/2)];

% Radial distance map
if strcmp(type,'radial')

    for m = 1:M
        for n = 1:N

            % Compute scalar for pixel
            map(m,n) = (m-centre(1))^2 + (n-centre(2))^2;
            
        end
    end
    
% X/Y Minimum distance map
elseif strcmp(type,'xymin')
    
    map = zeros(M-centre(1)-1,N-centre(2)-1);
    for m = 1:M
        for n = 1:N

            % Compute scalar for pixel
            map(m,n) = min([abs(m-centre(1)) abs(n-centre(2))])^2; 

        end
    end

% X/Y Minimum distance map
elseif strcmp(type,'xyminplusradial')
    
    map = zeros(M-centre(1)-1,N-centre(2)-1);
    for m = 1:M
        for n = 1:N

            % Compute scalar for pixel
            map(m,n) = (min([abs(m-centre(1)) abs(n-centre(2))]) + sqrt((m-centre(1))^2 + (n-centre(2))^2))^2; 

        end
    end
    
end


end

