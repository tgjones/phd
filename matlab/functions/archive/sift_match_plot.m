function sift_match_plot(Ia,Ib,fa,fb,m)

% Scale tallest image and modify coordinate for display accordingly
if size(Ia,1) > size(Ib,1)
    sc = size(Ib,1)/size(Ia,1);
    Ia = imresize(Ia, [size(Ib,1), size(Ia,2) * sc]);
    fa = fa * sc;    
elseif size(Ib,1) > size(Ia,1)
    sc = size(Ia,1)/size(Ib,1);
    Ib = imresize(Ib, [size(Ia,1), size(Ib,2) * sc]);
    fb = fb * sc;
end

% Shift x coordinates of Ib features based on Ia width
fb(1,:) = fb(1,:) + size(Ia,2);

% Extract coordinates for lines
xa = fa(1,m(1,:));
xb = fb(1,m(2,:));
ya = fa(2,m(1,:));
yb = fb(2,m(2,:));

% Display images side-by-side
clf;
%figure(1);
imshow(cat(2, Ia, Ib),[],'Border','tight');
axis equal;
axis off;

% Draw lines between matches
hold on;
h = line([xa ; xb], [ya ; yb]);
set(h,'linewidth', 1, 'color', 'b');

% Plot features on Ia
h = vl_plotframe(fa(:,m(1,:)));
set(h,'color','r','linewidth',1);

% Plot features on Ib
h = vl_plotframe(fb(:,m(2,:)));
set(h,'color','r','linewidth',1);
