function psd = fft_grid_fourier(data, blockX, blockY)

% Extract PSD from grid data
psd = reshape(data.psd_fourier(blockY,blockX,:,:), ...
    size(data.psd_fourier(blockY,blockX,:,:),3), ...
    size(data.psd_fourier(blockY,blockX,:,:),4));

end

