function [thetaa, thetab, ra, rb] = measure_alignment_points(xa, xb, ya, yb)

    % Calculate angle in radians
    thetaa = atan((ya(2) - ya(1)) / (xa(2) - xa(1)));
    thetab = atan((yb(2) - yb(1)) / (xb(2) - xb(1)));
    
    % Calculate the length of r
    ra = sqrt(((ya(2) - ya(1))^2) + ((xa(2) - xa(1))^2));
    rb = sqrt(((yb(2) - yb(1))^2) + ((xb(2) - xb(1))^2));

end

