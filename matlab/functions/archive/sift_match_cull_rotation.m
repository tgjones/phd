function [ m_out ] = sift_match_cull_rotation( fa, fb, m_in, thresh )
%SIFT_MATCH_CULL_ROTATION Cull SIFT matches based on difference of rotation.

THRESH = ~(fa(4,m_in(1,:)) + thresh > fb(4,m_in(2,:)) & ...
    (fa(4,m_in(1,:)) - thresh > fb(4,m_in(2,:))));

m_out = m_in(:,THRESH);

end

