function [ angle_pyramid ] = edge_angle_pyramid(I,floor,spacing,layers,thresh)

    % Generate pyramid array with edge angles in
    angle_pyramid = cell(layers,1);
    for n = 1:layers
        angle_pyramid{n} = edge_angles(I,floor+(n-1*spacing),thresh);
    end

end

