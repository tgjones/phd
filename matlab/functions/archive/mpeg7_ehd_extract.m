function [  ] = mpeg7_ehd_extract(I, blocks)

    % IMPORTANT NOTE: This is not finished!

    % TODO:
    % This calculates blocks for a square image, we need to proportionally
    % divide the image up instead.

    % Calculate dimensions of image, block and sub-block
    imageWidth  = size(I, 2);
    imageHeight = size(I, 1);
    blockWidth  = round(imageWidth / (blocks / 2));
    blockHeight = round(imageHeight / (blocks / 2));
    subBlockWidth  = round(blockWidth / 2);
    subBlockHeight = round(blockHeight / 2);
    
    % Define filter co-efficients
    filterVertical       = [1 -1; 1 -1];
    filterHorizontal     = [1 1; -1 -1];
    filter45Diagonal     = [sqrt(2) 0; 0 sqrt(2)];
    filter135Diagonal    = [0 sqrt(2); sqrt(2) 0];
    filterNonDirectional = [2 2; 2 2];
    
    % Iterate through image blocks
    for blockRow = 1 : blocks / 2
        for blockCol = 1 : blocks / 2
 
            % Get the block if image data
            block = I(blockRow-1*blockHeight: blockRow*blockHeight, ...
                blockCol-1*blockWidth:blockCol*blockWidth);
            
            % Pre-allocate mean array
            meanGrays = zeros(subBlocks/2:subBlocks/2);
            
            % Iterate through sub blocks (quadrants)
            for subBlockRow = 1 : 2
                for subBlockCol = 1 : 2
                    
                % Get the sub-block of image data
                subBlock = block(subBlockRow-1*subBlockHeight: ...
                    subBlockRow*subBlockHeight, ...
                    subBlockCol-1*subBlockWidth: ...
                    subBlockCol*subBlockWidth);
                
                % Save the mean graylevel
                meanGrays(subBlockRow,subBlockCol) = mean(subBlock);
                    
                end
            end
            
            % Calculate the magnitudes
            magnitudeVertical       = meanGrays .* filterVertical;
            magnitudeHorizontal     = meanGrays .* filterHorizontal;
            magnitude45Diagonal     = meanGrays .* filter45Diagonal;
            magnitude135Diagonal    = meanGrays .* filter135Diagonal;
            magnitudeNonDirectional = meanGrays .* filterNonDirectional;
            
            vec = [magnitudeVertical, magnitudeHorizontal, ...
                magnitude45Diagonal, magnitude135Diagonal, ...
                magnitudeNonDirectional];
            value = max(vec);
            index = find(vec == value);
            
        end
    end

end

