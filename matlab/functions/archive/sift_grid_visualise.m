function data = sift_grid_visualise(I1, I2, blockHeight, blockWidth, scale, visualiseX, visualiseY)

% Firstly scale images
I1 = imresize(I1, scale);
I2 = imresize(I2, scale);

% Calculate block X and Y counts based on smallest image dimensions
blockXNum = idivide(int32(min([size(I1,2) size(I2,2)])), blockWidth, 'floor');
blockYNum = idivide(int32(min([size(I1,1) size(I2,1)])), blockHeight, 'floor');

% Create output struct
data = struct;
data.I1 = struct;
data.I2 = struct;
data.I1.frames = cell(blockYNum,blockXNum);
data.I2.frames = cell(blockYNum,blockXNum);
data.I1.descriptors = cell(blockYNum,blockXNum);
data.I2.descriptors = cell(blockYNum,blockXNum);

% Loop compute SIFT
for i = 1:blockYNum
    for j = 1:blockXNum
        
        % Save blocks
        I1_block = I1((i-1)*blockHeight+1:(i-1)*blockHeight+blockHeight+1, ...
            (j-1)*blockWidth+1:(j-1)*blockWidth+blockWidth+1);
        I2_block = I2((i-1)*blockHeight+1:(i-1)*blockHeight+blockHeight+1, ...
            (j-1)*blockWidth+1:(j-1)*blockWidth+blockWidth+1);
        
        % Do SIFT feature detection
        [I1_frames,I1_descriptors] = vl_sift(im2single(I1_block));
        [I2_frames,I2_descriptors] = vl_sift(im2single(I2_block));
        
        % If this is the block to visualise
        if (i == visualiseY) && (j == visualiseX)
           
            % I1
            figure;
            imshow(I1_block);
            vl_plotframe(I1_frames);
            
            % I2
            figure;
            imshow(I2_block);
            vl_plotframe(I2_frames);
            
        end
                
        % Save output data
        if ~isempty(I1_frames)
            data.I1.frames{i,j} = I1_frames;
        end
        if ~isempty(I2_frames)
            data.I2.frames{i,j} = I2_frames;
        end
        if ~isempty(I1_descriptors)
            data.I1.descriptors{i,j} = I1_descriptors;
        end
        if ~isempty(I2_descriptors)
            data.I2.descriptors{i,j} = I2_descriptors;
        end
    end
end

end


