function block = fft_grid_spatial(data, blockX, blockY)

% Extract block from grid data and normalise
block = reshape(data.spatial(blockY,blockX,:,:), ...
    size(data.spatial(blockY,blockX,:,:),3), ...
    size(data.spatial(blockY,blockX,:,:),4));

end

