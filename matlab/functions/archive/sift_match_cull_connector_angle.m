function [ m_out ] = sift_match_cull_connector_angle (fa, fb, ra, rb, ...
    m_in, max_degrees)

% NOTE: Not entirely finished, needs further testing.

% Convert max theta from degrees to radians
max_radians = max_degrees/(180/pi);

% Extract matched features
fa_m = fa(:,m_in(1,:));
fb_m = fb(:,m_in(2,:));

% Scale fa frames to match fb's space
fa_m(1:2,:) = fa_m(1:2,:).*(rb/ra);

% Calculate connector angles
connector_thetas = abs(atan((fa_m(2,:)-fb_m(2,:))./(fa_m(1,:)-fb_m(1,:))));
    
% Get indexes for thresholded angle
THRESH = connector_thetas < max_radians;

% Cull appropriate matches
m_out = m_in(:,THRESH);

end

