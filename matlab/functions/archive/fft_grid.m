function data = fft_grid(I, blockHeight, blockWidth, scale)

% Set default scale
if ~exist('scale','var')
    scale = 1;
end

% Firstly scale image if required
if scale ~= 1
    I = imresize(I, scale);
end
    
% Calculate block X and Y counts
blockXNum = idivide(int32(size(I,2)), blockWidth, 'ceil');
blockYNum = idivide(int32(size(I,1)), blockHeight, 'ceil');

% Extend image (with white) to make sure all blocks fit
tmp = I;
I = ones(blockYNum*blockHeight,blockXNum*blockWidth);
I(1:size(tmp,1),1:size(tmp,2)) = tmp;

% Create output struct
data = struct;
data.spatial = zeros(blockYNum, blockXNum, blockHeight, blockWidth);
data.psd_fourier = zeros(blockYNum, blockXNum, blockHeight, blockWidth);
%data.psd_cepstrum = zeros(blockYNum, blockXNum, blockHeight+1, blockWidth+1);

% Loop through blocks
for i = 1:blockYNum
    for j = 1:blockXNum
        
        % Get block
        spatial = I((i-1)*blockHeight+1:(i-1)*blockHeight+blockHeight, ...
            (j-1)*blockWidth+1:(j-1)*blockWidth+blockWidth);
        
        % Do FFT and normalise
        fourier = log(fftshift(abs(fft2(spatial))).^2);
        fourier = fourier./max(max(fourier));
        
        % Otsu the FFT
        fourier_otsu = psd_otsu(fourier);
        
        % Get Cepstrum and normalise
        %cepstrum = log(fftshift(abs(fft2(fourier_otsu))).^2);
        %cepstrum = cepstrum./max(max(cepstrum));
        
        % Save output data
        data.spatial(i,j,:,:) = spatial;
        data.psd_fourier(i,j,:,:) = fourier;
        %data.psd_cepstrum(i,j,:,:) = cepstrum;
    end
end

end


