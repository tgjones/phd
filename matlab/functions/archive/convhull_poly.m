function h = convhull_poly( BW )
% LOGICAL_CONVHULL accepts a logical array and produces a convex hull
% polygon from the array.
%
% Usage:
%   h = CONVHULL_POLY( BW )
%
%   BW - Logical array
%   h  - Cartesian convex polygon points

[r,c] = find(BW);
K = convhull(r,c);
h = [c(K), (r(K))];

end