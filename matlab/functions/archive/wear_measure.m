function [measure,peaks,delta] = wear_measure(psd, max_iterations, target_peak_count, weighting_map)

% Check dimensions match
if (size(psd,1) ~= size(weighting_map,1)) || (size(psd,2) ~= size(weighting_map,2))
   error('Weighting map dimensions do not match PSD dimensions');
end

% Initialise measure
measure = 0;

delta = 0.5;
adjustment = 0.25;
for i = 1:max_iterations
    
    peaks = psd_peak_detect(psd,delta);
    
    % Don't bother doing this on the last iteration
    if i ~= max_iterations

        % Too many peaks so raise the delta 50% for the next iteration
        if size(peaks,1) > target_peak_count

            delta = delta + adjustment;

        % Not enough peaks so lower the delta for the next iteration
        elseif size(peaks,1) < target_peak_count

            delta = delta - adjustment;

        % Exactly the right number of peaks (!) so stop now
        else
            break
        end
        
        % Half adjustment value
        adjustment = adjustment / 2;
    end
end

% Only do something if peaks were found
if ~isempty(peaks)

    % Get weighted magnitudes
    weighted_mags = zeros(size(peaks,1),1);
    for m=1:size(peaks,1)
        weighted_mags(m) = weighting_map(peaks(m,1),peaks(m,2))*peaks(m,3);
    end

    % Compute normalised measure
    measure = sum(weighted_mags)/sum(peaks(:,3));
    
end

end

