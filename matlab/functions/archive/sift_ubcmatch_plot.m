function sift_ubcmatch_plot(Ia,Ib)
% SIFT_UBCMATCH Computes and plots SIFT detection and matching using
% VLFeat toolbox.
%
% Usage:
%   SIFT_UBCMATCH(Ia,Ib)
%
%   Ia - first image filename
%   Ib - second image filename
%
% The VLFeat implementation of David Lowe's SIFT algorithm is executed on
% both input images and then a basic matching algorithm, also part of the
% VLFeat toolbox is computed and the results are plotted.
%
% For the purposes of visualisation, the tallest image is resized to match
% the height of the shortest image.

% Read images off disk
Ia = imread(Ia);
Ib = imread(Ib);

% Convert to grayscale
if size(Ia,3) > 1
    Ia = rgb2gray(Ia);
end
if size(Ib,3) > 1
    Ib = rgb2gray(Ib);
end

% Extract features             
[fa,da] = vl_sift(im2single(Ia));
[fb,db] = vl_sift(im2single(Ib));

% Do simple matching
matches = vl_ubcmatch(da,db);

% Scale tallest image and modify coordinate for display accordingly
if size(Ia,1) > size(Ib,1)
    sc = size(Ib,1)/size(Ia,1);
    Ia = imresize(Ia, [size(Ib,1), size(Ia,2) * sc]);
    fa = fa * sc;    
elseif size(Ib,1) > size(Ia,1)
    sc = size(Ia,1)/size(Ib,1);
    Ib = imresize(Ib, [size(Ia,1), size(Ib,2) * sc]);
    fb = fb * sc;
end

% Shift x coordinates of Ib features based on Ia width
fb(1,:) = fb(1,:) + size(Ia,2);

% Extract coordinates for lines
xa = fa(1,matches(1,:));
xb = fb(1,matches(2,:));
ya = fa(2,matches(1,:));
yb = fb(2,matches(2,:));

% Display images side-by-side
clf;
figure(1);
imshow(cat(2, Ia, Ib),[],'Border','tight');
axis equal;
axis off;

% Draw lines between matches
hold on;
h = line([xa ; xb], [ya ; yb]);
set(h,'linewidth', 1, 'color', 'b');

% Plot features on Ia
h = vl_plotframe(fa(:,matches(1,:)));
set(h,'color','r','linewidth',1);

% Plot features on Ib
h = vl_plotframe(fb(:,matches(2,:)));
set(h,'color','r','linewidth',1);
