function W = simulate_wear_01( R, s )
%SIMULATE_WEAR Simulate wear of the shoesole.
%   This function accepts the reference image of a shoe pattern (R), by
%   reference image we mean noise-free and sampled prior to wear, and a
%   wear amount (s). s represents the number of 1000 steps the algorithm is
%   is to simulate.
%
%   Usage: W = SIMULATE_WEAR(R, s);
%
%     W: Worn image
%     R: Reference image
%     s: Wear amount in 1000s of steps

% Get modifier
mod = s / 50;

% Erode vertically to model sole pattern detail loss
if mod > 1
    Wv = imerode(R, strel('line',mod,90));
else
    Wv = R;
end

% Erode horizontally to model sole pattern detail loss
if mod/2 > 1
    Wh = imerode(R, strel('line',mod/2,0));
else
    Wh = R;
end

% AND the results
W = bitand(Wv, Wh);

% Blur to make process seem more natural
f = fspecial('gaussian',10);
W = imfilter(W,f);

end

