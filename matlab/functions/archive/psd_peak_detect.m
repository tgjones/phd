function [peaks,troughs] = psd_peak_detect(psd, delta)

% This function simply runs Eli Billauer's peakdet.m on a row-by-row and
% column-by-column basis concatenating the results as we go.

peaks = [];
troughs = [];

% Do smoothing
psd = imfilter(psd,fspecial('gaussian',[5 5]));

% Do Otsu
psd = psd_otsu(psd);

% Do rows
for m=1:size(psd,1)
    [maxtab,mintab] = peakdet(psd(m,:),delta);
    if ~isempty(maxtab)
        peaks = [peaks; [maxtab(:,1) zeros(size(maxtab,1),1)+m maxtab(:,2)]];
    end
    if ~isempty(mintab)
        troughs = [troughs; [mintab(:,1) zeros(size(mintab,1),1)+m mintab(:,2)]];
    end
end

% Do columns
for n=1:size(psd,2)
    [maxtab,mintab] = peakdet(psd(:,n),delta);
    if ~isempty(maxtab)
        peaks = [peaks; [zeros(size(maxtab,1),1)+n maxtab(:,1) maxtab(:,2)]];
    end
    if ~isempty(mintab)
        troughs = [troughs; [zeros(size(mintab,1),1)+n mintab(:,1) mintab(:,2)]];
    end
end

end

