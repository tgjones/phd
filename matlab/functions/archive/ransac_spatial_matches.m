function [ best_tform, best_consensus_set, best_error ] = ...
    ransac_spatial_matches( matches, base_points, input_points, n, k, m)
%RANSAC_SPATIAL_MATCHES Selects the best Cartesian matches for automatic 
%   image registration.
%   See http://en.wikipedia.org/wiki/RANSAC for pseudocode.
%   TODO: SIFT scores?

% Initialise output variables
best_tform = 0;
best_consensus_set = 0;
best_error = Inf;

for i=1:k
    
    %consensus_set = 0;
    
    % Selecting the maybe inliers needs to be looped to ensure no
    % duplicate pairs are produced.
    maybe_unique = false;
    while ~maybe_unique
        
        % Randomly select some *unique* control point indeces, I had to use
        % this two-step randperm method as randi produces duplicate numbers
        % sometimes.
        match_permutation = randperm(size(matches,2));
        maybe_inliers_idx = match_permutation(1:n);

        % Initialise consensus set as maybe inliers from matches
        consensus_set = matches(:,maybe_inliers_idx);
        
        % Check for duplication on both rows of the match matrix
        unique_base_count = size(unique(consensus_set(1,:)),2);
        unique_input_count = size(unique(consensus_set(2,:)),2);
        
        % Break out of loop, or not, depending on above
        if ((unique_base_count ~= n) || (unique_input_count ~= n))
            maybe_unique = false;  
        else
            maybe_unique = true;
        end
    end
    
    % Generate transform object from maybe inliers
    maybe_tform = cp2tform(base_points(1:2,consensus_set(1,:))', ...
        input_points(1:2,consensus_set(2,:))', 'nonreflective similarity');
    
    % Create a logical array for selecting non-consensus values
    OTHER_VALUES = true(1,size(matches,2));
    OTHER_VALUES(:,maybe_inliers_idx) = false;
        
    % Extract other matches to new matrix
    other_matches = matches(:,OTHER_VALUES);
  
    % Transform both sets of points
    base_tform_points = tformfwd(maybe_tform, ...
        base_points(1:2,other_matches(1,:))')';
    input_tform_points = tformfwd(maybe_tform, ...
        input_points(1:2,other_matches(2,:))')';

    % Measure radial displacements
    displacement = sqrt( ...
        ( base_tform_points(1,:) - input_tform_points(1,:) ).^2 + ...
        ( base_tform_points(2,:) - input_tform_points(2,:) ).^2 );
    
    % Get indices of displacements less than t
    %MORE_VALUES = logical(displacement < t);
    
    % Append additional values to consensus_set
    %consensus_set = cat(2,consensus_set,other_matches(:,MORE_VALUES));
  
    % TODO: If the number of consensus matches are > d we have a good TFORM
    %if size(consensus_set,2) > d
    
        %sorted_displacement = sort(displacement);
        %displacement_threshold = sorted_displacement(:,m);

        % Extract the best scoring matches from the other matches
        %BEST_VALUES = displacement < displacement_threshold;

        % Mean top m displacement values as a way of measuring error
        %this_error = mean(displacement(:,BEST_VALUES));
        
        this_error = mean(displacement);
        
        % If this is the lowest error encountered so far
        if this_error < best_error
            
            % Re-generate transform with the additional values
            %best_tform = cp2tform(base_points(1:2,consensus_set(1,:))', ...
            %    input_points(1:2,consensus_set(2,:))', ...
            %    'nonreflective similarity');
 
            best_tform = maybe_tform;
            
            % Set best consensus set and error for return
            
            best_consensus_set = consensus_set;
            best_error = this_error;
        end
    %end
end

end

