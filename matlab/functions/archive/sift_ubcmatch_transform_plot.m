function sift_ubcmatch_transform_plot(I,sc,q)
% SIFT_UBCMATCH_TRANSFORM Transforms reference image I and plots matching SIFT
% keypoints using VLFeat toolbox.
%
% Usage:
%   SIFT_UBCMATCH_TRANSFORM(I,sc,q)
%
%   I  - image filename
%   sc - scale factor (default: 4)
%   q  - rotation factor (default: 4)
%
% Based on 'vl_demo_sift_match.m' from the VLFeat toolbox, the original can
% be found in the '\toolbox\demo' directory of the library download.

% Set default values for optional parameters
if ~exist('sc', 'var')
    sc = 4;
end
if ~exist('q', 'var')
    q = 4;
end

% Read image from disk
Ia = (imread(I));

% Convert to grayscale
if size(Ia,3) > 1
    Ia = rgb2gray(Ia);
end

% Rotate and scale image and store as Ib
th = pi/q;
c = sc*cos(th);
s = sc*sin(th);
A = [c -s; s c];
T = [- size(Ia,2); - size(Ia,1)] / 2;
tform = maketform('affine', [A, A * T - T ; 0 0 1]');
Ib = imtransform(Ia,tform,'size',size(Ia), ...
                 'xdata', [1 size(Ia,2)], ...
                 'ydata', [1 size(Ia,1)], ...
                 'fill', 255);

% Extract features             
[fa,da] = vl_sift(im2single(Ia));
[fb,db] = vl_sift(im2single(Ib));

% Do simple matching
matches = vl_ubcmatch(da,db);

% Display images side-by-side
clf;
figure(1);
imshow(cat(2, Ia, Ib),[]);
axis equal;
axis off;

% Shift x coordinates of Ib features based on Ia width
fb(1,:) = fb(1,:) + size(Ia,2);

% Extract co-ordinates of features
xa = fa(1,matches(1,:));
xb = fb(1,matches(2,:));
ya = fa(2,matches(1,:));
yb = fb(2,matches(2,:));

% Draw lines between matches
hold on;
h = line([xa ; xb], [ya ; yb]);
set(h,'linewidth', 1, 'color', 'b');

% Plot features on Ia
h = vl_plotframe(fa(:,matches(1,:)));
set(h,'color','r','linewidth',1);

% Plot features on Ib
h = vl_plotframe(fb(:,matches(2,:)));
set(h,'color','r','linewidth',1);
