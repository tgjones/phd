function ridge_grid_visualise_pair(ridge_data_A,ridge_data_B,x,y)

figure;
subplot(1,2,1);
imshow(ridge_grid_binary(ridge_data_A,x,y));
title('A');
subplot(1,2,2);
imshow(ridge_grid_binary(ridge_data_B,x,y));
title('B');

end

