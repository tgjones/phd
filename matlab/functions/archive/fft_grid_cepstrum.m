function psd = fft_grid_cepstrum(data, blockX, blockY)

% Extract PSD from grid data
psd = reshape(data.psd_cepstrum(blockY,blockX,:,:), ...
    size(data.psd_cepstrum(blockY,blockX,:,:),3), ...
    size(data.psd_cepstrum(blockY,blockX,:,:),4));

end

