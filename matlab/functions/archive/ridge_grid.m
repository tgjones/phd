function data = ridge_grid(fft_data,delta,window_radius)

% Get dimensions
blocks_m   = size(fft_data.psd_fourier,1);
blocks_n   = size(fft_data.psd_fourier,2);
psd_width  = size(fft_data.psd_fourier,4);
psd_height = size(fft_data.psd_fourier,3);

% Allocate memory
data = zeros(blocks_m,blocks_n,psd_height,psd_width);

% Detect ridges
for m = 1:blocks_m
    for n = 1:blocks_n
        data(m,n,:,:) = psd_ridge_detect(fft_grid_fourier(fft_data,n,m),delta,window_radius);
    end
end

end

