function block = ridge_grid_binary(data, blockX, blockY)

% Extract block from grid data
block = reshape(data(blockY,blockX,:,:), ...
    size(data(blockY,blockX,:,:),3), ...
    size(data(blockY,blockX,:,:),4));

end

