function ratios = wear_ratio_grid(data1, data2, max_iterations, target_peak_count, weighting_map)

% Get dimensions
blockXNum = min([size(data1.psd_fourier,2) size(data2.psd_fourier,2)]);
blockYNum = min([size(data1.psd_fourier,1) size(data2.psd_fourier,1)]);

% Create output array
ratios = zeros(blockYNum,blockXNum);

% Iterate through blocks
for m = 1:blockYNum
    for n = 1:blockXNum
        
        % T1 measure
        t1_measure = wear_measure(fft_grid_fourier(data1,n,m), max_iterations, target_peak_count, weighting_map);
        t2_measure = wear_measure(fft_grid_fourier(data2,n,m), max_iterations, target_peak_count, weighting_map);
        
        % Compute ratio
        ratios(m,n) = t1_measure/t2_measure;        
    end
end

end

