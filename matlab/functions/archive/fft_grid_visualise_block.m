function fft_grid_visualise_block(data, blockX, blockY)

figure;

% Get data for visulisation
spatial = fft_grid_spatial(data,blockX,blockY);
fourier = fft_grid_fourier(data,blockX,blockY);
cepstrum = fft_grid_cepstrum(data,blockX,blockY);
fourier_otsu = psd_otsu(fourier);
cepstrum_otsu = psd_otsu(cepstrum);

% Original block
subplot(2,4,1);
imshow(spatial,[]);
title('Spatial');

% Fourier PSD
subplot(2,4,2);
imshow(fourier,[]);
title('Fourier');

% Trimmed Fourier PSD
subplot(2,4,3);
imshow(fourier_otsu);
title('Fourier (Otsu)');

% Fourier peaks
subplot(2,4,4);
imshow(fourier_otsu);
hold on;
peaks = psd_peak_detect(fourier_otsu,0.7);
plot(peaks(:,1),peaks(:,2),'g*');
title('Fourier peaks');

% Cepstrum PSD
subplot(2,4,6);
imshow(cepstrum,[]);
title('Cepstrum');

% Trimmed Cepstrum PSD
subplot(2,4,7);
imshow(cepstrum_otsu);
title('Cepstrum (Otsu)');

% Cepstrum peaks
subplot(2,4,8);
imshow(cepstrum_otsu);
hold on;
peaks = psd_peak_detect(cepstrum_otsu,0.5);
plot(peaks(:,1),peaks(:,2),'g*');
title('Cepstrum peaks');
