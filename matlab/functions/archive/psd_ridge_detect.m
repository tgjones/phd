function [ridges,valleys] = psd_ridge_detect(psd, delta, window_radius)

% Check window radius is an integer
if mod(window_radius,1) ~= 0
    error('Window radius must be a whole number');
end

% Do smoothing
psd = imfilter(psd,fspecial('gaussian',[5 5]));

% Do Otsu
psd = psd_otsu(psd);

% Dimensions
psd_height = size(psd,1);
psd_width  = size(psd,2);

% Setup binary images
ridges_horiz  = false(psd_height,psd_width);
ridges_vert   = false(psd_height,psd_width);
valleys_horiz = false(psd_height,psd_width);
valleys_vert  = false(psd_height,psd_width);

% Do rows
for m=1:size(psd,1)
    [maxtab,mintab] = peakdet(psd(m,:),delta);
    if ~isempty(maxtab)  
        ridges_horiz(m,maxtab(:,1)) = true;
    end
    if ~isempty(mintab)
        valleys_horiz(m,mintab(:,1)) = true;
    end
end

% Filter out non-ridges (peaks without nearby peaks)
for m = 1:psd_height
    for n = 1:psd_width
        if ridges_horiz(m,n)
            
            % Get window
            window = ridges_horiz(max([m-window_radius 1]):min([m+window_radius psd_height]), ...
                max([n-window_radius 1]):min([n+window_radius psd_width]));
            
            % If the sum(window) is not greater than 1 then we don't have any neighbors
            if sum(sum(window)) == 1
                ridges_horiz(m,n) = 0;
            end
        end
    end
end
        
%TODO: Valleys

% Do columns
for n=1:size(psd,2)
    [maxtab,mintab] = peakdet(psd(:,n),delta);
    if ~isempty(maxtab)  
        ridges_vert(maxtab(:,1),n) = 1;
    end
    if ~isempty(mintab)
        valleys_vert(mintab(:,1),n) = 1;
    end
end

% Filter out non-ridges (peaks without nearby peaks)
for m = 1:psd_height
    for n = 1:psd_width
        if ridges_vert(m,n)
            
            % Get window
            window = ridges_vert(max([m-window_radius 1]):min([m+window_radius psd_height]), ...
                max([n-window_radius 1]):min([n+window_radius psd_width]));
            
            % If the sum(window) is not greater than 1 then we don't have any neighbors
            if sum(sum(window)) == 1
                ridges_vert(m,n) = 0;
            end
        end
    end
end

% OR the ridges together
ridges = ridges_horiz | ridges_vert;

end

