function [ m_out ] = sift_match_cull_roi(f, m_in, ROI)

% Not tested yet.

f_m = f(m_in);

IN = inpolygon(f_m(1,:), f_m(2,:), ROI(:,1), ROI(:,2));

m_out = m_in(IN);

end

