function [NS, EW] = fft_grid_ns_ew(data, blockX, blockY, thresh)

% Create filters
north = [-1 -2 -1; 0 4 0; 0 0 0];
south = [0 0 0; 0 4 0; -1 -2 -1];
east  = [0 0 -1; 0 4 -2; 0 0 -1];
west  = [-1 0 0; -2 4 0; -1 0 0];

% Extract block FFT PSD
psd = fft_grid_psd(data, blockX, blockY);

% Normalise PSD
psd_norm = psd./max(max(psd));

% Use Otsu if theshold is not specified
if ~exist('thresh','var')
    thresh = graythresh(psd_norm);
end

% Generate threshold mask
psd_mask = psd_norm > thresh;

% Discard thresholded data
psd_thresh = psd_norm .* psd_mask;

% Discard thresholded data
psd_trim = psd_thresh - thresh;
psd_trim(psd_trim<0) = 0;

% Do filtering
N = conv2(psd_trim,north);
S = conv2(psd_trim,south);
E = conv2(psd_trim,east);
W = conv2(psd_trim,west);

% Combine output
NS = (N > graythresh(N)) & (S > graythresh(S));
EW = (E > graythresh(E)) & (W > graythresh(W));

end

