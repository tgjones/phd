function [ T, I1f, I2f, I1d, I2d ] = sift_transformation( I1, I2, thresh_c, thresh_s, thresh_o )
% SIFT_TRANSFORMATION extracts the SIFT features from two images, matches them
% and computes the transformation data between corresponding features.
%
% Usage:
%   [ T, I1f, I2f, I1d, I2y ] = SIFT_TRANSFORMATION( I1, I2 )
%
%   Input:
%     I1, I2   - Images to compare
%
%     Thresholds for outlier culling (optional):
%       thresh_c - Cartesian threshold (default: 2)
%       thresh_s - Scale threshold (default: 3)
%       thresh_o - Orientation threshold (default: 1)
%     
%   Output:
%     I1f - Matching SIFT frames for image 1
%     I2f - Matching SIFT frames for image 2
%     I1d - Matching SIFT descriptors for image 1
%     I2d - Matching SIFT descriptors for image 2
%     T   - Transformation frame data
%
% D is the displacement information calculated from the SIFT frame information for 
% x and y. D(1,:) is the Cartesian displacement, D(2,:) is the scale displacement 
% and D(3,:) is the orientation shift.
% 
% Additionally, this function depends on the VLFeat Matlab library which you can
% download and install from http://www.vlfeat.org/.

% Defaults for optional arguments
DFLT_THRESH_C = 2;
DFLT_THRESH_S = 3;
DFLT_THRESH_O = 1;

% Set argument defaults if required
switch nargin
    case 2
        thresh_c = DFLT_THRESH_C;
        thresh_s = DFLT_THRESH_S;
        thresh_o = DFLT_THRESH_O;
    case 3
        thresh_s = DFLT_THRESH_S;
        thresh_o = DFLT_THRESH_O;
    case 4
        thresh_o = DFLT_THRESH_O;
end    
    
% Set culling threshold defualts
if isempty('thresh_c'); thresh_c = 2; end;
if isempty('thresh_s'); thresh_s = 3; end;
if isempty('thresh_o'); thresh_o = 1; end;

% Extract SIFT keypoints
[I1f, I1d] = vl_sift(im2single(I1));
[I2f, I2d] = vl_sift(im2single(I2));

% Perform UBC descriptor match to establish unique matches based on
% descriptor information, and elimate the majority of false positives.
m = vl_ubcmatch(I1d, I2d); % TODO: Tweak tolerance (default 1.5)

% Extract matching frames and descriptors from indices in m
I1f = I1f(:,m(1,:));
I2f = I2f(:,m(2,:));

% Extract descriptors too
I1d = I1d(:,m(1,:));
I2d = I2d(:,m(2,:));

% Extract raw displacement information for matched frames
T = I2f - I1f;

% Perform frame outlier cull, based on custom minimum value thresholds.
idx = (T(1,:) < thresh_c) & (T(1,:) > -thresh_c) ... % X
    & (T(2,:) < thresh_c) & (T(2,:) > -thresh_c) ... % Y
    & (T(3,:) < thresh_s) & (T(3,:) > -thresh_s) ... % Scale
    & (T(4,:) < thresh_o) & (T(4,:) > -thresh_o);    % Orientation
T = T(:,idx);

% Cull original frames too
I1f = I1f(:,idx);
I2f = I2f(:,idx);

% Cull descriptors too
I1d = I1d(:,idx);
I2d = I2d(:,idx);

end

