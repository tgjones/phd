function [ m_out ] = sift_match_cull_ellipse( fa, fb, m_in, parts )
% SIFT_MATCH_CULL_ELLIPSE Parses matched SIFT keypoints and culls based on
% coparative matching 'rings'.
%
% WARNING: This Matlab function has not been tested!!
%
% Usage:
%   [ m_out ] = SIFT_MATCH_CULL_ELLIPSE( fa, fb, m_in, rings )
%
%   fa         - SIFT frames from image A
%   fb         - SIFT frames from image B
%   m_in       - Match index input
%   partitions - Number of rings to compare, 1 is a whole ellipse, >1
%                creates 'ring' partitions with a central ellipse.
%   m_out      - Updated match indexes after ellipse comparison.
%
% Let a be the distince from the centre of the ellipse to the foci on the 
% major (vertical) axis:
%
% a = (x_max - x_min) / 2
%
% Let b be the distance from the centre of the ellipse to the ellipse on
% the minor (horizontal) axis:
%
% b = (y_max - y_min) / 2
%
% Therefore the equation for the ellipsis are based on:
% 
% (x^2 / b^2) + (y^2 / a^2) = 1
%
% So if p is the number of partitions and p_idx is the partition index 
% then the partition outline ellipse for p_idx is:
%
% (x^2 / (b - (p_idx * (b/p))^2) + (y^2 / (a - (p_idx * (a/p))^2) = 1
%
% SIFT_MATCH_CULL_ELLIPSE is designed to work with data output by functions
% in VLFeat library (http://www.vlfeat.org/).

% Extract bounds of outer ellipse for maxima and minima coordinates
fa_x_min = min(fa(1,m_in(1,:)));
fa_x_max = max(fa(1,m_in(1,:)));
fa_y_min = min(fa(2,m_in(1,:)));
fa_y_max = max(fa(2,m_in(1,:)));
fb_x_min = min(fb(1,m_in(2,:)));
fb_x_max = max(fb(1,m_in(2,:)));
fb_y_min = min(fb(2,m_in(2,:)));
fb_y_max = max(fb(2,m_in(2,:)));

% Calculate a and b
fa_a = (fa_x_max - fa_x_min) / 2;
fa_b = (fa_y_max - fa_y_min) / 2;
fb_a = (fb_x_max - fb_x_min) / 2;
fb_b = (fb_y_max - fb_y_min) / 2;

% Counter for match output index
m_out_idx=1;

% Preallocate m_out to size of m_in as a maximum
m_out=zeros(size(m_in));

% For each partition
for part=1:parts
    
    % Calculate square of partition width
    fa_a_part_width = fa_a/parts;
    fa_b_part_width = fa_b/parts;
    fb_a_part_width = fb_a/parts;
    fb_b_part_width = fb_b/parts;
    
    % For each matched pair
    for idx=1:size(m_in,2)
        
        % Initialise pair as valid
        valid=true;
        
        % Extract coordinates
        fa_x = fa(1,m_in(1,idx));
        fa_y = fa(2,m_in(1,idx));
        fb_x = fb(1,m_in(2,idx));
        fb_y = fb(2,m_in(2,idx));
        
        % If pair is outside of ellipse
        if ((fa_x^2 / (part*fa_b_part_width)^2) ...
            + (fa_y^2 / (part*fa_a_part_width)^2) > 1) ...
            && ((fb_x^2 / (part*fb_b_part_width)^2) ...
            + (fb_y^2 / (part*fb_a_part_width)^2) > 1)
        
            % Invalidate pair
            valid=false;
            
        end;

        % If we are not on the last partition (which would not have inner 
        % ellipsis) then ensure pair is *outside* of any inner ellipsis.
        if (part~=parts) ...
            && ((fa_x^2 / (part+1*fa_b_part_width)^2) ...
            + (fa_y^2 / (part+1*fa_a_part_width)^2) <= 1) ...
            && ((fb_x^2 / (part+1*fb_b_part_width)^2) ...
            + (fb_y^2 / (part+1*fb_a_part_width)^2) <= 1)
        
            % Invalidate pair
            valid=false;
            
        end;
        
        % Copy match to output only if valid is still true
        if valid==true
            m_out(:,m_out_idx) = m_in(:,idx);
        end;
        
    end;
end

