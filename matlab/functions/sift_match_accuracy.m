function [accuracy,outliers] = sift_match_accuracy(fa,fb,m,interval,count)
% fa: frames A
% fb: frames B
% m: matches
% interval: pixels
% count: number of intervals

accuracy = zeros(count,1);
outliers = 0;

for i = 1:size(m,2)
    dx = fb(2,m(2,i)) - fa(1,m(1,i));
    dy = fb(2,m(2,i)) - fa(1,m(1,i));

    idx = ceil(max(abs(dx),abs(dy))/interval);
    
    if (idx <= count)
        accuracy(idx,1) = accuracy(idx,1)+1;
    else
        outliers = outliers+1;
    end
end

end

