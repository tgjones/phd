function out = or_images(I1, I2)

% Pad images to maximum dimensions
max_rows = max(size(I1,1),size(I2,1));
max_cols = max(size(I1,2),size(I2,2));
tmp1 = I1;
tmp2 = I2;
I1 = zeros(max_rows,max_cols);
I2 = zeros(max_rows,max_cols);
I1(1:size(tmp1,1),1:size(tmp1,2)) = tmp1;
I2(1:size(tmp2,1),1:size(tmp2,2)) = tmp2;

% OR the images
out = I1+I2;

end

