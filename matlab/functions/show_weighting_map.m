function weighting_map = show_weighting_map(size)

% Calculate centre pixel coordinates
centre = [floor(size/2), floor(size/2)];

% Create FFT weighting map using the following function:
% (min(x,y)+sqrt(x^2 + y^2))^2
weighting_map = zeros(size,size);
for m = 1:size
    for n = 1:size
        weighting_map(m,n) = (min([abs(m-centre(1)) abs(n-centre(2))]) + sqrt((m-centre(1))^2 + (n-centre(2))^2))^2; 
    end
end

% Normalise FFT weighting map
weighting_map = weighting_map./max(max(weighting_map));

% Show the map on screen
figure;
imshow(weighting_map,[]);
title('(min(x,y)+sqrt(x^2 + y^2))^2');

end

