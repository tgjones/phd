function [ output ] = manual_registration( base, input )

input = rgb2gray(imread(input));
base  = rgb2gray(imread(base));

% Get control points from base
[input_points, base_points] = cpselect(input, base, 'Wait', true);

% Fine-tune control points with cross-correlation
input_points_adj = cpcorr(input_points, base_points, input, base);

% Infer the transform
tform = cp2tform(input_points_adj, base_points, 'nonreflective similarity');

% Do the transform
[output xdata ydata] = imtransform(input, tform, 'FillValues', 255, ...
    'XData', [1 size(input,2)+tform.tdata.T(3,1)], ...
    'YData', [1 size(input,1)+tform.tdata.T(3,2)]);

% Overlay the images to visually confirm
figure; 
imshow(output, 'XData', xdata, 'YData', ydata);
title('Registration Overlay');
hold on;
h = imshow(base);
set(h, 'AlphaData', 0.6); 

end