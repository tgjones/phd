function show_micro_registration(A,B,x,y)

% Confirm A and B are the same size
if (size(A) ~= size(B))
    error('A and B need to be the same size');
end

% Save image sizes with friendly names
image_height = size(A,1);
image_width  = size(A,2);

% Copy and pad A
base = zeros(image_height+abs(y)*2,image_width+abs(x)*2);
base(abs(y)+1:image_height+abs(y),abs(x)+1:image_width+abs(x)) = A;

% Compute coordinate space offsets
base_x = abs(x)+x;
base_y = abs(y)+y;

% Add B in the offset position
base(base_y+1:image_height+base_y,base_x+1:image_width+base_x) = ...
    base(base_y+1:image_height+base_y,base_x+1:image_width+base_x)+B;

% Crop back the image
base = base(abs(y)+1:image_height+abs(y),abs(x)+1:image_width+abs(x));

% Show base
figure;
imshow(base,[]);

end 