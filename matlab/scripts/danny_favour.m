% VARIABLES

% Specify block size (in pixels)
block_size = 200;

% Specify the block you're interested in (row,col format)
block_coord = [10 10];

% Specify the time slot you're interested (1
time_slot = 2;

% Mask radius
mask_r = 200;




% ALGORITHM

% Close current figures
close all;

% Load fused image data
load('Subject1.mat');

% Calculate range pixels
row_start = (block_coord(1)*block_size)-block_size+1;
row_end   = block_coord(1)*block_size;
col_start = (block_coord(2)*block_size)-block_size+1;
col_end   = block_coord(2)*block_size;

% Extract time slot block into 3D array
block_data = zeros(block_size,block_size,7);
block_data(:,:,1) = x01_Subject1_0km_140610(row_start:row_end,col_start:col_end);
block_data(:,:,2) = x02_Subject1_55km_190710(row_start:row_end,col_start:col_end);
block_data(:,:,3) = x03_Subject1_32km_020810(row_start:row_end,col_start:col_end);
block_data(:,:,4) = x04_Subject1_35km_060910(row_start:row_end,col_start:col_end);
block_data(:,:,5) = x05_Subject1_10km_200910(row_start:row_end,col_start:col_end);
block_data(:,:,6) = x06_Subject1_12km_041010(row_start:row_end,col_start:col_end);
block_data(:,:,7) = x07_Subject1_35km_181010(row_start:row_end,col_start:col_end);

% Extract time slot we're interested in
block = reshape(block_data(:,:,time_slot),block_size,block_size);

% Show the spatial image 
figure;
imshow(block,[]);

% Get the FFT of the block
block_fft = fft2(block);

% Show the PSD
figure;
imshow(log(fftshift(abs(block_fft))).^2,[]);

% Set up the mask
mask = zeros(block_size,block_size);
for m = 1:block_size
    for n = 1:block_size
        if (m^2)+(n^2) < mask_r^2
            mask(m,n) = 1;
        end
    end 
end

% Show the mask
figure;
imshow(mask);

% Apply the mask
block_fft_masked = block_fft.*mask;

% Invert the FFT and show
block_filtered = ifft2(block_fft_masked);
figure;
imshow(block_filtered,[]);





