block_size=100;
peak_delta=0.05;

% TODO: Check I have rising/falling the right way round!

fake_block=zeros(block_size,block_size);

% Vertical line
%for i = 1:block_size
%    fake_block(i,4) = 0.9;
%    fake_block(i,5) = 0.8;
%end

% Small diagonal line
%fake_block(3,7) = 0.9;
%fake_block(4,6) = 0.8;
%fake_block(5,5) = 0.8;
%fake_block(6,4) = 0.9; 

% The centre 100x100 of a real FFT
load('/Volumes/CRUZER/PhD/Matlab/workspaces/diag_test.mat');
fake_block = fft(51:150,51:150);
fake_block = fake_block/max(fake_block(:));

% Threshold out noise
fake_thresh = graythresh(fake_block);
fake_mask  = fake_block > fake_thresh;
fake_block = fake_block.*fake_mask - fake_thresh*fake_mask;

% Binary imagea for ridge detection pixels
peaks_horiz  = false(block_size,block_size);
peaks_vert   = false(block_size,block_size);

% Iterate through rows to detect vertical peaks
for row = 1:block_size
    
    % Initialise variables for SW-NE peak detection
    peak_min = Inf;
    peak_max = -Inf;
    peak_find_max = true;

    for col = 1:block_size

        % Get scalar element
        value = fake_block(row,col);

        % Update current maximum
        if value > peak_max
            peak_max = value;
            peak_max_idx = col;
        end

        % Update current minimum
        if value < peak_min
            peak_min = value;
            peak_in_idx = col;
        end

        % Currently searching for a maximum
        if peak_find_max
            if value < peak_max-peak_delta
                peaks_vert(row,col-1) = true;
                peak_min = value;
                peak_min_idx = col;
                peak_find_max = false;
            end

        % Currently searching for a minimum
        else
            if value > peak_min+peak_delta
                % Could save 'valleys' here if needed
                peak_max = value;
                peak_max_idx = col;
                peak_find_max = true;
            end
        end 
    end                
end

% Iterate through columns to detect horizontal peaks
for col = 1:block_size
    
    % Initialise variables for NW-SE peak detection
    peak_min = Inf;
    peak_max = -Inf;
    peak_find_max = true;
    
    for row = 1:block_size

        % Get scalar element
        value = fake_block(row,col);

        % Update current maximum
        if value > peak_max
            peak_max = value;
            peak_max_idx = row;
        end

        % Update current minimum
        if value < peak_min
            peak_min = value;
            peak_in_idx = row;
        end

        % Currently searching for a maximum
        if peak_find_max
            if value < peak_max-peak_delta
                peaks_horiz(row-1,col) = true;
                peak_min = value;
                peak_min_idx = row;
                peak_find_max = false;
            end

        % Currently searching for a minimum
        else
            if value > peak_min+peak_delta
                % Could save 'valleys' here if needed
                peak_max = value;
                peak_max_idx = row;
                peak_find_max = true;
            end
        end
    end
end

% Mergo horizontal and vertical peaks
peaks = peaks_vert|peaks_horiz;

% Open the image trying to single out falling diagonals
peaks = ...
    imopen(peaks, [0 1 0; 0 1 0; 0 0 1]) | ...
    imopen(peaks, [1 0 0; 0 1 0; 0 0 1]) | ...
    imopen(peaks, [1 0 0; 0 1 0; 0 1 0]);
