close all;
clear all;

%%%%%%%%%%%%%
% Subject 1 %
%%%%%%%%%%%%%

a = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\01_Subject1_0km_140610_0001.png');
b = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\01_Subject1_0km_140610_0002.png');
c = fuse_wear_prints(a,b,200,10,100);
imwrite(c/255,'r:\PhD\Imagery\TimeStudy\fused\Subject1\01_Subject1_0km_140610.png');

a = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\02_Subject1_55km_190710_0001.png');
b = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\02_Subject1_55km_190710_0002.png');
c = fuse_wear_prints(a,b,200,10,100);
imwrite(c/255,'r:\PhD\Imagery\TimeStudy\fused\Subject1\02_Subject1_55km_190710.png');

a = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\03_Subject1_32km_020810_0001.png');
b = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\03_Subject1_32km_020810_0002.png');
c = fuse_wear_prints(a,b,200,10,100);
imwrite(c/255,'r:\PhD\Imagery\TimeStudy\fused\Subject1\03_Subject1_32km_020810.png');

a = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\04_Subject1_35km_060910_0001.png');
b = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\04_Subject1_35km_060910_0002.png');
c = fuse_wear_prints(a,b,200,10,100);
imwrite(c/255,'r:\PhD\Imagery\TimeStudy\fused\Subject1\04_Subject1_35km_060910.png');

a = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\05_Subject1_10km_200910_0001.png');
b = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\05_Subject1_10km_200910_0002.png');
c = fuse_wear_prints(a,b,200,10,100);
imwrite(c/255,'r:\PhD\Imagery\TimeStudy\fused\Subject1\05_Subject1_10km_200910.png');

a = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\06_Subject1_12km_041010_0001.png');
b = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\06_Subject1_12km_041010_0002.png');
c = fuse_wear_prints(a,b,200,10,100);
imwrite(c/255,'r:\PhD\Imagery\TimeStudy\fused\Subject1\06_Subject1_12km_041010.png');

a = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\07_Subject1_35km_181010_0001.png');
b = imread('r:\PhD\Imagery\TimeStudy\registered\Subject1\07_Subject1_35km_181010_0002.png');
c = fuse_wear_prints(a,b,200,10,100);
imwrite(c/255,'r:\PhD\Imagery\TimeStudy\fused\Subject1\07_Subject1_35km_181010.png');