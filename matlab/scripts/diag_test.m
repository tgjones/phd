block_size=100;
peak_delta=0.05;

% TODO: Check I have rising/falling the right way round!

%fake_block=zeros(block_size,block_size);

% Vertical line
%for i = 1:block_size
%    fake_block(i,4) = 0.9;
%    fake_block(i,5) = 0.8;
%end

% Small diagonal line
%fake_block(3,7) = 0.9;
%fake_block(4,6) = 0.8;
%fake_block(5,5) = 0.8;
%fake_block(6,4) = 0.9; 

% The centre 100x100 of a real FFT
load('R:/PhD/Matlab/workspaces/diag_test.mat');
fake_block = fft(51:150,51:150);

% A whole FFT
%load('R:/PhD/Matlab/workspaces/13_5.mat');
%fake_block = fft_13_5_unworn;

% Threshold out data we're not interested in with a circle mask with r =
% 3*(block_size/8)
%circle_mask = zeros(block_size,block_size);
%for m = 1:block_size
%    for n = 1:block_size
%        r = sqrt(abs(m-block_size/2)^2+abs(n-block_size/2)^2);
%        if r < 3*(block_size/8);
%            circle_mask(m,n) = true;
%        end
%    end
%end
%fake_block = circle_mask.*fake_block;

% Perform an Otsu on the pixels we are interested in
inds = find(fake_block);
fake_thresh = graythresh(fake_block(inds)); 

% Trim out noise based on the new threshold
fake_mask  = fake_block > fake_thresh;
fake_block = fake_block.*fake_mask - fake_thresh*fake_mask;

% Binary imagea for ridge detection pixels
peaks_rising  = false(block_size,block_size);
peaks_falling = false(block_size,block_size);

% Iterate through falling diagonal lines to detect rising peaks
for d = 1:(2*block_size)-1
    
    % Initialise variables for SW-NE peak detection
    peak_min = Inf;
    peak_max = -Inf;
    peak_find_max = true;

    for p = 1:block_size-abs(d-block_size)

        % Calculate coordinate of pixel
        row = max(1,block_size-d+1)+(p-1);
        col = max(1,d-block_size+1)+(p-1);

        % Get scalar element
        value = fake_block(row,col);

        % Update current maximum
        if value > peak_max
            peak_max = value;
            peak_max_idx = p;
        end

        % Update current minimum
        if value < peak_min
            peak_min = value;
            peak_in_idx = p;
        end

        % Currently searching for a maximum
        if peak_find_max
            if value < peak_max-peak_delta
                peaks_rising(row-1,col-1) = true;
                peak_min = value;
                peak_min_idx = p;
                peak_find_max = false;
            end

        % Currently searching for a minimum
        else
            if value > peak_min+peak_delta
                % Could save 'valleys' here if needed
                peak_max = value;
                peak_max_idx = p;
                peak_find_max = true;
            end
        end 
    end                
end

% Iterate through rising diagonal lines to detect peaks
for d = 1:(block_size*2)-1
    
    % Initialise variables for NW-SE peak detection
    peak_min = Inf;
    peak_max = -Inf;
    peak_find_max = true;
    
    for p = 1:block_size-abs(d-block_size)

        % Calculate coordinate of pixel
        %row = abs(block_size-(max(1,block_size-d+1)+(p-1))+1);
        row = min(d,block_size)-(p-1);
        col = max(1,d-block_size+1)+(p-1);

        % Get scalar element
        value = fake_block(row,col);

        % Update current maximum
        if value > peak_max
            peak_max = value;
            peak_max_idx = p;
        end

        % Update current minimum
        if value < peak_min
            peak_min = value;
            peak_in_idx = p;
        end

        % Currently searching for a maximum
        if peak_find_max
            if value < peak_max-peak_delta
                peaks_falling(row+1,col-1) = true;
                peak_min = value;
                peak_min_idx = p;
                peak_find_max = false;
            end

        % Currently searching for a minimum
        else
            if value > peak_min+peak_delta
                % Could save 'valleys' here if needed
                peak_max = value;
                peak_max_idx = p;
                peak_find_max = true;
            end
        end
    end
end

% Use an open to filter out noise on the falling diagonals
peaks_falling = ...
    imopen(peaks_falling, [0 1 0; 0 1 0; 0 0 1]) | ...
    imopen(peaks_falling, [1 0 0; 0 1 0; 0 0 1]) | ...
    imopen(peaks_falling, [1 0 0; 0 1 0; 0 1 0]);% | ...
    %imopen(peaks_falling, [1 0 0; 0 1 1; 0 0 0]);

% Use an open to filter out noise on the rising diagonals
peaks_rising = ...
    imopen(peaks_rising, [0 1 0; 0 1 0; 1 0 0]) | ...
    imopen(peaks_rising, [0 0 1; 0 1 0; 1 0 0]) | ...
    imopen(peaks_rising, [0 0 1; 0 1 0; 0 1 0]);% | ...
    %imopen(peaks_rising, [0 0 1; 1 1 0; 0 0 0]);

% Mergo horizontal and vertical peaks
peaks = peaks_falling|peaks_rising;
