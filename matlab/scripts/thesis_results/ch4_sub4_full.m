clear all

% Read images from disk
T0a = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject4\01_Subject4_0_050710_0001.png');
T0b = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject4\01_Subject4_0_050710_0002.png');
T1a = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject4\02_Subject4_18miles_190710_0001.png');
T1b = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject4\02_Subject4_18miles_190710_0002.png');
T2a = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject4\03_Subject4_99miles_200910_0001.png');
T2b = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject4\03_Subject4_99miles_200910_0002.png');
T3a = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject4\04_Subject4_66miles_181010_0001.png');
T3b = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject4\04_Subject4_66miles_181010_0002.png');
T4a = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject4\05_Subject4_32miles_011110_0001.png');
T4b = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject4\05_Subject4_32miles_011110_0002.png');

% Get features and descriptors
[T0a_frames,T0a_descriptors] = vl_sift(rgb2gray(im2single(T0a)));
[T0b_frames,T0b_descriptors] = vl_sift(rgb2gray(im2single(T0b)));
[T1a_frames,T1a_descriptors] = vl_sift(rgb2gray(im2single(T1a)));
[T1b_frames,T1b_descriptors] = vl_sift(rgb2gray(im2single(T1b)));
[T2a_frames,T2a_descriptors] = vl_sift(rgb2gray(im2single(T2a)));
[T2b_frames,T2b_descriptors] = vl_sift(rgb2gray(im2single(T2b)));
[T3a_frames,T3a_descriptors] = vl_sift(rgb2gray(im2single(T3a)));
[T3b_frames,T3b_descriptors] = vl_sift(rgb2gray(im2single(T3b)));
[T4a_frames,T4a_descriptors] = vl_sift(rgb2gray(im2single(T4a)));
[T4b_frames,T4b_descriptors] = vl_sift(rgb2gray(im2single(T4b)));

% Get matches and scores
[T0a_T1a_matches,T0a_T1a_scores] = vl_ubcmatch(T0a_descriptors,T1a_descriptors);
[T0a_T1b_matches,T0a_T1b_scores] = vl_ubcmatch(T0a_descriptors,T1b_descriptors);
[T0b_T1a_matches,T0b_T1a_scores] = vl_ubcmatch(T0b_descriptors,T1a_descriptors);
[T0b_T1b_matches,T0b_T1b_scores] = vl_ubcmatch(T0b_descriptors,T1b_descriptors);

[T0a_T2a_matches,T0a_T2a_scores] = vl_ubcmatch(T0a_descriptors,T2a_descriptors);
[T0a_T2b_matches,T0a_T2b_scores] = vl_ubcmatch(T0a_descriptors,T2b_descriptors);
[T0b_T2a_matches,T0b_T2a_scores] = vl_ubcmatch(T0b_descriptors,T2a_descriptors);
[T0b_T2b_matches,T0b_T2b_scores] = vl_ubcmatch(T0b_descriptors,T2b_descriptors);

[T0a_T3a_matches,T0a_T3a_scores] = vl_ubcmatch(T0a_descriptors,T3a_descriptors);
[T0a_T3b_matches,T0a_T3b_scores] = vl_ubcmatch(T0a_descriptors,T3b_descriptors);
[T0b_T3a_matches,T0b_T3a_scores] = vl_ubcmatch(T0b_descriptors,T3a_descriptors);
[T0b_T3b_matches,T0b_T3b_scores] = vl_ubcmatch(T0b_descriptors,T3b_descriptors);

[T0a_T4a_matches,T0a_T4a_scores] = vl_ubcmatch(T0a_descriptors,T4a_descriptors);
[T0a_T4b_matches,T0a_T4b_scores] = vl_ubcmatch(T0a_descriptors,T4b_descriptors);
[T0b_T4a_matches,T0b_T4a_scores] = vl_ubcmatch(T0b_descriptors,T4a_descriptors);
[T0b_T4b_matches,T0b_T4b_scores] = vl_ubcmatch(T0b_descriptors,T4b_descriptors);

% Threshold Euclidean Distance
%T0a_T1a_m = T0a_T1a_m(:,(T0a_T1a_s < mean(T0a_T1a_s)));
%T2a_T3a_m = T2a_T3a_m(:,(T2a_T3a_s < mean(T2a_T3a_s)));
%T0a_T3a_m = T0a_T3a_m(:,(T0a_T3a_s < mean(T0a_T3a_s)));
%T1a_T4a_m = T1a_T4a_m(:,(T1a_T4a_s < mean(T1a_T4a_s)));%

% Threshold Orientation
%thresh_o = 0;
%thresh_s = 0;

%THRESH = ~(T0a_f(4,T0a_T1a_m(1,:)) + thresh_s > T1a_f(4,T0a_T1a_m(2,:)) ...
%    & (T0a_f(4,T0a_T1a_m(1,:)) - thresh_s > T1a_f(4,T0a_T1a_m(2,:))));
%T0a_T1a_m = T0a_T1a_m(:,THRESH);

%THRESH = ~(T2a_f(4,T2a_T3a_m(1,:)) + thresh_s > T3a_f(4,T2a_T3a_m(2,:)) ...
%    & (T2a_f(4,T2a_T3a_m(1,:)) - thresh_s > T3a_f(4,T2a_T3a_m(2,:))));
% T2a_T3a_m = T2a_T3a_m(:,THRESH);
% 
% THRESH = ~(T0a_f(4,T0a_T3a_m(1,:)) + thresh_s > T3a_f(4,T0a_T3a_m(2,:)) ...
%     & (T0a_f(4,T0a_T3a_m(1,:)) - thresh_s > T3a_f(4,T0a_T3a_m(2,:))));
% T0a_T3a_m = T0a_T3a_m(:,THRESH);
% 
% THRESH = ~(T1a_f(4,T1a_T4a_m(1,:)) + thresh_s > T4a_f(4,T1a_T4a_m(2,:)) ...
%     & (T1a_f(4,T1a_T4a_m(1,:)) - thresh_s > T4a_f(4,T1a_T4a_m(2,:))));
% T1a_T4a_m = T1a_T4a_m(:,THRESH);
% 

% Calculate displacements
T0a_T1a_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T1a_matches(1,:)) - T1a_frames(1,T0a_T1a_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T1a_matches(1,:)) - T1a_frames(2,T0a_T1a_matches(2,:)) ).^2); 
T0a_T1b_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T1b_matches(1,:)) - T1b_frames(1,T0a_T1b_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T1b_matches(1,:)) - T1b_frames(2,T0a_T1b_matches(2,:)) ).^2); 
T0b_T1a_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T1a_matches(1,:)) - T1a_frames(1,T0b_T1a_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T1a_matches(1,:)) - T1a_frames(2,T0b_T1a_matches(2,:)) ).^2); 
T0b_T1b_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T1b_matches(1,:)) - T1b_frames(1,T0b_T1b_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T1b_matches(1,:)) - T1b_frames(2,T0b_T1b_matches(2,:)) ).^2); 
    
T0a_T2a_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T2a_matches(1,:)) - T2a_frames(1,T0a_T2a_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T2a_matches(1,:)) - T2a_frames(2,T0a_T2a_matches(2,:)) ).^2); 
T0a_T2b_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T2b_matches(1,:)) - T2b_frames(1,T0a_T2b_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T2b_matches(1,:)) - T2b_frames(2,T0a_T2b_matches(2,:)) ).^2); 
T0b_T2a_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T2a_matches(1,:)) - T2a_frames(1,T0b_T2a_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T2a_matches(1,:)) - T2a_frames(2,T0b_T2a_matches(2,:)) ).^2); 
T0b_T2b_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T2b_matches(1,:)) - T2b_frames(1,T0b_T2b_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T2b_matches(1,:)) - T2b_frames(2,T0b_T2b_matches(2,:)) ).^2); 

T0a_T3a_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T3a_matches(1,:)) - T3a_frames(1,T0a_T3a_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T3a_matches(1,:)) - T3a_frames(2,T0a_T3a_matches(2,:)) ).^2); 
T0a_T3b_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T3b_matches(1,:)) - T3b_frames(1,T0a_T3b_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T3b_matches(1,:)) - T3b_frames(2,T0a_T3b_matches(2,:)) ).^2); 
T0b_T3a_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T3a_matches(1,:)) - T3a_frames(1,T0b_T3a_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T3a_matches(1,:)) - T3a_frames(2,T0b_T3a_matches(2,:)) ).^2); 
T0b_T3b_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T3b_matches(1,:)) - T3b_frames(1,T0b_T3b_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T3b_matches(1,:)) - T3b_frames(2,T0b_T3b_matches(2,:)) ).^2); 

T0a_T4a_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T4a_matches(1,:)) - T4a_frames(1,T0a_T4a_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T4a_matches(1,:)) - T4a_frames(2,T0a_T4a_matches(2,:)) ).^2); 
T0a_T4b_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T4b_matches(1,:)) - T4b_frames(1,T0a_T4b_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T4b_matches(1,:)) - T4b_frames(2,T0a_T4b_matches(2,:)) ).^2); 
T0b_T4a_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T4a_matches(1,:)) - T4a_frames(1,T0b_T4a_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T4a_matches(1,:)) - T4a_frames(2,T0b_T4a_matches(2,:)) ).^2); 
T0b_T4b_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T4b_matches(1,:)) - T4b_frames(1,T0b_T4b_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T4b_matches(1,:)) - T4b_frames(2,T0b_T4b_matches(2,:)) ).^2); 

% Threshold Displacement
thresh_d = 13;

T0a_T1a_results = [size(T0a_T1a_displacements,2) sum(T0a_T1a_displacements < thresh_d)];
T0a_T1a_results(:,3) = round(100/T0a_T1a_results(:,1) * T0a_T1a_results(:,2));
T0a_T1b_results = [size(T0a_T1b_displacements,2) sum(T0a_T1b_displacements < thresh_d)];
T0a_T1b_results(:,3) = round(100/T0a_T1b_results(:,1) * T0a_T1b_results(:,2));
T0b_T1a_results = [size(T0b_T1a_displacements,2) sum(T0b_T1a_displacements < thresh_d)];
T0b_T1a_results(:,3) = round(100/T0b_T1a_results(:,1) * T0b_T1a_results(:,2));
T0b_T1b_results = [size(T0b_T1b_displacements,2) sum(T0b_T1b_displacements < thresh_d)];
T0b_T1b_results(:,3) = round(100/T0b_T1b_results(:,1) * T0b_T1b_results(:,2));

T0a_T2a_results = [size(T0a_T2a_displacements,2) sum(T0a_T2a_displacements < thresh_d)];
T0a_T2a_results(:,3) = round(100/T0a_T2a_results(:,1) * T0a_T2a_results(:,2));
T0a_T2b_results = [size(T0a_T2b_displacements,2) sum(T0a_T2b_displacements < thresh_d)];
T0a_T2b_results(:,3) = round(100/T0a_T2b_results(:,1) * T0a_T2b_results(:,2));
T0b_T2a_results = [size(T0b_T2a_displacements,2) sum(T0b_T2a_displacements < thresh_d)];
T0b_T2a_results(:,3) = round(100/T0b_T2a_results(:,1) * T0b_T2a_results(:,2));
T0b_T2b_results = [size(T0b_T2b_displacements,2) sum(T0b_T2b_displacements < thresh_d)];
T0b_T2b_results(:,3) = round(100/T0b_T2b_results(:,1) * T0b_T2b_results(:,2));

T0a_T3a_results = [size(T0a_T3a_displacements,2) sum(T0a_T3a_displacements < thresh_d)];
T0a_T3a_results(:,3) = round(100/T0a_T3a_results(:,1) * T0a_T3a_results(:,2));
T0a_T3b_results = [size(T0a_T3b_displacements,2) sum(T0a_T3b_displacements < thresh_d)];
T0a_T3b_results(:,3) = round(100/T0a_T3b_results(:,1) * T0a_T3b_results(:,2));
T0b_T3a_results = [size(T0b_T3a_displacements,2) sum(T0b_T3a_displacements < thresh_d)];
T0b_T3a_results(:,3) = round(100/T0b_T3a_results(:,1) * T0b_T3a_results(:,2));
T0b_T3b_results = [size(T0b_T3b_displacements,2) sum(T0b_T3b_displacements < thresh_d)];
T0b_T3b_results(:,3) = round(100/T0b_T3b_results(:,1) * T0b_T3b_results(:,2));

T0a_T4a_results = [size(T0a_T4a_displacements,2) sum(T0a_T4a_displacements < thresh_d)];
T0a_T4a_results(:,3) = round(100/T0a_T4a_results(:,1) * T0a_T4a_results(:,2));
T0a_T4b_results = [size(T0a_T4b_displacements,2) sum(T0a_T4b_displacements < thresh_d)];
T0a_T4b_results(:,3) = round(100/T0a_T4b_results(:,1) * T0a_T4b_results(:,2));
T0b_T4a_results = [size(T0b_T4a_displacements,2) sum(T0b_T4a_displacements < thresh_d)];
T0b_T4a_results(:,3) = round(100/T0b_T4a_results(:,1) * T0b_T4a_results(:,2));
T0b_T4b_results = [size(T0b_T4b_displacements,2) sum(T0b_T4b_displacements < thresh_d)];
T0b_T4b_results(:,3) = round(100/T0b_T4b_results(:,1) * T0b_T4b_results(:,2));