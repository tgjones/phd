clear all

gnd_path = '/Users/Tim/Box Documents/My PhD/TimeStudy/gnd';
reg_path = '/Users/Tim/Box Documents/My PhD/TimeStudy/registered';

% Read ground truth
T0gnd = xlsread(strcat(gnd_path, '/Subject2.xlsx'),'01 (Ref)','A1:R25');
T1gnd = xlsread(strcat(gnd_path, '/Subject2.xlsx'),'02','A1:R25');
T2gnd = xlsread(strcat(gnd_path, '/Subject2.xlsx'),'03','A1:R25');
T3gnd = xlsread(strcat(gnd_path, '/Subject2.xlsx'),'04','A1:R25');
T4gnd = xlsread(strcat(gnd_path, '/Subject2.xlsx'),'05','A1:R25');
T5gnd = xlsread(strcat(gnd_path, '/Subject2.xlsx'),'06','A1:R25');
T6gnd = xlsread(strcat(gnd_path, '/Subject2.xlsx'),'07','A1:R25');

% Normalise (and invert) ground truth
T0gnd_nml = 1-((T0gnd-2).*(T0gnd>1)/3);
T1gnd_nml = 1-((T1gnd-2).*(T1gnd>1)/3);
T2gnd_nml = 1-((T2gnd-2).*(T2gnd>1)/3);
T3gnd_nml = 1-((T3gnd-2).*(T3gnd>1)/3);
T4gnd_nml = 1-((T4gnd-2).*(T4gnd>1)/3);
T5gnd_nml = 1-((T5gnd-2).*(T5gnd>1)/3);
T6gnd_nml = 1-((T6gnd-2).*(T6gnd>1)/3);

% Place NaN's where no wear information is
T0gnd_nml(T0gnd == 0) = NaN;
T1gnd_nml(T1gnd == 0) = NaN;
T2gnd_nml(T2gnd == 0) = NaN;
T3gnd_nml(T3gnd == 0) = NaN;
T4gnd_nml(T4gnd == 0) = NaN;
T5gnd_nml(T5gnd == 0) = NaN;
T6gnd_nml(T6gnd == 0) = NaN;

% Get minimum dimensions
T0gnd_T1gnd_size = [min([size(T0gnd_nml,1) size(T1gnd_nml,1)]) min([size(T0gnd_nml,2) size(T1gnd_nml,2)])];
T1gnd_T2gnd_size = [min([size(T1gnd_nml,1) size(T2gnd_nml,1)]) min([size(T1gnd_nml,2) size(T2gnd_nml,2)])];
T2gnd_T3gnd_size = [min([size(T2gnd_nml,1) size(T3gnd_nml,1)]) min([size(T2gnd_nml,2) size(T3gnd_nml,2)])];
T3gnd_T4gnd_size = [min([size(T3gnd_nml,1) size(T4gnd_nml,1)]) min([size(T3gnd_nml,2) size(T4gnd_nml,2)])];
T4gnd_T5gnd_size = [min([size(T4gnd_nml,1) size(T5gnd_nml,1)]) min([size(T4gnd_nml,2) size(T5gnd_nml,2)])];
T5gnd_T6gnd_size = [min([size(T5gnd_nml,1) size(T6gnd_nml,1)]) min([size(T5gnd_nml,2) size(T6gnd_nml,2)])];

% Get successive wear distances
T0gnd_T1gnd = abs(T1gnd_nml(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)) - T0gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)));
T1gnd_T2gnd = abs(T2gnd_nml(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)) - T1gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)));
T2gnd_T3gnd = abs(T3gnd_nml(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)) - T2gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)));
T3gnd_T4gnd = abs(T4gnd_nml(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)) - T3gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)));
T4gnd_T5gnd = abs(T5gnd_nml(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)) - T4gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)));
T5gnd_T6gnd = abs(T6gnd_nml(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)) - T5gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)));

clear T0gnd T1gnd T2gnd T3gnd T4gnd T5gnd T6gnd;

% Read images from disk (A)
T0img = rgb2gray(imread(strcat(reg_path,'/Subject2/01_Subject2_0km_140610_0002.png')));
T1img = rgb2gray(imread(strcat(reg_path,'/Subject2/02_Subject2_32km_280610_0002.png')));
T2img = rgb2gray(imread(strcat(reg_path,'/Subject2/03_Subject2_53km_180710_0002.png')));
T3img = rgb2gray(imread(strcat(reg_path,'/Subject2/04_Subject2_76km_150810_0002.png')));
T4img = rgb2gray(imread(strcat(reg_path,'/Subject2/05_Subject2_94km_210910_0002.png')));
T5img = rgb2gray(imread(strcat(reg_path,'/Subject2/06_Subject2_45miles_041010_0002.png')));
T6img = rgb2gray(imread(strcat(reg_path,'/Subject2/07_Subject2_47miles_181010_0002.png')));

% TODO: Check parameter values
block_size = 200; pixel_thresh = 10; peak_delta = 0.1;
T0data = compute_wear_data(T0img,block_size,pixel_thresh,peak_delta);
T1data = compute_wear_data(T1img,block_size,pixel_thresh,peak_delta);
T2data = compute_wear_data(T2img,block_size,pixel_thresh,peak_delta);
T3data = compute_wear_data(T3img,block_size,pixel_thresh,peak_delta);
T4data = compute_wear_data(T4img,block_size,pixel_thresh,peak_delta);
T5data = compute_wear_data(T5img,block_size,pixel_thresh,peak_delta);
T6data = compute_wear_data(T6img,block_size,pixel_thresh,peak_delta);

% Get minimum dimensions compared to GND
T0_size = [min([size(T0data.wear_measures,1) size(T0gnd_nml,1)]) ... 
    min([size(T0data.wear_measures,2) size(T0gnd_nml,2)])];
T1_size = [min([size(T1data.wear_measures,1) size(T1gnd_nml,1)]) ... 
    min([size(T1data.wear_measures,2) size(T1gnd_nml,2)])];
T2_size = [min([size(T2data.wear_measures,1) size(T2gnd_nml,1)]) ... 
    min([size(T2data.wear_measures,2) size(T2gnd_nml,2)])];
T3_size = [min([size(T3data.wear_measures,1) size(T3gnd_nml,1)]) ... 
    min([size(T3data.wear_measures,2) size(T3gnd_nml,2)])];
T4_size = [min([size(T4data.wear_measures,1) size(T4gnd_nml,1)]) ... 
    min([size(T4data.wear_measures,2) size(T4gnd_nml,2)])];
T5_size = [min([size(T5data.wear_measures,1) size(T5gnd_nml,1)]) ... 
    min([size(T5data.wear_measures,2) size(T5gnd_nml,2)])];
T6_size = [min([size(T6data.wear_measures,1) size(T6gnd_nml,1)]) ... 
    min([size(T6data.wear_measures,2) size(T6gnd_nml,2)])];

% Get common blocks
T0out = T0data.wear_measures(1:T0_size(1),1:T0_size(2));
T1out = T1data.wear_measures(1:T1_size(1),1:T1_size(2));
T2out = T2data.wear_measures(1:T2_size(1),1:T2_size(2));
T3out = T3data.wear_measures(1:T3_size(1),1:T3_size(2));
T4out = T4data.wear_measures(1:T4_size(1),1:T4_size(2));
T5out = T5data.wear_measures(1:T5_size(1),1:T5_size(2));
T6out = T6data.wear_measures(1:T6_size(1),1:T6_size(2));

% Filter out non-useful blocks
T0out = T0out(1:T0_size(1),1:T0_size(2)) .* ~isnan(T0gnd_nml(1:T0_size(1),1:T0_size(2)));
T1out = T1out(1:T1_size(1),1:T1_size(2)) .* ~isnan(T1gnd_nml(1:T1_size(1),1:T1_size(2)));
T2out = T2out(1:T2_size(1),1:T2_size(2)) .* ~isnan(T2gnd_nml(1:T2_size(1),1:T2_size(2)));
T3out = T3out(1:T3_size(1),1:T3_size(2)) .* ~isnan(T3gnd_nml(1:T3_size(1),1:T3_size(2)));
T4out = T4out(1:T4_size(1),1:T4_size(2)) .* ~isnan(T4gnd_nml(1:T4_size(1),1:T4_size(2)));
T5out = T5out(1:T5_size(1),1:T5_size(2)) .* ~isnan(T5gnd_nml(1:T5_size(1),1:T5_size(2)));
T6out = T6out(1:T6_size(1),1:T6_size(2)) .* ~isnan(T6gnd_nml(1:T6_size(1),1:T6_size(2)));

% Determine maximum measure
T0flat = reshape(T0out, numel(T0out), 1);
T1flat = reshape(T1out, numel(T1out), 1);
T2flat = reshape(T2out, numel(T2out), 1);
T3flat = reshape(T3out, numel(T3out), 1);
T4flat = reshape(T4out, numel(T4out), 1);
T5flat = reshape(T5out, numel(T5out), 1);
T6flat = reshape(T6out, numel(T6out), 1);
min_measure = min([T0flat; T1flat; T2flat; T3flat; T4flat; T5flat; T6flat]);
max_measure = max([T0flat; T1flat; T2flat; T3flat; T4flat; T5flat; T6flat]);

% Normalise wear measures
T0nml = (T0out-min_measure)/(max_measure-min_measure);
T1nml = (T1out-min_measure)/(max_measure-min_measure);
T2nml = (T2out-min_measure)/(max_measure-min_measure);
T3nml = (T3out-min_measure)/(max_measure-min_measure);
T4nml = (T4out-min_measure)/(max_measure-min_measure);
T5nml = (T5out-min_measure)/(max_measure-min_measure);
T6nml = (T6out-min_measure)/(max_measure-min_measure);

% Get minimum dimensions
T0nml_T1nml_size = [min([size(T0nml,1) size(T1nml,1)]) min([size(T0nml,2) size(T1nml,2)])];
T1nml_T2nml_size = [min([size(T1nml,1) size(T2nml,1)]) min([size(T1nml,2) size(T2nml,2)])];
T2nml_T3nml_size = [min([size(T2nml,1) size(T3nml,1)]) min([size(T2nml,2) size(T3nml,2)])];
T3nml_T4nml_size = [min([size(T3nml,1) size(T4nml,1)]) min([size(T3nml,2) size(T4nml,2)])];
T4nml_T5nml_size = [min([size(T4nml,1) size(T5nml,1)]) min([size(T4nml,2) size(T5nml,2)])];
T5nml_T6nml_size = [min([size(T5nml,1) size(T6nml,1)]) min([size(T5nml,2) size(T6nml,2)])];

% Get successive wear distances
T0nml_T1nml = abs(T1nml(1:T0nml_T1nml_size(1), 1:T0nml_T1nml_size(2)) - T0nml(1:T0nml_T1nml_size(1), 1:T0nml_T1nml_size(2)));
T1nml_T2nml = abs(T2nml(1:T1nml_T2nml_size(1), 1:T1nml_T2nml_size(2)) - T1nml(1:T1nml_T2nml_size(1), 1:T1nml_T2nml_size(2)));
T2nml_T3nml = abs(T3nml(1:T2nml_T3nml_size(1), 1:T2nml_T3nml_size(2)) - T2nml(1:T2nml_T3nml_size(1), 1:T2nml_T3nml_size(2)));
T3nml_T4nml = abs(T4nml(1:T3nml_T4nml_size(1), 1:T3nml_T4nml_size(2)) - T3nml(1:T3nml_T4nml_size(1), 1:T3nml_T4nml_size(2)));
T4nml_T5nml = abs(T5nml(1:T4nml_T5nml_size(1), 1:T4nml_T5nml_size(2)) - T4nml(1:T4nml_T5nml_size(1), 1:T4nml_T5nml_size(2)));
T5nml_T6nml = abs(T6nml(1:T5nml_T6nml_size(1), 1:T5nml_T6nml_size(2)) - T5nml(1:T5nml_T6nml_size(1), 1:T5nml_T6nml_size(2)));

% Get minimum dimensions
T0_T1_size = [min([size(T0nml_T1nml,1) size(T0gnd_T1gnd,1)]) min([size(T0nml_T1nml,2) size(T0gnd_T1gnd,2)])];
T1_T2_size = [min([size(T1nml_T2nml,1) size(T1gnd_T2gnd,1)]) min([size(T1nml_T2nml,2) size(T1gnd_T2gnd,2)])];
T2_T3_size = [min([size(T2nml_T3nml,1) size(T2gnd_T3gnd,1)]) min([size(T2nml_T3nml,2) size(T2gnd_T3gnd,2)])];
T3_T4_size = [min([size(T3nml_T4nml,1) size(T3gnd_T4gnd,1)]) min([size(T3nml_T4nml,2) size(T3gnd_T4gnd,2)])];
T4_T5_size = [min([size(T4nml_T5nml,1) size(T4gnd_T5gnd,1)]) min([size(T4nml_T5nml,2) size(T4gnd_T5gnd,2)])];
T5_T6_size = [min([size(T5nml_T6nml,1) size(T5gnd_T6gnd,1)]) min([size(T5nml_T6nml,2) size(T5gnd_T6gnd,2)])];

% Get distancees between ground truth and measures
T0_T1_dist = abs(T0gnd_T1gnd(1:T0_T1_size(1), 1:T0_T1_size(2))-T0nml_T1nml(1:T0_T1_size(1), 1:T0_T1_size(2)));
T1_T2_dist = abs(T1gnd_T2gnd(1:T1_T2_size(1), 1:T1_T2_size(2))-T1nml_T2nml(1:T1_T2_size(1), 1:T1_T2_size(2)));
T2_T3_dist = abs(T2gnd_T3gnd(1:T2_T3_size(1), 1:T2_T3_size(2))-T2nml_T3nml(1:T2_T3_size(1), 1:T2_T3_size(2)));
T3_T4_dist = abs(T3gnd_T4gnd(1:T3_T4_size(1), 1:T3_T4_size(2))-T3nml_T4nml(1:T3_T4_size(1), 1:T3_T4_size(2)));
T4_T5_dist = abs(T4gnd_T5gnd(1:T4_T5_size(1), 1:T4_T5_size(2))-T4nml_T5nml(1:T4_T5_size(1), 1:T4_T5_size(2)));
T5_T6_dist = abs(T5gnd_T6gnd(1:T5_T6_size(1), 1:T5_T6_size(2))-T5nml_T6nml(1:T5_T6_size(1), 1:T5_T6_size(2)));

% Get distance means
T0_T1_mean = mean(T0_T1_dist(~isnan(T0_T1_dist)));
T1_T2_mean = mean(T1_T2_dist(~isnan(T1_T2_dist)));
T2_T3_mean = mean(T2_T3_dist(~isnan(T2_T3_dist)));
T3_T4_mean = mean(T3_T4_dist(~isnan(T3_T4_dist)));
T4_T5_mean = mean(T4_T5_dist(~isnan(T4_T5_dist)));
T5_T6_mean = mean(T5_T6_dist(~isnan(T5_T6_dist)));

% Get distance medians
T0_T1_median = median(T0_T1_dist(~isnan(T0_T1_dist)));
T1_T2_median = median(T1_T2_dist(~isnan(T1_T2_dist)));
T2_T3_median = median(T2_T3_dist(~isnan(T2_T3_dist)));
T3_T4_median = median(T3_T4_dist(~isnan(T3_T4_dist)));
T4_T5_median = median(T4_T5_dist(~isnan(T4_T5_dist)));
T5_T6_median = median(T5_T6_dist(~isnan(T5_T6_dist)));