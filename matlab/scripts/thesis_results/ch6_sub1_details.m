clear all

gnd_path = '/Users/Tim/Box Documents/My PhD/TimeStudy/gnd';
reg_path = '/Users/Tim/Box Documents/My PhD/TimeStudy/registered';

% Read ground truth
T0gnd = xlsread(strcat(gnd_path, '/Subject1.xlsx'),'01 (Ref)','A1:R25');
T1gnd = xlsread(strcat(gnd_path, '/Subject1.xlsx'),'02','A1:R25');
T2gnd = xlsread(strcat(gnd_path, '/Subject1.xlsx'),'03','A1:R25');
T3gnd = xlsread(strcat(gnd_path, '/Subject1.xlsx'),'04','A1:R25');
T4gnd = xlsread(strcat(gnd_path, '/Subject1.xlsx'),'05','A1:R25');
T5gnd = xlsread(strcat(gnd_path, '/Subject1.xlsx'),'06','A1:R25');
T6gnd = xlsread(strcat(gnd_path, '/Subject1.xlsx'),'07','A1:R25');

% Normalise (and invert) ground truth
T0gnd_nml = 1-((T0gnd-2).*(T0gnd>1)/3);
T1gnd_nml = 1-((T1gnd-2).*(T1gnd>1)/3);
T2gnd_nml = 1-((T2gnd-2).*(T2gnd>1)/3);
T3gnd_nml = 1-((T3gnd-2).*(T3gnd>1)/3);
T4gnd_nml = 1-((T4gnd-2).*(T4gnd>1)/3);
T5gnd_nml = 1-((T5gnd-2).*(T5gnd>1)/3);
T6gnd_nml = 1-((T6gnd-2).*(T6gnd>1)/3);

% Place NaN's where no wear information is
T0gnd_nml(T0gnd == 0) = NaN;
T1gnd_nml(T1gnd == 0) = NaN;
T2gnd_nml(T2gnd == 0) = NaN;
T3gnd_nml(T3gnd == 0) = NaN;
T4gnd_nml(T4gnd == 0) = NaN;
T5gnd_nml(T5gnd == 0) = NaN;
T6gnd_nml(T6gnd == 0) = NaN;

% Get gnd detail means
T0gnd_mean = mean(T0gnd_nml(~isnan(T0gnd_nml)));
T1gnd_mean = mean(T1gnd_nml(~isnan(T1gnd_nml)));
T2gnd_mean = mean(T2gnd_nml(~isnan(T2gnd_nml)));
T3gnd_mean = mean(T3gnd_nml(~isnan(T3gnd_nml)));
T4gnd_mean = mean(T4gnd_nml(~isnan(T4gnd_nml)));
T5gnd_mean = mean(T5gnd_nml(~isnan(T5gnd_nml)));
T6gnd_mean = mean(T6gnd_nml(~isnan(T6gnd_nml)));

clear T0gnd T1gnd T2gnd T3gnd T4gnd T5gnd T6gnd;

% Read images from disk (A)
T0a_img = imread(strcat(reg_path,'/Subject1/01_Subject1_0km_140610_0001.png'));
T1a_img = imread(strcat(reg_path,'/Subject1/02_Subject1_55km_190710_0001.png'));
T2a_img = imread(strcat(reg_path,'/Subject1/03_Subject1_32km_020810_0001.png'));
T3a_img = imread(strcat(reg_path,'/Subject1/04_Subject1_35km_060910_0001.png'));
T4a_img = imread(strcat(reg_path,'/Subject1/05_Subject1_10km_200910_0001.png'));
T5a_img = imread(strcat(reg_path,'/Subject1/06_Subject1_12km_041010_0001.png'));
T6a_img = imread(strcat(reg_path,'/Subject1/07_Subject1_35km_181010_0001.png'));

% Read images from disk (B)
T0b_img = imread(strcat(reg_path,'/Subject1/01_Subject1_0km_140610_0002.png'));
T1b_img = imread(strcat(reg_path,'/Subject1/02_Subject1_55km_190710_0002.png'));
T2b_img = imread(strcat(reg_path,'/Subject1/03_Subject1_32km_020810_0002.png'));
T3b_img = imread(strcat(reg_path,'/Subject1/04_Subject1_35km_060910_0002.png'));
T4b_img = imread(strcat(reg_path,'/Subject1/05_Subject1_10km_200910_0002.png'));
T5b_img = imread(strcat(reg_path,'/Subject1/06_Subject1_12km_041010_0002.png'));
T6b_img = imread(strcat(reg_path,'/Subject1/07_Subject1_35km_181010_0002.png'));

% Fuse A and B
[T0img,T0data] = fuse_wear_prints(T0a_img, T0b_img, 200, 10, 10);
[T1img,T1data] = fuse_wear_prints(T1a_img, T1b_img, 200, 10, 10);
[T2img,T2data] = fuse_wear_prints(T2a_img, T2b_img, 200, 10, 10);
[T3img,T3data] = fuse_wear_prints(T3a_img, T3b_img, 200, 10, 10);
[T4img,T4data] = fuse_wear_prints(T4a_img, T4b_img, 200, 10, 10);
[T5img,T5data] = fuse_wear_prints(T5a_img, T5b_img, 200, 10, 10);
[T6img,T6data] = fuse_wear_prints(T6a_img, T6b_img, 200, 10, 10);

% Unload unfused
clear T0a_img T0b_img;
clear T1a_img T1b_img;
clear T2a_img T2b_img;
clear T3a_img T3b_img;
clear T4a_img T4b_img;
clear T5a_img T5b_img;
clear T6a_img T6b_img;

% Run FFT ridge detection
block_size = 200; pixel_thresh = 10; peak_delta = 0.1;
T0out = compute_wear_data(T0img,block_size,pixel_thresh,peak_delta);
T1out = compute_wear_data(T1img,block_size,pixel_thresh,peak_delta);
T2out = compute_wear_data(T2img,block_size,pixel_thresh,peak_delta);
T3out = compute_wear_data(T3img,block_size,pixel_thresh,peak_delta);
T4out = compute_wear_data(T4img,block_size,pixel_thresh,peak_delta);
T5out = compute_wear_data(T5img,block_size,pixel_thresh,peak_delta);
T6out = compute_wear_data(T6img,block_size,pixel_thresh,peak_delta);

% Determine maximum measure
T0flat = reshape(T0out.wear_measures, numel(T0out.wear_measures), 1);
T1flat = reshape(T1out.wear_measures, numel(T1out.wear_measures), 1);
T2flat = reshape(T2out.wear_measures, numel(T2out.wear_measures), 1);
T3flat = reshape(T3out.wear_measures, numel(T3out.wear_measures), 1);
T4flat = reshape(T4out.wear_measures, numel(T4out.wear_measures), 1);
T5flat = reshape(T5out.wear_measures, numel(T5out.wear_measures), 1);
T6flat = reshape(T6out.wear_measures, numel(T6out.wear_measures), 1);
min_measure = min([T0flat; T1flat; T2flat; T3flat; T4flat; T5flat; T6flat]);
max_measure = max([T0flat; T1flat; T2flat; T3flat; T4flat; T5flat; T6flat]);

% Normalise wear measures
T0nml = (T0out.wear_measures-min_measure)/max_measure;
T1nml = (T1out.wear_measures-min_measure)/max_measure;
T2nml = (T2out.wear_measures-min_measure)/max_measure;
T3nml = (T3out.wear_measures-min_measure)/max_measure;
T4nml = (T4out.wear_measures-min_measure)/max_measure;
T5nml = (T5out.wear_measures-min_measure)/max_measure;
T6nml = (T6out.wear_measures-min_measure)/max_measure;

% Get gnd detail means
T0val = T0nml(~isnan(T0gnd_nml));
T1val = T1nml(~isnan(T1gnd_nml));
T2val = T2nml(~isnan(T2gnd_nml));
T3val = T3nml(~isnan(T3gnd_nml));
T4val = T4nml(~isnan(T4gnd_nml));
T5val = T5nml(~isnan(T5gnd_nml)); 
T6val = T6nml(~isnan(T6gnd_nml));

% Get detail means
T0_nml_mean = mean(T0val(T0val > 0));
T1_nml_mean = mean(T1val(T1val > 0));
T2_nml_mean = mean(T2val(T2val > 0));
T3_nml_mean = mean(T3val(T3val > 0));
T4_nml_mean = mean(T4val(T4val > 0));
T5_nml_mean = mean(T5val(T5val > 0));
T6_nml_mean = mean(T6val(T6val > 0));

% Get detail means
T0_nml_median = median(T0val(T0val > 0));
T1_nml_median = median(T1val(T1val > 0));
T2_nml_median = median(T2val(T2val > 0));
T3_nml_median = median(T3val(T3val > 0));
T4_nml_median = median(T4val(T4val > 0));
T5_nml_median = median(T5val(T5val > 0));
T6_nml_median = median(T6val(T6val > 0));

% Added pixels from A to B
T0_a_to_b = sum(abs(T0data.mask_diff(T0data.mask_diff < 0)));
T1_a_to_b = sum(abs(T1data.mask_diff(T1data.mask_diff < 0)));
T2_a_to_b = sum(abs(T2data.mask_diff(T2data.mask_diff < 0)));
T3_a_to_b = sum(abs(T3data.mask_diff(T3data.mask_diff < 0)));
T4_a_to_b = sum(abs(T4data.mask_diff(T4data.mask_diff < 0)));
T5_a_to_b = sum(abs(T5data.mask_diff(T5data.mask_diff < 0)));
T6_a_to_b = sum(abs(T6data.mask_diff(T6data.mask_diff < 0)));

% Added pixels from B to A
T0_b_to_a = sum(T0data.mask_diff(T0data.mask_diff > 0));
T1_b_to_a = sum(T1data.mask_diff(T1data.mask_diff > 0));
T2_b_to_a = sum(T2data.mask_diff(T2data.mask_diff > 0));
T3_b_to_a = sum(T3data.mask_diff(T3data.mask_diff > 0));
T4_b_to_a = sum(T4data.mask_diff(T4data.mask_diff > 0));
T5_b_to_a = sum(T5data.mask_diff(T5data.mask_diff > 0));
T6_b_to_a = sum(T6data.mask_diff(T6data.mask_diff > 0));
