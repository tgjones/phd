clear all

gnd_path = '/Users/Tim/Box Documents/My PhD/TimeStudy/gnd';
reg_path = '/Users/Tim/Box Documents/My PhD/TimeStudy/registered';

% Read ground truth
T0gnd = xlsread(strcat(gnd_path, '/Subject3.xlsx'),'01 (Ref)','A1:R25');
T1gnd = xlsread(strcat(gnd_path, '/Subject3.xlsx'),'02','A1:R25');
T2gnd = xlsread(strcat(gnd_path, '/Subject3.xlsx'),'03','A1:R25');
T3gnd = xlsread(strcat(gnd_path, '/Subject3.xlsx'),'04','A1:R25');
T4gnd = xlsread(strcat(gnd_path, '/Subject3.xlsx'),'05','A1:R25');
T5gnd = xlsread(strcat(gnd_path, '/Subject3.xlsx'),'06','A1:R25');
T6gnd = xlsread(strcat(gnd_path, '/Subject3.xlsx'),'07','A1:R25');

% Normalise (and invert) ground truth
T0gnd_nml = 1-((T0gnd-2).*(T0gnd>1)/3);
T1gnd_nml = 1-((T1gnd-2).*(T1gnd>1)/3);
T2gnd_nml = 1-((T2gnd-2).*(T2gnd>1)/3);
T3gnd_nml = 1-((T3gnd-2).*(T3gnd>1)/3);
T4gnd_nml = 1-((T4gnd-2).*(T4gnd>1)/3);
T5gnd_nml = 1-((T5gnd-2).*(T5gnd>1)/3);
T6gnd_nml = 1-((T6gnd-2).*(T6gnd>1)/3);

% Place NaN's where no wear information is
T0gnd_nml(T0gnd == 0) = NaN;
T1gnd_nml(T1gnd == 0) = NaN;
T2gnd_nml(T2gnd == 0) = NaN;
T3gnd_nml(T3gnd == 0) = NaN;
T4gnd_nml(T4gnd == 0) = NaN;
T5gnd_nml(T5gnd == 0) = NaN;
T6gnd_nml(T6gnd == 0) = NaN;

% Get gnd detail means
T0gnd_mean = mean(T0gnd_nml(~isnan(T0gnd_nml)));
T1gnd_mean = mean(T1gnd_nml(~isnan(T1gnd_nml)));
T2gnd_mean = mean(T2gnd_nml(~isnan(T2gnd_nml)));
T3gnd_mean = mean(T3gnd_nml(~isnan(T3gnd_nml)));
T4gnd_mean = mean(T4gnd_nml(~isnan(T4gnd_nml)));
T5gnd_mean = mean(T5gnd_nml(~isnan(T5gnd_nml)));
T6gnd_mean = mean(T6gnd_nml(~isnan(T6gnd_nml)));

clear T0gnd ;
clear T1gnd ;
clear T2gnd ;
clear T3gnd ;
clear T4gnd ;
clear T5gnd ;
clear T6gnd ;

% Read images from disk (A)
T0img = rgb2gray(imread(strcat(reg_path, '/Subject3/01_Subject3_0miles_140610_0001.png')));
T1img = rgb2gray(imread(strcat(reg_path, '/Subject3/02_Subject3_48miles_290610_0001.png')));
T2img = rgb2gray(imread(strcat(reg_path, '/Subject3/03_Subject3_10miles_050710_0001.png')));
T3img = rgb2gray(imread(strcat(reg_path, '/Subject3/04_Subject3_92miles_150810_0001.png')));
T4img = rgb2gray(imread(strcat(reg_path, '/Subject3/05_Subject3_71miles_200910_0001.png')));
T5img = rgb2gray(imread(strcat(reg_path, '/Subject3/06_Subject3_25miles_041010_0001.png')));
T6img = rgb2gray(imread(strcat(reg_path, '/Subject3/07_Subject3_43miles_181010_0001.png')));

% Run FFT ridge detection
block_size = 200; pixel_thresh = 10; peak_delta = 0.1;
T0out = compute_wear_data(T0img,block_size,pixel_thresh,peak_delta);
T1out = compute_wear_data(T1img,block_size,pixel_thresh,peak_delta);
T2out = compute_wear_data(T2img,block_size,pixel_thresh,peak_delta);
T3out = compute_wear_data(T3img,block_size,pixel_thresh,peak_delta);
T4out = compute_wear_data(T4img,block_size,pixel_thresh,peak_delta);
T5out = compute_wear_data(T5img,block_size,pixel_thresh,peak_delta);
T6out = compute_wear_data(T6img,block_size,pixel_thresh,peak_delta);

% Determine maximum measure
T0flat = reshape(T0out.wear_measures, numel(T0out.wear_measures), 1);
T1flat = reshape(T1out.wear_measures, numel(T1out.wear_measures), 1);
T2flat = reshape(T2out.wear_measures, numel(T2out.wear_measures), 1);
T3flat = reshape(T3out.wear_measures, numel(T3out.wear_measures), 1);
T4flat = reshape(T4out.wear_measures, numel(T4out.wear_measures), 1);
T5flat = reshape(T5out.wear_measures, numel(T5out.wear_measures), 1);
T6flat = reshape(T6out.wear_measures, numel(T6out.wear_measures), 1);
min_measure = min([T0flat; T1flat; T2flat; T3flat; T4flat; T5flat; T6flat]);
max_measure = max([T0flat; T1flat; T2flat; T3flat; T4flat; T5flat; T6flat]);

% Normalise wear measures
T0nml = (T0out.wear_measures-min_measure)/max_measure;
T1nml = (T1out.wear_measures-min_measure)/max_measure;
T2nml = (T2out.wear_measures-min_measure)/max_measure;
T3nml = (T3out.wear_measures-min_measure)/max_measure;
T4nml = (T4out.wear_measures-min_measure)/max_measure;
T5nml = (T5out.wear_measures-min_measure)/max_measure;
T6nml = (T6out.wear_measures-min_measure)/max_measure;

% Get gnd detail means
T0val = T0nml(~isnan(T0gnd_nml)); 
T1val = T1nml(~isnan(T1gnd_nml)); 
T2val = T2nml(~isnan(T2gnd_nml)); 
T3val = T3nml(~isnan(T3gnd_nml)); 
T4val = T4nml(~isnan(T4gnd_nml)); 
T5val = T5nml(~isnan(T5gnd_nml)); 
T6val = T6nml(~isnan(T6gnd_nml)); 

% Get detail means
T0a_nml_mean = mean(T0val(T0val > 0));
T1a_nml_mean = mean(T1val(T1val > 0));
T2a_nml_mean = mean(T2val(T2val > 0));
T3a_nml_mean = mean(T3val(T3val > 0));
T4a_nml_mean = mean(T4val(T4val > 0));
T5a_nml_mean = mean(T5val(T5val > 0));
T6a_nml_mean = mean(T6val(T6val > 0));

% Get detail medians
T0a_nml_median = median(T0val(T0val > 0));
T1a_nml_median = median(T1val(T1val > 0));
T2a_nml_median = median(T2val(T2val > 0));
T3a_nml_median = median(T3val(T3val > 0));
T4a_nml_median = median(T4val(T4val > 0));
T5a_nml_median = median(T5val(T5val > 0));
T6a_nml_median = median(T6val(T6val > 0));

clear T0img T0out T0flat T0nml;
clear T1img T1out T1flat T1nml;
clear T2img T2out T2flat T2nml;
clear T3img T3out T3flat T3nml;
clear T4img T4out T4flat T4nml;
clear T5img T5out T5flat T5nml;
clear T6img T6out T6flat T6nml;


% Read images from disk (B)
T0img = rgb2gray(imread(strcat(reg_path, '/Subject3/01_Subject3_0miles_140610_0002.png')));
T1img = rgb2gray(imread(strcat(reg_path, '/Subject3/02_Subject3_48miles_290610_0002.png')));
T2img = rgb2gray(imread(strcat(reg_path, '/Subject3/03_Subject3_10miles_050710_0002.png')));
T3img = rgb2gray(imread(strcat(reg_path, '/Subject3/04_Subject3_92miles_150810_0002.png')));
T4img = rgb2gray(imread(strcat(reg_path, '/Subject3/05_Subject3_71miles_200910_0002.png')));
T5img = rgb2gray(imread(strcat(reg_path, '/Subject3/06_Subject3_25miles_041010_0002.png')));
T6img = rgb2gray(imread(strcat(reg_path, '/Subject3/07_Subject3_43miles_181010_0002.png')));

% Run FFT ridge detection
block_size = 200; pixel_thresh = 10; peak_delta = 0.1;
T0out = compute_wear_data(T0img,block_size,pixel_thresh,peak_delta);
T1out = compute_wear_data(T1img,block_size,pixel_thresh,peak_delta);
T2out = compute_wear_data(T2img,block_size,pixel_thresh,peak_delta);
T3out = compute_wear_data(T3img,block_size,pixel_thresh,peak_delta);
T4out = compute_wear_data(T4img,block_size,pixel_thresh,peak_delta);
T5out = compute_wear_data(T5img,block_size,pixel_thresh,peak_delta);
T6out = compute_wear_data(T6img,block_size,pixel_thresh,peak_delta);

% Determine maximum measure
T0flat = reshape(T0out.wear_measures, numel(T0out.wear_measures), 1);
T1flat = reshape(T1out.wear_measures, numel(T1out.wear_measures), 1);
T2flat = reshape(T2out.wear_measures, numel(T2out.wear_measures), 1);
T3flat = reshape(T3out.wear_measures, numel(T3out.wear_measures), 1);
T4flat = reshape(T4out.wear_measures, numel(T4out.wear_measures), 1);
T5flat = reshape(T5out.wear_measures, numel(T5out.wear_measures), 1);
T6flat = reshape(T6out.wear_measures, numel(T6out.wear_measures), 1);
min_measure = min([T0flat; T1flat; T2flat; T3flat; T4flat; T5flat; T6flat]);
max_measure = max([T0flat; T1flat; T2flat; T3flat; T4flat; T5flat; T6flat]);

% Normalise wear measures
T0nml = (T0out.wear_measures-min_measure)/(max_measure-min_measure);
T1nml = (T1out.wear_measures-min_measure)/(max_measure-min_measure);
T2nml = (T2out.wear_measures-min_measure)/(max_measure-min_measure);
T3nml = (T3out.wear_measures-min_measure)/(max_measure-min_measure);
T4nml = (T4out.wear_measures-min_measure)/(max_measure-min_measure);
T5nml = (T5out.wear_measures-min_measure)/(max_measure-min_measure);
T6nml = (T6out.wear_measures-min_measure)/(max_measure-min_measure);

% Get gnd detail means
T0val = T0nml(~isnan(T0gnd_nml)); 
T1val = T1nml(~isnan(T1gnd_nml)); 
T2val = T2nml(~isnan(T2gnd_nml)); 
T3val = T3nml(~isnan(T3gnd_nml)); 
T4val = T4nml(~isnan(T4gnd_nml)); 
T5val = T5nml(~isnan(T5gnd_nml)); 
T6val = T6nml(~isnan(T6gnd_nml)); 

% Get detail means
T0b_nml_mean = mean(T0val(T0val > 0));
T1b_nml_mean = mean(T1val(T1val > 0));
T2b_nml_mean = mean(T2val(T2val > 0));
T3b_nml_mean = mean(T3val(T3val > 0));
T4b_nml_mean = mean(T4val(T4val > 0));
T5b_nml_mean = mean(T5val(T5val > 0));
T6b_nml_mean = mean(T6val(T6val > 0));

% Get detail medians
T0b_nml_median = median(T0val(T0val > 0));
T1b_nml_median = median(T1val(T1val > 0));
T2b_nml_median = median(T2val(T2val > 0));
T3b_nml_median = median(T3val(T3val > 0));
T4b_nml_median = median(T4val(T4val > 0));
T5b_nml_median = median(T5val(T5val > 0));
T6b_nml_median = median(T6val(T6val > 0));

clear T0img T0out T0flat T0nml;
clear T1img T1out T1flat T1nml;
clear T2img T2out T2flat T2nml;
clear T3img T3out T3flat T3nml;
clear T4img T4out T4flat T4nml;
clear T5img T5out T5flat T5nml;
clear T6img T6out T6flat T6nml;