clear all

% Read images from disk
T0a = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\01_Subject2_0km_140610_0001.png');
T0b = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\01_Subject2_0km_140610_0002.png');
T1a = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\02_Subject2_32km_280610_0001.png');
T1b = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\02_Subject2_32km_280610_0002.png');
T2a = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\03_Subject2_53km_180710_0001.png');
T2b = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\03_Subject2_53km_180710_0002.png');
T3a = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\04_Subject2_76km_150810_0001.png');
T3b = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\04_Subject2_76km_150810_0002.png');
T4a = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\05_Subject2_94km_210910_0001.png');
T4b = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\05_Subject2_94km_210910_0002.png');
T5a = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\06_Subject2_45miles_041010_0001.png');
T5b = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\06_Subject2_45miles_041010_0002.png');
T6a = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\07_Subject2_47miles_181010_0001.png');
T6b = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject2\07_Subject2_47miles_181010_0002.png');

T0a = rgb2gray(T0a);

% Crop sample
T0a = T0a(720:920,1400:1550);
T0b = T0b(720:920,1400:1550);
T1a = T1a(720:920,1400:1550);
T1b = T1b(720:920,1400:1550);
T2a = T2a(720:920,1400:1550);
T2b = T2b(720:920,1400:1550);
T3a = T3a(720:920,1400:1550);
T3b = T3b(720:920,1400:1550);
T4a = T4a(720:920,1400:1550);
T4b = T4b(720:920,1400:1550);
T5a = T5a(720:920,1400:1550);
T5b = T5b(720:920,1400:1550);
T6a = T6a(720:920,1400:1550);
T6b = T6b(720:920,1400:1550);

% Get features and descriptors
[T0a_frames,T0a_descriptors] = vl_sift(im2single(T0a));
[T0b_frames,T0b_descriptors] = vl_sift(im2single(T0b));
[T1a_frames,T1a_descriptors] = vl_sift(im2single(T1a));
[T1b_frames,T1b_descriptors] = vl_sift(im2single(T1b));
[T2a_frames,T2a_descriptors] = vl_sift(im2single(T2a));
[T2b_frames,T2b_descriptors] = vl_sift(im2single(T2b));
[T3a_frames,T3a_descriptors] = vl_sift(im2single(T3a));
[T3b_frames,T3b_descriptors] = vl_sift(im2single(T3b));
[T4a_frames,T4a_descriptors] = vl_sift(im2single(T4a));
[T4b_frames,T4b_descriptors] = vl_sift(im2single(T4b));
[T5a_frames,T5a_descriptors] = vl_sift(im2single(T5a));
[T5b_frames,T5b_descriptors] = vl_sift(im2single(T5b));
[T6a_frames,T6a_descriptors] = vl_sift(im2single(T6a));
[T6b_frames,T6b_descriptors] = vl_sift(im2single(T6b));

% Get matches and scores
[T0a_T1a_matches,T0a_T1a_scores] = vl_ubcmatch(T0a_descriptors,T1a_descriptors);
[T0a_T1b_matches,T0a_T1b_scores] = vl_ubcmatch(T0a_descriptors,T1b_descriptors);
[T0b_T1a_matches,T0b_T1a_scores] = vl_ubcmatch(T0b_descriptors,T1a_descriptors);
[T0b_T1b_matches,T0b_T1b_scores] = vl_ubcmatch(T0b_descriptors,T1b_descriptors);

[T0a_T2a_matches,T0a_T2a_scores] = vl_ubcmatch(T0a_descriptors,T2a_descriptors);
[T0a_T2b_matches,T0a_T2b_scores] = vl_ubcmatch(T0a_descriptors,T2b_descriptors);
[T0b_T2a_matches,T0b_T2a_scores] = vl_ubcmatch(T0b_descriptors,T2a_descriptors);
[T0b_T2b_matches,T0b_T2b_scores] = vl_ubcmatch(T0b_descriptors,T2b_descriptors);

[T0a_T3a_matches,T0a_T3a_scores] = vl_ubcmatch(T0a_descriptors,T3a_descriptors);
[T0a_T3b_matches,T0a_T3b_scores] = vl_ubcmatch(T0a_descriptors,T3b_descriptors);
[T0b_T3a_matches,T0b_T3a_scores] = vl_ubcmatch(T0b_descriptors,T3a_descriptors);
[T0b_T3b_matches,T0b_T3b_scores] = vl_ubcmatch(T0b_descriptors,T3b_descriptors);

[T0a_T4a_matches,T0a_T4a_scores] = vl_ubcmatch(T0a_descriptors,T4a_descriptors);
[T0a_T4b_matches,T0a_T4b_scores] = vl_ubcmatch(T0a_descriptors,T4b_descriptors);
[T0b_T4a_matches,T0b_T4a_scores] = vl_ubcmatch(T0b_descriptors,T4a_descriptors);
[T0b_T4b_matches,T0b_T4b_scores] = vl_ubcmatch(T0b_descriptors,T4b_descriptors);

[T0a_T5a_matches,T0a_T5a_scores] = vl_ubcmatch(T0a_descriptors,T5a_descriptors);
[T0a_T5b_matches,T0a_T5b_scores] = vl_ubcmatch(T0a_descriptors,T5b_descriptors);
[T0b_T5a_matches,T0b_T5a_scores] = vl_ubcmatch(T0b_descriptors,T5a_descriptors);
[T0b_T5b_matches,T0b_T5b_scores] = vl_ubcmatch(T0b_descriptors,T5b_descriptors);

[T0a_T6a_matches,T0a_T6a_scores] = vl_ubcmatch(T0a_descriptors,T6a_descriptors);
[T0a_T6b_matches,T0a_T6b_scores] = vl_ubcmatch(T0a_descriptors,T6b_descriptors);
[T0b_T6a_matches,T0b_T6a_scores] = vl_ubcmatch(T0b_descriptors,T6a_descriptors);
[T0b_T6b_matches,T0b_T6b_scores] = vl_ubcmatch(T0b_descriptors,T6b_descriptors);

% Threshold Euclidean Distance
%T0a_T1a_m = T0a_T1a_m(:,(T0a_T1a_s < mean(T0a_T1a_s)));
%T2a_T3a_m = T2a_T3a_m(:,(T2a_T3a_s < mean(T2a_T3a_s)));
%T0a_T3a_m = T0a_T3a_m(:,(T0a_T3a_s < mean(T0a_T3a_s)));
%T1a_T4a_m = T1a_T4a_m(:,(T1a_T4a_s < mean(T1a_T4a_s)));%

% Threshold Orientation
%thresh_o = 0;
%thresh_s = 0;

%THRESH = ~(T0a_f(4,T0a_T1a_m(1,:)) + thresh_s > T1a_f(4,T0a_T1a_m(2,:)) ...
%    & (T0a_f(4,T0a_T1a_m(1,:)) - thresh_s > T1a_f(4,T0a_T1a_m(2,:))));
%T0a_T1a_m = T0a_T1a_m(:,THRESH);

%THRESH = ~(T2a_f(4,T2a_T3a_m(1,:)) + thresh_s > T3a_f(4,T2a_T3a_m(2,:)) ...
%    & (T2a_f(4,T2a_T3a_m(1,:)) - thresh_s > T3a_f(4,T2a_T3a_m(2,:))));
% T2a_T3a_m = T2a_T3a_m(:,THRESH);
% 
% THRESH = ~(T0a_f(4,T0a_T3a_m(1,:)) + thresh_s > T3a_f(4,T0a_T3a_m(2,:)) ...
%     & (T0a_f(4,T0a_T3a_m(1,:)) - thresh_s > T3a_f(4,T0a_T3a_m(2,:))));
% T0a_T3a_m = T0a_T3a_m(:,THRESH);
% 
% THRESH = ~(T1a_f(4,T1a_T4a_m(1,:)) + thresh_s > T4a_f(4,T1a_T4a_m(2,:)) ...
%     & (T1a_f(4,T1a_T4a_m(1,:)) - thresh_s > T4a_f(4,T1a_T4a_m(2,:))));
% T1a_T4a_m = T1a_T4a_m(:,THRESH);
% 

% Calculate displacements
T0a_T1a_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T1a_matches(1,:)) - T1a_frames(1,T0a_T1a_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T1a_matches(1,:)) - T1a_frames(2,T0a_T1a_matches(2,:)) ).^2); 
T0a_T1b_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T1b_matches(1,:)) - T1b_frames(1,T0a_T1b_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T1b_matches(1,:)) - T1b_frames(2,T0a_T1b_matches(2,:)) ).^2); 
T0b_T1a_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T1a_matches(1,:)) - T1a_frames(1,T0b_T1a_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T1a_matches(1,:)) - T1a_frames(2,T0b_T1a_matches(2,:)) ).^2); 
T0b_T1b_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T1b_matches(1,:)) - T1b_frames(1,T0b_T1b_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T1b_matches(1,:)) - T1b_frames(2,T0b_T1b_matches(2,:)) ).^2); 
    
T0a_T2a_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T2a_matches(1,:)) - T2a_frames(1,T0a_T2a_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T2a_matches(1,:)) - T2a_frames(2,T0a_T2a_matches(2,:)) ).^2); 
T0a_T2b_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T2b_matches(1,:)) - T2b_frames(1,T0a_T2b_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T2b_matches(1,:)) - T2b_frames(2,T0a_T2b_matches(2,:)) ).^2); 
T0b_T2a_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T2a_matches(1,:)) - T2a_frames(1,T0b_T2a_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T2a_matches(1,:)) - T2a_frames(2,T0b_T2a_matches(2,:)) ).^2); 
T0b_T2b_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T2b_matches(1,:)) - T2b_frames(1,T0b_T2b_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T2b_matches(1,:)) - T2b_frames(2,T0b_T2b_matches(2,:)) ).^2); 

T0a_T3a_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T3a_matches(1,:)) - T3a_frames(1,T0a_T3a_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T3a_matches(1,:)) - T3a_frames(2,T0a_T3a_matches(2,:)) ).^2); 
T0a_T3b_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T3b_matches(1,:)) - T3b_frames(1,T0a_T3b_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T3b_matches(1,:)) - T3b_frames(2,T0a_T3b_matches(2,:)) ).^2); 
T0b_T3a_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T3a_matches(1,:)) - T3a_frames(1,T0b_T3a_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T3a_matches(1,:)) - T3a_frames(2,T0b_T3a_matches(2,:)) ).^2); 
T0b_T3b_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T3b_matches(1,:)) - T3b_frames(1,T0b_T3b_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T3b_matches(1,:)) - T3b_frames(2,T0b_T3b_matches(2,:)) ).^2); 

T0a_T4a_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T4a_matches(1,:)) - T4a_frames(1,T0a_T4a_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T4a_matches(1,:)) - T4a_frames(2,T0a_T4a_matches(2,:)) ).^2); 
T0a_T4b_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T4b_matches(1,:)) - T4b_frames(1,T0a_T4b_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T4b_matches(1,:)) - T4b_frames(2,T0a_T4b_matches(2,:)) ).^2); 
T0b_T4a_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T4a_matches(1,:)) - T4a_frames(1,T0b_T4a_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T4a_matches(1,:)) - T4a_frames(2,T0b_T4a_matches(2,:)) ).^2); 
T0b_T4b_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T4b_matches(1,:)) - T4b_frames(1,T0b_T4b_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T4b_matches(1,:)) - T4b_frames(2,T0b_T4b_matches(2,:)) ).^2); 

T0a_T5a_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T5a_matches(1,:)) - T5a_frames(1,T0a_T5a_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T5a_matches(1,:)) - T5a_frames(2,T0a_T5a_matches(2,:)) ).^2); 
T0a_T5b_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T5b_matches(1,:)) - T5b_frames(1,T0a_T5b_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T5b_matches(1,:)) - T5b_frames(2,T0a_T5b_matches(2,:)) ).^2); 
T0b_T5a_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T5a_matches(1,:)) - T5a_frames(1,T0b_T5a_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T5a_matches(1,:)) - T5a_frames(2,T0b_T5a_matches(2,:)) ).^2); 
T0b_T5b_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T5b_matches(1,:)) - T5b_frames(1,T0b_T5b_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T5b_matches(1,:)) - T5b_frames(2,T0b_T5b_matches(2,:)) ).^2); 
 
T0a_T6a_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T6a_matches(1,:)) - T6a_frames(1,T0a_T6a_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T6a_matches(1,:)) - T6a_frames(2,T0a_T6a_matches(2,:)) ).^2); 
T0a_T6b_displacements = sqrt( ...
    ( T0a_frames(1,T0a_T6b_matches(1,:)) - T6b_frames(1,T0a_T6b_matches(2,:)) ).^2 + ...
    ( T0a_frames(2,T0a_T6b_matches(1,:)) - T6b_frames(2,T0a_T6b_matches(2,:)) ).^2); 
T0b_T6a_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T6a_matches(1,:)) - T6a_frames(1,T0b_T6a_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T6a_matches(1,:)) - T6a_frames(2,T0b_T6a_matches(2,:)) ).^2); 
T0b_T6b_displacements = sqrt( ...
    ( T0b_frames(1,T0b_T6b_matches(1,:)) - T6b_frames(1,T0b_T6b_matches(2,:)) ).^2 + ...
    ( T0b_frames(2,T0b_T6b_matches(1,:)) - T6b_frames(2,T0b_T6b_matches(2,:)) ).^2); 
 

% Threshold Displacement
thresh_d = 13;

T0a_T1a_results = [size(T0a_T1a_displacements,2) sum(T0a_T1a_displacements < thresh_d)];
T0a_T1a_results(:,3) = round(100/T0a_T1a_results(:,1) * T0a_T1a_results(:,2));
T0a_T1b_results = [size(T0a_T1b_displacements,2) sum(T0a_T1b_displacements < thresh_d)];
T0a_T1b_results(:,3) = round(100/T0a_T1b_results(:,1) * T0a_T1b_results(:,2));
T0b_T1a_results = [size(T0b_T1a_displacements,2) sum(T0b_T1a_displacements < thresh_d)];
T0b_T1a_results(:,3) = round(100/T0b_T1a_results(:,1) * T0b_T1a_results(:,2));
T0b_T1b_results = [size(T0b_T1b_displacements,2) sum(T0b_T1b_displacements < thresh_d)];
T0b_T1b_results(:,3) = round(100/T0b_T1b_results(:,1) * T0b_T1b_results(:,2));

T0a_T2a_results = [size(T0a_T2a_displacements,2) sum(T0a_T2a_displacements < thresh_d)];
T0a_T2a_results(:,3) = round(100/T0a_T2a_results(:,1) * T0a_T2a_results(:,2));
T0a_T2b_results = [size(T0a_T2b_displacements,2) sum(T0a_T2b_displacements < thresh_d)];
T0a_T2b_results(:,3) = round(100/T0a_T2b_results(:,1) * T0a_T2b_results(:,2));
T0b_T2a_results = [size(T0b_T2a_displacements,2) sum(T0b_T2a_displacements < thresh_d)];
T0b_T2a_results(:,3) = round(100/T0b_T2a_results(:,1) * T0b_T2a_results(:,2));
T0b_T2b_results = [size(T0b_T2b_displacements,2) sum(T0b_T2b_displacements < thresh_d)];
T0b_T2b_results(:,3) = round(100/T0b_T2b_results(:,1) * T0b_T2b_results(:,2));

T0a_T3a_results = [size(T0a_T3a_displacements,2) sum(T0a_T3a_displacements < thresh_d)];
T0a_T3a_results(:,3) = round(100/T0a_T3a_results(:,1) * T0a_T3a_results(:,2));
T0a_T3b_results = [size(T0a_T3b_displacements,2) sum(T0a_T3b_displacements < thresh_d)];
T0a_T3b_results(:,3) = round(100/T0a_T3b_results(:,1) * T0a_T3b_results(:,2));
T0b_T3a_results = [size(T0b_T3a_displacements,2) sum(T0b_T3a_displacements < thresh_d)];
T0b_T3a_results(:,3) = round(100/T0b_T3a_results(:,1) * T0b_T3a_results(:,2));
T0b_T3b_results = [size(T0b_T3b_displacements,2) sum(T0b_T3b_displacements < thresh_d)];
T0b_T3b_results(:,3) = round(100/T0b_T3b_results(:,1) * T0b_T3b_results(:,2));

T0a_T4a_results = [size(T0a_T4a_displacements,2) sum(T0a_T4a_displacements < thresh_d)];
T0a_T4a_results(:,3) = round(100/T0a_T4a_results(:,1) * T0a_T4a_results(:,2));
T0a_T4b_results = [size(T0a_T4b_displacements,2) sum(T0a_T4b_displacements < thresh_d)];
T0a_T4b_results(:,3) = round(100/T0a_T4b_results(:,1) * T0a_T4b_results(:,2));
T0b_T4a_results = [size(T0b_T4a_displacements,2) sum(T0b_T4a_displacements < thresh_d)];
T0b_T4a_results(:,3) = round(100/T0b_T4a_results(:,1) * T0b_T4a_results(:,2));
T0b_T4b_results = [size(T0b_T4b_displacements,2) sum(T0b_T4b_displacements < thresh_d)];
T0b_T4b_results(:,3) = round(100/T0b_T4b_results(:,1) * T0b_T4b_results(:,2));

T0a_T5a_results = [size(T0a_T5a_displacements,2) sum(T0a_T5a_displacements < thresh_d)];
T0a_T5a_results(:,3) = round(100/T0a_T5a_results(:,1) * T0a_T5a_results(:,2));
T0a_T5b_results = [size(T0a_T5b_displacements,2) sum(T0a_T5b_displacements < thresh_d)];
T0a_T5b_results(:,3) = round(100/T0a_T5b_results(:,1) * T0a_T5b_results(:,2));
T0b_T5a_results = [size(T0b_T5a_displacements,2) sum(T0b_T5a_displacements < thresh_d)];
T0b_T5a_results(:,3) = round(100/T0b_T5a_results(:,1) * T0b_T5a_results(:,2));
T0b_T5b_results = [size(T0b_T5b_displacements,2) sum(T0b_T5b_displacements < thresh_d)];
T0b_T5b_results(:,3) = round(100/T0b_T5b_results(:,1) * T0b_T5b_results(:,2));

T0a_T6a_results = [size(T0a_T6a_displacements,2) sum(T0a_T6a_displacements < thresh_d)];
T0a_T6a_results(:,3) = round(100/T0a_T6a_results(:,1) * T0a_T6a_results(:,2));
T0a_T6b_results = [size(T0a_T6b_displacements,2) sum(T0a_T6b_displacements < thresh_d)];
T0a_T6b_results(:,3) = round(100/T0a_T6b_results(:,1) * T0a_T6b_results(:,2));
T0b_T6a_results = [size(T0b_T6a_displacements,2) sum(T0b_T6a_displacements < thresh_d)];
T0b_T6a_results(:,3) = round(100/T0b_T6a_results(:,1) * T0b_T6a_results(:,2));
T0b_T6b_results = [size(T0b_T6b_displacements,2) sum(T0b_T6b_displacements < thresh_d)];
T0b_T6b_results(:,3) = round(100/T0b_T6b_results(:,1) * T0b_T6b_results(:,2));