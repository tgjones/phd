clear all

% Read images from disk
T0img = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject1\01_Subject1_0km_140610_0001.png');
T1img = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject1\02_Subject1_55km_190710_0001.png');
T2img = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject1\03_Subject1_32km_020810_0001.png');
T3img = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject1\04_Subject1_35km_060910_0001.png');
T4img = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject1\05_Subject1_10km_200910_0001.png');
T5img = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject1\06_Subject1_12km_041010_0001.png');
T6img = imread('C:\Users\Tim\Documents\TimeStudy\registered\Subject1\07_Subject1_35km_181010_0001.png');

% Read ground truth
T0gnd = xlsread('C:\Users\Tim\Documents\TimeStudy\gnd\Subject1.xlsx','01 (Ref)','A1:R25');
T1gnd = xlsread('C:\Users\Tim\Documents\TimeStudy\gnd\Subject1.xlsx','02','A1:R25');
T2gnd = xlsread('C:\Users\Tim\Documents\TimeStudy\gnd\Subject1.xlsx','03','A1:R25');
T3gnd = xlsread('C:\Users\Tim\Documents\TimeStudy\gnd\Subject1.xlsx','04','A1:R25');
T4gnd = xlsread('C:\Users\Tim\Documents\TimeStudy\gnd\Subject1.xlsx','05','A1:R25');
T5gnd = xlsread('C:\Users\Tim\Documents\TimeStudy\gnd\Subject1.xlsx','06','A1:R25');
T6gnd = xlsread('C:\Users\Tim\Documents\TimeStudy\gnd\Subject1.xlsx','07','A1:R25');

% TODO: Cull 'empty' blocks

% Normalise ground truth
T0gnd = (T0gnd-2).*(T0gnd>1)/3;
T1gnd = (T1gnd-2).*(T1gnd>1)/3;
T2gnd = (T2gnd-2).*(T2gnd>1)/3;
T3gnd = (T3gnd-2).*(T3gnd>1)/3;
T4gnd = (T4gnd-2).*(T4gnd>1)/3;
T5gnd = (T5gnd-2).*(T5gnd>1)/3;
T6gnd = (T6gnd-2).*(T6gnd>1)/3;

% Get minimum dimensions
T0gnd_T1gnd_size = [min([size(T0gnd,1) size(T1gnd,1)]) min([size(T0gnd,2) size(T1gnd,2)])];
T1gnd_T2gnd_size = [min([size(T1gnd,1) size(T2gnd,1)]) min([size(T1gnd,2) size(T2gnd,2)])];
T2gnd_T3gnd_size = [min([size(T2gnd,1) size(T3gnd,1)]) min([size(T2gnd,2) size(T3gnd,2)])];
T3gnd_T4gnd_size = [min([size(T3gnd,1) size(T4gnd,1)]) min([size(T3gnd,2) size(T4gnd,2)])];
T4gnd_T5gnd_size = [min([size(T4gnd,1) size(T5gnd,1)]) min([size(T4gnd,2) size(T5gnd,2)])];
T5gnd_T6gnd_size = [min([size(T5gnd,1) size(T6gnd,1)]) min([size(T5gnd,2) size(T6gnd,2)])];

% Get successive wear distances
T0gnd_T1gnd = T1gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)) - T0gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2));
T1gnd_T2gnd = T2gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)) - T1gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2));
T2gnd_T3gnd = T3gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)) - T2gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2));
T3gnd_T4gnd = T4gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)) - T3gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2));
T4gnd_T5gnd = T5gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)) - T4gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2));
T5gnd_T6gnd = T6gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2)) - T5gnd(1:T0gnd_T1gnd_size(1), 1:T0gnd_T1gnd_size(2));

% Run FFT ridge detection
% TODO: Check parameter values
block_size = 200; pixel_thresh = 10; peak_delta = 0.1;
T0out = compute_wear_data(T0img,block_size,pixel_thresh,peak_delta);
T1out = compute_wear_data(T1img,block_size,pixel_thresh,peak_delta);
T2out = compute_wear_data(T2img,block_size,pixel_thresh,peak_delta);
T3out = compute_wear_data(T3img,block_size,pixel_thresh,peak_delta);
T4out = compute_wear_data(T4img,block_size,pixel_thresh,peak_delta);
T5out = compute_wear_data(T5img,block_size,pixel_thresh,peak_delta);
T6out = compute_wear_data(T6img,block_size,pixel_thresh,peak_delta);

% Determine maximum measure
T0flat = reshape(T0out.wear_measures, numel(T0out.wear_measures), 1);
T1flat = reshape(T1out.wear_measures, numel(T1out.wear_measures), 1);
T2flat = reshape(T2out.wear_measures, numel(T2out.wear_measures), 1);
T3flat = reshape(T3out.wear_measures, numel(T3out.wear_measures), 1);
T4flat = reshape(T4out.wear_measures, numel(T4out.wear_measures), 1);
T5flat = reshape(T5out.wear_measures, numel(T5out.wear_measures), 1);
T6flat = reshape(T6out.wear_measures, numel(T6out.wear_measures), 1);
min_measure = min([T0flat; T1flat; T2flat; T3flat; T4flat; T5flat; T6flat]);
max_measure = max([T0flat; T1flat; T2flat; T3flat; T4flat; T5flat; T6flat]);

% Normalise wear measures
T0nml = (T0out.wear_measures-min_measure)/max_measure;
T1nml = (T1out.wear_measures-min_measure)/max_measure;
T2nml = (T2out.wear_measures-min_measure)/max_measure;
T3nml = (T3out.wear_measures-min_measure)/max_measure;
T4nml = (T4out.wear_measures-min_measure)/max_measure;
T5nml = (T5out.wear_measures-min_measure)/max_measure;
T6nml = (T6out.wear_measures-min_measure)/max_measure;

% Get minimum dimensions
T0nml_T1nml_size = [min([size(T0nml,1) size(T1nml,1)]) min([size(T0nml,2) size(T1nml,2)])];
T1nml_T2nml_size = [min([size(T1nml,1) size(T2nml,1)]) min([size(T1nml,2) size(T2nml,2)])];
T2nml_T3nml_size = [min([size(T2nml,1) size(T3nml,1)]) min([size(T2nml,2) size(T3nml,2)])];
T3nml_T4nml_size = [min([size(T3nml,1) size(T4nml,1)]) min([size(T3nml,2) size(T4nml,2)])];
T4nml_T5nml_size = [min([size(T4nml,1) size(T5nml,1)]) min([size(T4nml,2) size(T5nml,2)])];
T5nml_T6nml_size = [min([size(T5nml,1) size(T6nml,1)]) min([size(T5nml,2) size(T6nml,2)])];

% Get successive wear distances
T0nml_T1nml = T1nml(1:T0nml_T1nml_size(1), 1:T0nml_T1nml_size(2)) - T0nml(1:T0nml_T1nml_size(1), 1:T0nml_T1nml_size(2));
T1nml_T2nml = T2nml(1:T1nml_T2nml_size(1), 1:T1nml_T2nml_size(2)) - T1nml(1:T1nml_T2nml_size(1), 1:T1nml_T2nml_size(2));
T2nml_T3nml = T3nml(1:T2nml_T3nml_size(1), 1:T2nml_T3nml_size(2)) - T2nml(1:T2nml_T3nml_size(1), 1:T2nml_T3nml_size(2));
T3nml_T4nml = T4nml(1:T3nml_T4nml_size(1), 1:T3nml_T4nml_size(2)) - T3nml(1:T3nml_T4nml_size(1), 1:T3nml_T4nml_size(2));
T4nml_T5nml = T5nml(1:T4nml_T5nml_size(1), 1:T4nml_T5nml_size(2)) - T4nml(1:T4nml_T5nml_size(1), 1:T4nml_T5nml_size(2));
T5nml_T6nml = T6nml(1:T5nml_T6nml_size(1), 1:T5nml_T6nml_size(2)) - T5nml(1:T5nml_T6nml_size(1), 1:T5nml_T6nml_size(2));

% Get minimum dimensions
T0_T1_size = [min([size(T0nml_T1nml,1) size(T0gnd_T1gnd,1)]) min([size(T0nml_T1nml,2) size(T0gnd_T1gnd,2)])];
T1_T2_size = [min([size(T1nml_T2nml,1) size(T1gnd_T2gnd,1)]) min([size(T1nml_T2nml,2) size(T1gnd_T2gnd,2)])];
T2_T3_size = [min([size(T2nml_T3nml,1) size(T2gnd_T3gnd,1)]) min([size(T2nml_T3nml,2) size(T2gnd_T3gnd,2)])];
T3_T4_size = [min([size(T3nml_T4nml,1) size(T3gnd_T4gnd,1)]) min([size(T3nml_T4nml,2) size(T3gnd_T4gnd,2)])];
T4_T5_size = [min([size(T4nml_T5nml,1) size(T4gnd_T5gnd,1)]) min([size(T4nml_T5nml,2) size(T4gnd_T5gnd,2)])];
T5_T6_size = [min([size(T5nml_T6nml,1) size(T5gnd_T6gnd,1)]) min([size(T5nml_T6nml,2) size(T5gnd_T6gnd,2)])];

% Get distancees between ground truth and measures
T0_T1_dist = abs(T0gnd_T1gnd(1:T0_T1_size(1), 1:T0_T1_size(2))-T0nml_T1nml(1:T0_T1_size(1), 1:T0_T1_size(2)));
T1_T2_dist = abs(T1gnd_T2gnd(1:T1_T2_size(1), 1:T1_T2_size(2))-T1nml_T2nml(1:T1_T2_size(1), 1:T1_T2_size(2)));
T2_T3_dist = abs(T2gnd_T3gnd(1:T2_T3_size(1), 1:T2_T3_size(2))-T2nml_T3nml(1:T2_T3_size(1), 1:T2_T3_size(2)));
T3_T4_dist = abs(T3gnd_T4gnd(1:T3_T4_size(1), 1:T3_T4_size(2))-T3nml_T4nml(1:T3_T4_size(1), 1:T3_T4_size(2)));
T4_T5_dist = abs(T4gnd_T5gnd(1:T4_T5_size(1), 1:T4_T5_size(2))-T4nml_T5nml(1:T4_T5_size(1), 1:T4_T5_size(2)));
T5_T6_dist = abs(T5gnd_T6gnd(1:T5_T6_size(1), 1:T5_T6_size(2))-T5nml_T6nml(1:T5_T6_size(1), 1:T5_T6_size(2)));

% Get distance means
T0_T1_mean = mean(T0_T1_dist(~isnan(T0_T1_dist)));
T1_T2_mean = mean(T1_T2_dist(~isnan(T1_T2_dist)));
T2_T3_mean = mean(T2_T3_dist(~isnan(T2_T3_dist)));
T3_T4_mean = mean(T3_T4_dist(~isnan(T3_T4_dist)));
T4_T5_mean = mean(T4_T5_dist(~isnan(T4_T5_dist)));
T5_T6_mean = mean(T5_T6_dist(~isnan(T5_T6_dist)));

% Get distance standard deviations
T0_T1_std = std(T0_T1_dist(~isnan(T0_T1_dist)));
T1_T2_std = std(T1_T2_dist(~isnan(T1_T2_dist)));
T2_T3_std = std(T2_T3_dist(~isnan(T2_T3_dist)));
T3_T4_std = std(T3_T4_dist(~isnan(T3_T4_dist)));
T4_T5_std = std(T4_T5_dist(~isnan(T4_T5_dist)));
T5_T6_std = std(T5_T6_dist(~isnan(T5_T6_dist)));


