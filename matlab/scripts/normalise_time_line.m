

% APPROACH 1
% Get the mean
% NOTE: Please load data_sub1 and ground_sub1

% Get timeslots
t_max = size(data_sub1,3);

% Initialise calibrated score array
calib_sub1 = zeros(size(data_sub1));

% For each time slot
for t = 1:t_max
   
    % Eliminate zeros
    idx = data_sub1(:,:,t)>0;

    % First invert the scores
    calib = zeros(size(data_sub1(:,:,t)));
    %calib(idx) = max(data_sub1(idx))-data_sub1(idx);
    %calib_sub1(:,:,t) = calib;
    
    % Then calibrate against the mean
    mean_score = mean(calib(idx));
    calib_sub1(:,:,t) = calib_sub1(:,:,t)/mean_score;
    
end 





% APPROACH 2
% Use the maximum value from blocks that reach very high wear to generate
% calibration factor
%factors = zeros(25,18);

% For each block
%for m = 1:size(factors,1)
%    for n = 1:size(factors,2)
   
        % Get timeline of ground truth
%        ground = [gnd_sub1_01(m,n) gnd_sub1_02(m,n) gnd_sub1_03(m,n) gnd_sub1_04(m,n) gnd_sub1_05(m,n) gnd_sub1_06(m,n) gnd_sub1_07(m,n)];
        
        % Only compute result of this block reaches 'very high wear'
%        if max(ground == 5)

            % Get timeline of detail values
%            detail = [data_sub1_01(m,n) data_sub1_02(m,n) data_sub1_03(m,n) data_sub1_04(m,n) data_sub1_05(m,n) data_sub1_06(m,n) data_sub1_07(m,n)];

            % Invert values
%            detail = max(detail) - detail;
        
            % Save factor
%            factors(m,n) = max(detail);
            
        % Put a NaN in otherwise
%        else        
             
%            factors(m,n) = NaN;
            
%        end        
%    end
%end

%clear detail ground m n;

% Get spread
%min_factor = min(factors(~isnan(factors)));
%max_factor = max(factors(~isnan(factors)));

% Get average factor
%mean_factor = sum(sum(factors(~isnan(factors))))/sum(sum(~isnan(factors)));

% Now go through and normalise detail measures
%data_sub1_01_norm = data_sub1_01/max_factor;
%data_sub1_02_norm = data_sub1_02/max_factor;
%data_sub1_03_norm = data_sub1_03/max_factor;
%data_sub1_04_norm = data_sub1_04/max_factor; 
%data_sub1_05_norm = data_sub1_05/max_factor;
%data_sub1_06_norm = data_sub1_06/max_factor;
%data_sub1_07_norm = data_sub1_07/max_factor;

% APPROACH 1
% Get average maxima for each time step (first three have no heavy wear)

%max_04 = data_sub1_04(gnd_sub1_04==5);
%max_04 = max_04(~isnan(max_04));
%mean_max_04 = sum(max_04(:))/size(max_04,1);

%max_05 = data_sub1_05(gnd_sub1_05==5);
%max_05 = max_05(~isnan(max_05));
%mean_max_05 = sum(max_05(:))/size(max_05,1);

%max_06 = data_sub1_06(gnd_sub1_06==5);
%max_06 = max_06(~isnan(max_06));
%mean_max_06 = sum(max_06(:))/size(max_06,1);

%max_07 = data_sub1_07(gnd_sub1_07==5);
%max_07 = max_07(~isnan(max_07));
%mean_max_07 = sum(max_07(:))/size(max_07,1);

% Get overall maximum average
%mean_max = (mean_max_04+mean_max_05+mean_max_06+mean_max_07)/4;

% No go through and normalise detail measures
%data_sub1_01_norm = data_sub1_01/mean_max;
%data_sub1_02_norm = data_sub1_02/mean_max;
%data_sub1_03_norm = data_sub1_03/mean_max;
%data_sub1_04_norm = data_sub1_04/mean_max;
%data_sub1_05_norm = data_sub1_05/mean_max;
%data_sub1_06_norm = data_sub1_06/mean_max;
%data_sub1_07_norm = data_sub1_07/mean_max;

