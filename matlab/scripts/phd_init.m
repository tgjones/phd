% This script simply sets up my Matlab PhD paths

% Current directory will be the scripts directory so cd..
cd ..

% Add the USB key paths
addpath(strcat(pwd, '/scripts'));
addpath(strcat(pwd, '/scripts/archive'));
addpath(strcat(pwd, '/scripts/thesis_results'));
addpath(strcat(pwd, '/functions'));
addpath(strcat(pwd, '/functions/archive'));
addpath(strcat(pwd, '/functions/3rdparty'));
addpath(strcat(pwd, '/functions/3rdparty/gonzalez'));
addpath(strcat(pwd, '/functions/3rdparty/kovesi'));