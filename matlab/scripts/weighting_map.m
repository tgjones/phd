block_size=200;
fft_circle_mask = false(block_size,block_size);
for m = 1:block_size
    for n = 1:block_size
        r = sqrt(abs(m-block_size/2)^2+abs(n-block_size/2)^2);
        if r < 0.75*(block_size/2)
            fft_circle_mask(m,n) = true;
        end
    end
end
fft_mask_radius=0.75;
w = zeros(block_size,block_size);
centre = [floor(block_size/2), floor(block_size/2)];
for m = 1:block_size
    for n = 1:block_size
        %w(m,n) = (min([abs(m-centre(1)) abs(n-centre(2))]) + sqrt((m-centre(1))^2 + (n-centre(2))^2))^2; 
        w(m,n) = fft_mask_radius*sqrt((m-centre(1))^2 + (n-centre(2))^2)^2;
    end
end
