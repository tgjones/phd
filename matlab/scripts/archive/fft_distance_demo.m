close all; clear all;

I1 = imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/originals/Subject1/01_Subject1_0km_140610_0001.tif');
I2 = imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/originals/Subject1/02_Subject1_55km_190710_0001.tif');

gridWidth = 200;
gridHeight = 200;

data1 = fft_grid(I1,gridWidth,gridHeight);
data2 = fft_grid(I2,gridWidth,gridHeight);