clear all; close all;

% Read image
I = imread('C:\Research\Imagery\TimeStudy\Subject1\04_Subject1_35km_060910_0001.tif'); I = I(1080:1580,1550:1850);

% Generate demo images
Ig = imfilter(I,fspecial('gaussian',100));
Iea = edge(im2bw(Ig,graythresh(Ig)), 'sobel', 0.5);
Ith = imtophat(Ig,strel('disk',10));
Ieb = edge(im2bw(Ith,graythresh(Ith)), 'sobel', 0.5);

% Display Top Hat image line
figure;
subplot(1,4,1); imshow(I,[]); title('Source');
subplot(1,4,2); imshow(Ig,[]); title('Gaussian');
subplot(1,4,3); imshow(Ith,[]); title('Top Hat');
subplot(1,4,4); imshow(Ieb,[]); title('Sobel (thresh: 0.1)');

% Display non Top Hat image line
figure;
subplot(1,3,1); imshow(I,[]); title('Source');
subplot(1,3,2); imshow(Ig,[]); title('Gaussian');
subplot(1,3,3); imshow(Iea,[]); title('Sobel (thresh: 0.1)');

