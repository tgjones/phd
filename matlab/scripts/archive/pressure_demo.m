clear all;
close all;

% Read reference image
R = imread('C:\Research\Data\Imagery\FFS100_new\FFS039.jpg');

% Plot R and P
subplot(2,3,1); imshow(R,[]); title('Reference (R)');

P = estimate_pressure_map(R,5,20,240);
subplot(2,3,2); imshow(P,[]); title('estimate\_pressure\_map(R, 5, 20, 240)');

P = estimate_pressure_map(R,4.5,15,240);
subplot(2,3,3); imshow(P,[]); title('estimate\_pressure\_map(R, 4.5, 15, 240)');

P = estimate_pressure_map(R,4,15,240);
subplot(2,3,4); imshow(P,[]); title('estimate\_pressure\_map(R, 4, 15, 240)');

P = estimate_pressure_map(R,3,10,240);
subplot(2,3,5); imshow(P,[]); title('estimate\_pressure\_map(R, 3, 10, 240)');

P = estimate_pressure_map(R,2.5,10,240);
subplot(2,3,6); imshow(P,[]); title('estimate\_pressure\_map(R, 2.5, 10, 240)');