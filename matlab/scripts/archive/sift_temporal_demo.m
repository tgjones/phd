close all; clear all;

% Read images
T0a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/01_Subject1_0km_140610_0001.png'));
T0b = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/01_Subject1_0km_140610_0002.png'));
T1a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/02_Subject1_55km_190710_0001.png'));
T1b = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/02_Subject1_55km_190710_0002.png'));
T2a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/03_Subject1_32km_020810_0001.png'));
T2b = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/03_Subject1_32km_020810_0002.png'));
T3a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/04_Subject1_35km_060910_0001.png'));
T3b = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/04_Subject1_35km_060910_0002.png'));
T4a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/05_Subject1_10km_200910_0001.png'));
T4b = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/05_Subject1_10km_200910_0002.png'));
T5a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/06_Subject1_12km_041010_0001.png'));
T5b = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/06_Subject1_12km_041010_0002.png'));
T6a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/07_Subject1_35km_181010_0001.png'));
T6b = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/07_Subject1_35km_181010_0002.png'));

% Extract SIFT features             
[T0a_frames,T0a_descriptors] = vl_sift(im2single(T0a));
[T0b_frames,T0b_descriptors] = vl_sift(im2single(T0b));
[T1a_frames,T1a_descriptors] = vl_sift(im2single(T1a));
[T1b_frames,T1b_descriptors] = vl_sift(im2single(T1b));
[T2a_frames,T2a_descriptors] = vl_sift(im2single(T2a));
[T2b_frames,T2b_descriptors] = vl_sift(im2single(T2b));
[T3a_frames,T3a_descriptors] = vl_sift(im2single(T3a));
[T3b_frames,T3b_descriptors] = vl_sift(im2single(T3b));
[T4a_frames,T4a_descriptors] = vl_sift(im2single(T4a));
[T4b_frames,T4b_descriptors] = vl_sift(im2single(T4b));
[T5a_frames,T5a_descriptors] = vl_sift(im2single(T5a));
[T5b_frames,T5b_descriptors] = vl_sift(im2single(T5b));
[T6a_frames,T6a_descriptors] = vl_sift(im2single(T6a));
[T6b_frames,T6b_descriptors] = vl_sift(im2single(T6b));

% Do simple matching along the T dimension for 'A' prints
T0T1a_matches = vl_ubcmatch(T0a_descriptors,T1a_descriptors);
T1T2a_matches = vl_ubcmatch(T1a_descriptors,T2a_descriptors);
T2T3a_matches = vl_ubcmatch(T2a_descriptors,T3a_descriptors);
T3T4a_matches = vl_ubcmatch(T3a_descriptors,T4a_descriptors);
T4T5a_matches = vl_ubcmatch(T4a_descriptors,T5a_descriptors);
T5T6a_matches = vl_ubcmatch(T5a_descriptors,T6a_descriptors);

% Do simple matching along the T dimension for 'A' prints
T0T1b_matches = vl_ubcmatch(T0b_descriptors,T1b_descriptors);
T1T2b_matches = vl_ubcmatch(T1b_descriptors,T2b_descriptors);
T2T3b_matches = vl_ubcmatch(T2b_descriptors,T3b_descriptors);
T3T4b_matches = vl_ubcmatch(T3b_descriptors,T4b_descriptors);
T4T5b_matches = vl_ubcmatch(T4b_descriptors,T5b_descriptors);
T5T6b_matches = vl_ubcmatch(T5b_descriptors,T6b_descriptors);

% Prune frames from matches for T data
T0T1a_T0a_matched_frames = T0a_frames(:,T0T1a_matches(1,:));
T0T1a_T1a_matched_frames = T1a_frames(:,T0T1a_matches(2,:));
T0T1b_T0b_matched_frames = T0b_frames(:,T0T1b_matches(1,:));
T0T1b_T1b_matched_frames = T1b_frames(:,T0T1b_matches(2,:));
T1T2a_T1a_matched_frames = T1a_frames(:,T1T2a_matches(1,:));
T1T2a_T2a_matched_frames = T2a_frames(:,T1T2a_matches(2,:));
T1T2b_T1b_matched_frames = T1b_frames(:,T1T2b_matches(1,:));
T1T2b_T2b_matched_frames = T2b_frames(:,T1T2b_matches(2,:));
T2T3a_T2a_matched_frames = T2a_frames(:,T2T3a_matches(1,:));
T2T3a_T3a_matched_frames = T3a_frames(:,T2T3a_matches(2,:));
T2T3b_T2b_matched_frames = T2b_frames(:,T2T3b_matches(1,:));
T2T3b_T3b_matched_frames = T3b_frames(:,T2T3b_matches(2,:));
T3T4a_T3a_matched_frames = T3a_frames(:,T3T4a_matches(1,:));
T3T4a_T4a_matched_frames = T4a_frames(:,T3T4a_matches(2,:));
T3T4b_T3b_matched_frames = T3b_frames(:,T3T4b_matches(1,:));
T3T4b_T4b_matched_frames = T4b_frames(:,T3T4b_matches(2,:));
T4T5a_T4a_matched_frames = T4a_frames(:,T4T5a_matches(1,:));
T4T5a_T5a_matched_frames = T5a_frames(:,T4T5a_matches(2,:));
T4T5b_T4b_matched_frames = T4b_frames(:,T4T5b_matches(1,:));
T4T5b_T5b_matched_frames = T5b_frames(:,T4T5b_matches(2,:));
T5T6a_T5a_matched_frames = T5a_frames(:,T5T6a_matches(1,:));
T5T6a_T6a_matched_frames = T6a_frames(:,T5T6a_matches(2,:));
T5T6b_T5b_matched_frames = T5b_frames(:,T5T6b_matches(1,:));
T5T6b_T6b_matched_frames = T6b_frames(:,T5T6b_matches(2,:));

% Subtract to get displacement values
T0T1a_displacement = T0T1a_T0a_matched_frames - T0T1a_T1a_matched_frames;
T0T1b_displacement = T0T1b_T0b_matched_frames - T0T1a_T1a_matched_frames;
T1T2a_displacement = T1T2a_T1a_matched_frames - T1T2a_T2a_matched_frames;
T1T2b_displacement = T1T2b_T1b_matched_frames - T1T2a_T2a_matched_frames;

% ...

% Calculate theta and r

% Do simple intra-print matching between 'A' and 'B' prints
T0ab_matches = vl_ubcmatch(T0a_descriptors,T0b_descriptors);
T1ab_matches = vl_ubcmatch(T1a_descriptors,T1b_descriptors);
T2ab_matches = vl_ubcmatch(T2a_descriptors,T2b_descriptors);
T3ab_matches = vl_ubcmatch(T3a_descriptors,T3b_descriptors);
T4ab_matches = vl_ubcmatch(T4a_descriptors,T4b_descriptors);
T5ab_matches = vl_ubcmatch(T5a_descriptors,T5b_descriptors);
T6ab_matches = vl_ubcmatch(T6a_descriptors,T6b_descriptors);

% Prune frames from matches for 'A'/'B' data
T0ab_a_matched_frames = T0a_frames(:,T0ab_matches);
T0ab_b_matched_frames = T0b_frames(:,T0ab_matches);
T1ab_a_matched_frames = T1a_frames(:,T1ab_matches);
T1ab_b_matched_frames = T1b_frames(:,T1ab_matches);
T2ab_a_matched_frames = T2a_frames(:,T2ab_matches);
T2ab_b_matched_frames = T2b_frames(:,T2ab_matches);
T3ab_a_matched_frames = T3a_frames(:,T3ab_matches);
T3ab_b_matched_frames = T3b_frames(:,T3ab_matches);
T4ab_a_matched_frames = T4a_frames(:,T4ab_matches);
T4ab_b_matched_frames = T4b_frames(:,T4ab_matches);
T5ab_a_matched_frames = T5a_frames(:,T5ab_matches);
T5ab_b_matched_frames = T5b_frames(:,T5ab_matches);
T6ab_a_matched_frames = T6a_frames(:,T6ab_matches);
T6ab_b_matched_frames = T6b_frames(:,T6ab_matches);

% Subtract for displacement values

% ...

% Calculate theta and r
