clear all; close all;

% Read in some demo images
I0 = imread('C:\Research\Imagery\TimeStudy\Subject1\Subject1_0km_140610_0001.tif');
I1 = imread('C:\Research\Imagery\TimeStudy\Subject1\Subject1_55km_190710_0001.tif');

% Get image dimensions
width0  = size(I0, 2);
height0 = size(I0, 1);
width1  = size(I1, 2);
height1 = size(I1, 1);

% Specify number of blocks
blocksX = 10;
blocksY = 20;

% Compute block dimensions
blockWidth0  = round(width0 / blocksX);
blockHeight0 = round(height0 / blocksY);
blockWidth1  = round(width1 / blocksX);
blockHeight1 = round(height0 / blocksY);

% Create figures
h0 = figure;
a = axes;
set(a,'xticklabel',[]);
set(a,'yticklabel',[]);
h1 = figure;
a = axes;
set(a,'xticklabel',[]);
set(a,'yticklabel',[]);

% Iteratate through blocks and draw subplot
for blockRow = 1 : blocksY
    for blockCol = 1 : blocksX
   
        % Get the block image data
        block0 = I0((blockRow-1)*blockHeight0+1:blockRow*blockHeight0+1, ...
            (blockCol-1)*blockWidth0+1:blockCol*blockWidth0+1);
        block1 = I1((blockRow-1)*blockHeight1+1:blockRow*blockHeight0+1, ...
            (blockCol-1)*blockWidth1+1:blockCol*blockWidth1+1);
        
        % Do sobel edge detection on block
        [edges0,thresh0,gv0,gh0] = edge(im2bw(block0, graythresh(block0)), ...
            'sobel', 'nothinning');
        [edges1,thresh1,gv1,gh1] = edge(im2bw(block1, graythresh(block1)), ...
            'sobel', 'nothinning');
        
        % Compute edge angles
        angles0_rad = atan2(gh0,gv0);
        angles1_rad = atan2(gh1,gv1);
        
        % Convert to degrees for simplicity
        angles0_deg = angles0_rad .* (180/pi);
        angles1_deg = angles1_rad .* (180/pi);
        
        % Draw block histograms for image 0
        figure(h0);
        subplot(blocksY,blocksX,((blockRow-1)*blocksX)+blockCol); 
        hist(angles0_deg(angles0_deg>0),360);

        % Draw block histograms for image 1
        figure(h1);
        subplot(blocksY,blocksX,((blockRow-1)*blocksX)+blockCol); 
        hist(angles1_deg(angles1_deg>0),360);
    end
end