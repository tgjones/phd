clear all; close all;

% Read in some demo images
I0 = imread('C:\Research\Imagery\TimeStudy\Subject1\01_Subject1_0km_140610_0001.tif');
I1 = imread('C:\Research\Imagery\TimeStudy\Subject1\07_Subject1_35km_181010_0001.tif');

% Get image subset
I0 = I0(1200:1700,1700:2000);
I1 = I1(1100:1600,1600:1900);

% Get Canny edge images (using graythresh reduces noise)
canny0 = edge(im2bw(I0, graythresh(I0)), 'canny');
canny1 = edge(im2bw(I1, graythresh(I1)), 'canny');

% Show canny comparison (image subset)
figure;
subplot(1,2,1); imshow(canny0); title('New (Canny)');
subplot(1,2,2); imshow(canny1); title('Worn (Canny)');

% Get Sobel edge images (using graythresh reduces noise)
[sobel0,thresh0,gv0,gh0] = edge(im2bw(I0, graythresh(I0)), 'sobel', 'nothinning');
[sobel1,thresh1,gv1,gh1] = edge(im2bw(I1, graythresh(I1)), 'sobel', 'nothinning');

% Get Sobel edge images (using graythresh reduces noise) (thinning)
sobel0thin = edge(im2bw(I0, graythresh(I0)), 'sobel');
sobel1thin = edge(im2bw(I1, graythresh(I1)), 'sobel');

% Show sobel comparison (no thinning, image subset)
figure;
subplot(1,2,1); imshow(sobel0); title('New (Sobel, no thinning)');
subplot(1,2,2); imshow(sobel1); title('Worn (Sobel, no thinning)');

% Show sobel comparison (image subset)
figure;
subplot(1,2,1); imshow(sobel0thin); title('New (Sobel, with thinning)');
subplot(1,2,2); imshow(sobel1thin); title('Worn (Sobel, with thinning)');

% Calculate edge angles
angles0_rad = atan2(gh0, gv0);
angles1_rad = atan2(gh1, gv1);

% Eradicate negative angles
angles0_rad = angles0_rad + (((sobel0 > 0) & (angles0_rad <= 0)) * pi);
angles1_rad = angles1_rad + (((sobel1 > 0) & (angles1_rad <= 0)) * pi);

% Convert to degrees for simplicity
angles0_deg = angles0_rad .* (180/pi);
angles1_deg = angles1_rad .* (180/pi);

% Show angle images (image subset)
figure;
subplot(1,2,1); imshow(angles0_rad,[]); title ('New (Sobel Combined Angles)');
subplot(1,2,2); imshow(angles1_rad,[]); title ('Worn (Sobel Combined Angles)');

% Show histograms for angle images (entire image)
figure;
subplot(1,2,1); hist(angles0_deg(angles0_deg>0)); title('New (Angle Histogram, angles > 0)');
subplot(1,2,2); hist(angles1_deg(angles1_deg>0)); title('Worn (Angle Histogram, angles > 0)');
