clear all; close all;

% Read in some demo images and get subsets
I0 = imread('C:\Research\Imagery\TimeStudy\Subject1\01_Subject1_0km_140610_0001.tif');  I0 = I0(1200:1700,1700:2000); 
I1 = imread('C:\Research\Imagery\TimeStudy\Subject1\02_Subject1_55km_190710_0001.tif'); I1 = I1(1100:1600,1750:2050); 
I2 = imread('C:\Research\Imagery\TimeStudy\Subject1\03_Subject1_32km_020810_0002.tif'); I2 = I2(950:1450,1600:1900); 
I3 = imread('C:\Research\Imagery\TimeStudy\Subject1\04_Subject1_35km_060910_0001.tif'); I3 = I3(1080:1580,1550:1850);
I4 = imread('C:\Research\Imagery\TimeStudy\Subject1\05_Subject1_10km_200910_0001.tif'); I4 = I4(1050:1550,1800:2100);
I5 = imread('C:\Research\Imagery\TimeStudy\Subject1\06_Subject1_12km_041010_0001.tif'); I5 = I5(1100:1600,1650:1950);
I6 = imread('C:\Research\Imagery\TimeStudy\Subject1\07_Subject1_35km_181010_0001.tif'); I6 = I6(1100:1600,1600:1900);

% Loop variabls
sigma = 0;
sigma_step = 10;
sigma_max = 100;
sobel_thresh = 0.5;

while(sigma <= sigma_max)
    
    figure;
    
    % Smooth images on all passes except the first
    if (sigma > 0)
        f = fspecial('gaussian',sigma);
        I0 = imfilter(I0,f);
        I1 = imfilter(I1,f);
        I2 = imfilter(I2,f);
        I3 = imfilter(I3,f);
        I4 = imfilter(I4,f);
        I5 = imfilter(I5,f);
        I6 = imfilter(I6,f);
    end
    
    % Increment sigma for next run
    sigma = sigma+sigma_step;

    % Show images in subplot
    subplot(3,7,1); imshow(I0,[]); title('14/06/10, 0km');
    subplot(3,7,2); imshow(I1,[]); title('19/07/10, 55km');
    subplot(3,7,3); imshow(I2,[]); title('02/08/10, 87km');
    subplot(3,7,4); imshow(I3,[]); title('06/09/10, 122km');
    subplot(3,7,5); imshow(I4,[]); title('20/09/10, 132km');
    subplot(3,7,6); imshow(I5,[]); title('04/10/10, 144km');
    subplot(3,7,7); imshow(I6,[]); title('18/10/10, 179km');

    % Display Sobel with thinning
    subplot(3,7,8);  imshow(edge(im2bw(I0, graythresh(I0)), 'sobel', sobel_thresh));
    subplot(3,7,9);  imshow(edge(im2bw(I1, graythresh(I1)), 'sobel', sobel_thresh));
    subplot(3,7,10); imshow(edge(im2bw(I2, graythresh(I2)), 'sobel', sobel_thresh));
    subplot(3,7,11); imshow(edge(im2bw(I3, graythresh(I3)), 'sobel', sobel_thresh));
    subplot(3,7,12); imshow(edge(im2bw(I4, graythresh(I4)), 'sobel', sobel_thresh));
    subplot(3,7,13); imshow(edge(im2bw(I5, graythresh(I5)), 'sobel', sobel_thresh));
    subplot(3,7,14); imshow(edge(im2bw(I6, graythresh(I6)), 'sobel', sobel_thresh));

    % Do Sobel without thinning for algorithm
    [sobel0,thresh0,gv0,gh0] = edge(im2bw(I0, graythresh(I0)), 'sobel', sobel_thresh, 'nothinning');
    [sobel1,thresh1,gv1,gh1] = edge(im2bw(I1, graythresh(I1)), 'sobel', sobel_thresh, 'nothinning');
    [sobel2,thresh2,gv2,gh2] = edge(im2bw(I2, graythresh(I2)), 'sobel', sobel_thresh, 'nothinning');
    [sobel3,thresh3,gv3,gh3] = edge(im2bw(I3, graythresh(I3)), 'sobel', sobel_thresh, 'nothinning');
    [sobel4,thresh4,gv4,gh4] = edge(im2bw(I4, graythresh(I4)), 'sobel', sobel_thresh, 'nothinning');
    [sobel5,thresh5,gv5,gh5] = edge(im2bw(I5, graythresh(I5)), 'sobel', sobel_thresh, 'nothinning');
    [sobel6,thresh6,gv6,gh6] = edge(im2bw(I6, graythresh(I6)), 'sobel', sobel_thresh, 'nothinning');

    % Compute edge angles
    angles0_rad = atan2(gh0, gv0);
    angles1_rad = atan2(gh1, gv1);
    angles2_rad = atan2(gh2, gv2);
    angles3_rad = atan2(gh3, gv3);
    angles4_rad = atan2(gh4, gv4);
    angles5_rad = atan2(gh5, gv5);
    angles6_rad = atan2(gh6, gv6);

    % Eradicate negative angles
    angles0_rad = angles0_rad + (((sobel0 > 0) & (angles0_rad <= 0)) * pi);
    angles1_rad = angles1_rad + (((sobel1 > 0) & (angles1_rad <= 0)) * pi);
    angles2_rad = angles2_rad + (((sobel2 > 0) & (angles2_rad <= 0)) * pi);
    angles3_rad = angles3_rad + (((sobel3 > 0) & (angles3_rad <= 0)) * pi);
    angles4_rad = angles4_rad + (((sobel4 > 0) & (angles4_rad <= 0)) * pi);
    angles5_rad = angles5_rad + (((sobel5 > 0) & (angles5_rad <= 0)) * pi);
    angles6_rad = angles6_rad + (((sobel6 > 0) & (angles6_rad <= 0)) * pi);

    % Convert to degrees for simplicity
    angles0_deg = angles0_rad .* (180/pi);
    angles1_deg = angles1_rad .* (180/pi);
    angles2_deg = angles2_rad .* (180/pi);
    angles3_deg = angles3_rad .* (180/pi);
    angles4_deg = angles4_rad .* (180/pi);
    angles5_deg = angles5_rad .* (180/pi);
    angles6_deg = angles6_rad .* (180/pi);

    % Display histograms
    subplot(3,7,15); hist(angles0_deg(angles0_deg>0));
    subplot(3,7,16); hist(angles1_deg(angles1_deg>0));
    subplot(3,7,17); hist(angles2_deg(angles2_deg>0));
    subplot(3,7,18); hist(angles3_deg(angles3_deg>0));
    subplot(3,7,19); hist(angles4_deg(angles4_deg>0));
    subplot(3,7,20); hist(angles5_deg(angles5_deg>0));
    subplot(3,7,21); hist(angles6_deg(angles6_deg>0));

end