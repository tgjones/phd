clear all;
close all;

% Read reference image
R = imread('C:\Research\Imagery\FFS100_new\FFS039.jpg');

% Add some noise for query image
T1 = imnoise(R,'gaussian');

% Extract SIFT keypoints
[fR, dR]   = vl_sift(im2single(R));
[fT1, dT1] = vl_sift(im2single(T1));

% Perform UBC descriptor match to establish unique matches based on
% descriptor information, and elimate the majority of false positives.
m = vl_ubcmatch(dR, dT1); % TODO: Tweak tolerance (default 1.5)

% Extract matching frames and descriptors from indices in m
mfR  = fR(:,m(1,:));
mdR  = dR(:,m(1,:));
mfT1 = fT1(:,m(2,:));
mdT1 = dT1(:,m(2,:));

% Extract displacement information for matched frames
D = mfT1 - mfR;

% Perform frame outlier cull, based on custom minimum value thresholds.
idx = (D(1,:) < 2) & (D(1,:) > -2) ... % X (assume coordinate system is the same)
    & (D(2,:) < 2) & (D(2,:) > -2) ... % Y (assume coordinate system is the same)
    & (D(3,:) < 3) & (D(3,:) > -3) ... % Scale
    & (D(4,:) < 1) & (D(4,:) > -1);    % Orientation
D = D(:,idx);
mfT1 = mfT1(:,idx);

% Show source images
figure; 
subplot(1,2,1); imshow(R);  title('Reference (R)');
subplot(1,2,2); imshow(T1); title('Query Time 1 (T1)');

% Show source SIFT frames
figure;
subplot(1,2,1); imshow(R);  title('R SIFT frames');  vl_plotframe(fR);
subplot(1,2,2); imshow(T1); title('T1 SIFT frames'); vl_plotframe(fT1);

% Show matched SIFT frames
figure;
subplot(1,2,1); imshow(R);  title('R matches');  vl_plotframe(mfR);
subplot(1,2,2); imshow(T1); title('T1 matches'); vl_plotframe(mfT1);

% Visually plot displacement using green/red
figure;
range = sqrt(abs(max(D,[],2)) + abs(min(D,[],2)));

% X displacement (2D)
subplot(2,2,1); imshow(T1); title('X displacement'); 
for i = 1:size(D,2)
    r = abs(sqrt(D(1,i)))/range(1);
    h = vl_plotframe(mfT1(:,i));
    set(h, 'linewidth', 2);
    set(h, 'color', [ r 1-r 0]);
end

% Y displacement (2D)
subplot(2,2,2); imshow(T1); title('Y displacement'); 
for i = 1:size(D,2)
    r = abs(sqrt(D(2,i)))/range(2);
    h = vl_plotframe(mfT1(:,i));
    set(h, 'linewidth', 2);
    set(h, 'color', [ r 1-r 0]);
end

% Scale displacement (2D)
subplot(2,2,3); imshow(T1); title('Scale displacement'); 
for i = 1:size(D,2)
    r = abs(sqrt(D(3,i)))/range(3);
    h = vl_plotframe(mfT1(:,i));
    set(h, 'linewidth', 2);
    set(h, 'color', [ r 1-r 0]);
end

% Orientation displacement (2D)
subplot(2,2,4); imshow(T1); title('Orientation displacement'); 
for i = 1:size(D,2)
    r = abs(sqrt(D(4,i)))/range(4);
    h = vl_plotframe(mfT1(:,i));
    set(h, 'linewidth', 2);
    set(h, 'color', [ r 1-r 0]);
end

% Clear useless variables from the workspace
clear ans i h r range idx;