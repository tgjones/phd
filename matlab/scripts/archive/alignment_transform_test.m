close all;

Ia = imread('C:\Research\Matlab\images\reebok7\REE007S_l.JPG');
Ib = imread('C:\Research\Matlab\images\reebok7\stage2\scan0072.png');

figure; imshow(Ia);
figure; imshow(Ib);

% Convert to grayscale
if size(Ia,3) > 1
    Ia = rgb2gray(Ia);
end
if size(Ib,3) > 1
    Ib = rgb2gray(Ib);
end

% Extract features and plot            
fa = vl_sift(im2single(Ia)); figure; imshow(Ia); hold on; vl_plotframe(fa);
fb = vl_sift(im2single(Ib)); figure; imshow(Ib); hold on; vl_plotframe(fb);

% Manual input of alignment points
%[xa, ya, xb, yb] = input_alignment_points(Ia, Ib);
[input_points, base_points] = cpselect(Ia, Ib, 'Wait', true);

% Infer transform between input points
%tform = cp2tform([xa, ya], [xb, yb], 'nonreflective similarity');
tform = cp2tform(input_points, base_points, 'nonreflective similarity');
 
% Transform Ib's features to match Ia
[fb(1,:), fb(2,:)] = tformfwd(fliptform(tform), fb(1,:), fb(2,:));

% Plot transformed Ib frames onto Ia
figure; imshow(Ia); hold on; vl_plotframe(fb);