% A demo file showing the ability to draw a convex hull polygon around a
% reference shoeprint.

close all;
clear all;

I = imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/01_Subject1_0km_140610_0001.png');
h = convhull_poly(~im2bw(I,graythresh(I)));
%subplot(2,3,1);
imshow(I);
hold on;
plot(h(:,1),h(:,2));

I = imread('C:\Research\Matlab\images\reebok7\REE007S_l.JPG');
h = convhull_poly(~im2bw(I,graythresh(I)));
subplot(2,3,2);
imshow(I);
hold on;
plot(h(:,1),h(:,2));

I = imread('C:\Research\Matlab\images\FFS037.jpg');
h = convhull_poly(~im2bw(I,graythresh(I)));
subplot(2,3,3);
imshow(I);
hold on;
plot(h(:,1),h(:,2));

I = imread('C:\Research\Matlab\images\FFS059.jpg');
h = convhull_poly(~im2bw(I,graythresh(I)));
subplot(2,3,4);
imshow(I);
hold on;
plot(h(:,1),h(:,2));

I = imread('C:\Research\Matlab\images\FFS030.jpg');
h = convhull_poly(~im2bw(I,graythresh(I)));
subplot(2,3,5);
imshow(I);
hold on;
plot(h(:,1),h(:,2));

I = imread('C:\Research\Matlab\images\FFS065.jpg');
h = convhull_poly(~im2bw(I,graythresh(I)));
subplot(2,3,6);
imshow(I);
hold on;
plot(h(:,1),h(:,2));

clear I h