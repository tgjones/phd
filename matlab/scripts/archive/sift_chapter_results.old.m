% Read zoomed images from disk
zoom_samples

% Our samples are:
%
% 1. T0a/T1a for one time step from reference
% 2. T2a/T3a for one time step from worn
% 3. T0a/T3a for a large time step from reference
% 4. T1a/T4a for a large time step from worn

[T0a_f,T0a_d] = vl_sift(im2single(T0a));
[T1a_f,T1a_d] = vl_sift(im2single(T1a));
[T2a_f,T2a_d] = vl_sift(im2single(T2a));
[T3a_f,T3a_d] = vl_sift(im2single(T3a));
[T4a_f,T4a_d] = vl_sift(im2single(T4a));

[T0a_T1a_m,T0a_T1a_s] = vl_ubcmatch(T0a_d,T1a_d);
[T2a_T3a_m,T2a_T3a_s] = vl_ubcmatch(T2a_d,T3a_d);
[T0a_T3a_m,T0a_T3a_s] = vl_ubcmatch(T0a_d,T3a_d);
[T1a_T4a_m,T1a_T4a_s] = vl_ubcmatch(T1a_d,T4a_d);

% Threshold Euclidean Distance
%T0a_T1a_m = T0a_T1a_m(:,(T0a_T1a_s < mean(T0a_T1a_s)));
%T2a_T3a_m = T2a_T3a_m(:,(T2a_T3a_s < mean(T2a_T3a_s)));
%T0a_T3a_m = T0a_T3a_m(:,(T0a_T3a_s < mean(T0a_T3a_s)));
%T1a_T4a_m = T1a_T4a_m(:,(T1a_T4a_s < mean(T1a_T4a_s)));%

% Threshold Orientation
%thresh_o = 0;
%thresh_s = 0;

%THRESH = ~(T0a_f(4,T0a_T1a_m(1,:)) + thresh_s > T1a_f(4,T0a_T1a_m(2,:)) ...
%    & (T0a_f(4,T0a_T1a_m(1,:)) - thresh_s > T1a_f(4,T0a_T1a_m(2,:))));
%T0a_T1a_m = T0a_T1a_m(:,THRESH);

%THRESH = ~(T2a_f(4,T2a_T3a_m(1,:)) + thresh_s > T3a_f(4,T2a_T3a_m(2,:)) ...
%    & (T2a_f(4,T2a_T3a_m(1,:)) - thresh_s > T3a_f(4,T2a_T3a_m(2,:))));
% T2a_T3a_m = T2a_T3a_m(:,THRESH);
% 
% THRESH = ~(T0a_f(4,T0a_T3a_m(1,:)) + thresh_s > T3a_f(4,T0a_T3a_m(2,:)) ...
%     & (T0a_f(4,T0a_T3a_m(1,:)) - thresh_s > T3a_f(4,T0a_T3a_m(2,:))));
% T0a_T3a_m = T0a_T3a_m(:,THRESH);
% 
% THRESH = ~(T1a_f(4,T1a_T4a_m(1,:)) + thresh_s > T4a_f(4,T1a_T4a_m(2,:)) ...
%     & (T1a_f(4,T1a_T4a_m(1,:)) - thresh_s > T4a_f(4,T1a_T4a_m(2,:))));
% T1a_T4a_m = T1a_T4a_m(:,THRESH);
% 
% Compute Displacement

T0a_T1a_d = sqrt(abs(T0a_f(1,T0a_T1a_m(1,:))).^2 ...
    + abs(T1a_f(2,T0a_T1a_m(2,:))).^2);

T2a_T3a_d = sqrt(abs(T2a_f(1,T2a_T3a_m(1,:))).^2 ...
    + abs(T3a_f(2,T2a_T3a_m(2,:))).^2);

T0a_T3a_d = sqrt(abs(T0a_f(1,T0a_T3a_m(1,:))).^2 ...
    + abs(T3a_f(2,T0a_T3a_m(2,:))).^2);

T1a_T4a_d = sqrt(abs(T1a_f(1,T1a_T4a_m(1,:))).^2 ...
    + abs(T4a_f(2,T1a_T4a_m(2,:))).^2);

% Threshold Displacement
thresh_d = 1000;

T0a_T1a_m = T0a_T1a_m(T0a_T1a_d < thresh_d);
T2a_T3a_m = T2a_T3a_m(T2a_T3a_d < thresh_d);
T0a_T3a_m = T0a_T3a_m(T0a_T3a_d < thresh_d);
T1a_T4a_m = T1a_T4a_m(T1a_T4a_d < thresh_d);