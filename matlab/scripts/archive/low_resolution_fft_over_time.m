clear all; close all;

% Read in some demo images and get subsets
I0 = imread('C:\Research\Imagery\TimeStudy\Subject1\01_Subject1_0km_140610_0001.tif');  I0 = I0(1200:1700,1700:2000); 
I1 = imread('C:\Research\Imagery\TimeStudy\Subject1\02_Subject1_55km_190710_0001.tif'); I1 = I1(1100:1600,1750:2050); 
I2 = imread('C:\Research\Imagery\TimeStudy\Subject1\03_Subject1_32km_020810_0002.tif'); I2 = I2(950:1450,1600:1900); 
I3 = imread('C:\Research\Imagery\TimeStudy\Subject1\04_Subject1_35km_060910_0001.tif'); I3 = I3(1080:1580,1550:1850);
I4 = imread('C:\Research\Imagery\TimeStudy\Subject1\05_Subject1_10km_200910_0001.tif'); I4 = I4(1050:1550,1800:2100);
I5 = imread('C:\Research\Imagery\TimeStudy\Subject1\06_Subject1_12km_041010_0001.tif'); I5 = I5(1100:1600,1650:1950);
I6 = imread('C:\Research\Imagery\TimeStudy\Subject1\07_Subject1_35km_181010_0001.tif'); I6 = I6(1100:1600,1600:1900);

% Apply guassian blur
f = fspecial('gaussian',50);
I0 = imfilter(I0,f);
I1 = imfilter(I1,f);
I2 = imfilter(I2,f);
I3 = imfilter(I3,f);
I4 = imfilter(I4,f);
I5 = imfilter(I5,f);
I6 = imfilter(I6,f);

% Show images in subplot
subplot(3,7,1); imshow(I0,[]); title('14/06/10, 0km');
subplot(3,7,2); imshow(I1,[]); title('19/07/10, 55km');
subplot(3,7,3); imshow(I2,[]); title('02/08/10, 87km');
subplot(3,7,4); imshow(I3,[]); title('06/09/10, 122km');
subplot(3,7,5); imshow(I4,[]); title('20/09/10, 132km');
subplot(3,7,6); imshow(I5,[]); title('04/10/10, 144km');
subplot(3,7,7); imshow(I6,[]); title('18/10/10, 179km');

% Show FFTs in subplot
psdI0 = log(fftshift(abs(fft2(I0))).^2); subplot(3,7,8);  imshow(psdI0,[]);
psdI1 = log(fftshift(abs(fft2(I1))).^2); subplot(3,7,9);  imshow(psdI1,[]);
psdI2 = log(fftshift(abs(fft2(I2))).^2); subplot(3,7,10); imshow(psdI2,[]);
psdI3 = log(fftshift(abs(fft2(I3))).^2); subplot(3,7,11); imshow(psdI3,[]);
psdI4 = log(fftshift(abs(fft2(I4))).^2); subplot(3,7,12); imshow(psdI4,[]);
psdI5 = log(fftshift(abs(fft2(I5))).^2); subplot(3,7,13); imshow(psdI5,[]);
psdI6 = log(fftshift(abs(fft2(I6))).^2); subplot(3,7,14); imshow(psdI6,[]);

% Split images into quadrants
I0a = I0(1:size(I0,1)/2,1:size(I0,2)/2);
I0b = I0(1:size(I0,1)/2,size(I0,2)/2:size(I0,2));
I0c = I0(size(I0,1)/2:size(I0,1),1:size(I0,2)/2);
I0d = I0(size(I0,1)/2:size(I0,1),size(I0,2)/2:size(I0,2));

I1a = I1(1:size(I1,1)/2,1:size(I1,2)/2);
I1b = I1(1:size(I1,1)/2,size(I1,2)/2:size(I1,2));
I1c = I1(size(I1,1)/2:size(I1,1),1:size(I1,2)/2);
I1d = I1(size(I1,1)/2:size(I1,1),size(I1,2)/2:size(I1,2));

I2a = I2(1:size(I2,1)/2,1:size(I2,2)/2);
I2b = I2(1:size(I2,1)/2,size(I2,2)/2:size(I2,2));
I2c = I2(size(I2,1)/2:size(I2,1),1:size(I2,2)/2);
I2d = I2(size(I2,1)/2:size(I2,1),size(I2,2)/2:size(I2,2));

I3a = I3(1:size(I3,1)/2,1:size(I3,2)/2);
I3b = I3(1:size(I3,1)/2,size(I3,2)/2:size(I3,2));
I3c = I3(size(I3,1)/2:size(I3,1),1:size(I3,2)/2);
I3d = I3(size(I3,1)/2:size(I3,1),size(I3,2)/2:size(I3,2));

I4a = I4(1:size(I4,1)/2,1:size(I4,2)/2);
I4b = I4(1:size(I4,1)/2,size(I4,2)/2:size(I4,2));
I4c = I4(size(I4,1)/2:size(I4,1),1:size(I4,2)/2);
I4d = I4(size(I4,1)/2:size(I4,1),size(I4,2)/2:size(I4,2));

I5a = I5(1:size(I5,1)/2,1:size(I5,2)/2);
I5b = I5(1:size(I5,1)/2,size(I5,2)/2:size(I5,2));
I5c = I5(size(I5,1)/2:size(I5,1),1:size(I5,2)/2);
I5d = I5(size(I5,1)/2:size(I5,1),size(I5,2)/2:size(I5,2));

I6a = I6(1:size(I6,1)/2,1:size(I6,2)/2);
I6b = I6(1:size(I6,1)/2,size(I6,2)/2:size(I6,2));
I6c = I6(size(I6,1)/2:size(I6,1),1:size(I6,2)/2);
I6d = I6(size(I6,1)/2:size(I6,1),size(I6,2)/2:size(I6,2));

% Perform FFT on quadrants
psdI0a = log(fftshift(abs(fft2(I0a))).^2);
psdI0b = log(fftshift(abs(fft2(I0b))).^2);
psdI0c = log(fftshift(abs(fft2(I0c))).^2);
psdI0d = log(fftshift(abs(fft2(I0d))).^2);

psdI1a = log(fftshift(abs(fft2(I1a))).^2);
psdI1b = log(fftshift(abs(fft2(I1b))).^2);
psdI1c = log(fftshift(abs(fft2(I1c))).^2);
psdI1d = log(fftshift(abs(fft2(I1d))).^2);

psdI2a = log(fftshift(abs(fft2(I2a))).^2);
psdI2b = log(fftshift(abs(fft2(I2b))).^2);
psdI2c = log(fftshift(abs(fft2(I2c))).^2);
psdI2d = log(fftshift(abs(fft2(I2d))).^2);

psdI3a = log(fftshift(abs(fft2(I3a))).^2);
psdI3b = log(fftshift(abs(fft2(I3b))).^2);
psdI3c = log(fftshift(abs(fft2(I3c))).^2);
psdI3d = log(fftshift(abs(fft2(I3d))).^2);

psdI4a = log(fftshift(abs(fft2(I4a))).^2);
psdI4b = log(fftshift(abs(fft2(I4b))).^2);
psdI4c = log(fftshift(abs(fft2(I4c))).^2);
psdI4d = log(fftshift(abs(fft2(I4d))).^2);

psdI5a = log(fftshift(abs(fft2(I5a))).^2);
psdI5b = log(fftshift(abs(fft2(I5b))).^2);
psdI5c = log(fftshift(abs(fft2(I5c))).^2);
psdI5d = log(fftshift(abs(fft2(I5d))).^2);

psdI6a = log(fftshift(abs(fft2(I6a))).^2);
psdI6b = log(fftshift(abs(fft2(I6b))).^2);
psdI6c = log(fftshift(abs(fft2(I6c))).^2);
psdI6d = log(fftshift(abs(fft2(I6d))).^2);

% Join PSD quadrants back up and show them
qpsdI0 = [psdI0a psdI0b; psdI0c psdI0d]; subplot(3,7,15); imshow(qpsdI0,[]);
qpsdI1 = [psdI1a psdI1b; psdI1c psdI1d]; subplot(3,7,16); imshow(qpsdI1,[]);
qpsdI2 = [psdI2a psdI2b; psdI2c psdI2d]; subplot(3,7,17); imshow(qpsdI2,[]);
qpsdI3 = [psdI3a psdI3b; psdI3c psdI3d]; subplot(3,7,18); imshow(qpsdI3,[]);
qpsdI4 = [psdI4a psdI4b; psdI4c psdI4d]; subplot(3,7,19); imshow(qpsdI4,[]);
qpsdI5 = [psdI5a psdI5b; psdI5c psdI5d]; subplot(3,7,20); imshow(qpsdI5,[]);
qpsdI6 = [psdI6a psdI6b; psdI6c psdI6d]; subplot(3,7,21); imshow(qpsdI6,[]);
