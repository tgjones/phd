close all;

Ia = imread('C:\Research\Matlab\images\reebok7\REE007S_l.JPG');
Ib = imread('C:\Research\Matlab\images\reebok7\stage2\scan0072.png');

% Convert to grayscale
if size(Ia,3) > 1
    Ia = rgb2gray(Ia);
end
if size(Ib,3) > 1
    Ib = rgb2gray(Ib);
end

% Extract features             
[fa,da] = vl_sift(im2single(Ia));
[fb,db] = vl_sift(im2single(Ib));

% Do simple matching
matches = vl_ubcmatch(da,db);

% Get pivot points from image Ia
figure;
imshow(Ia);
title('Please input two points for Ia');
[xa,ya] = ginput(2);
close;

% Get pivot points from image Ib
figure;
imshow(Ib);
title('Please input two points for Ib');
[xb,yb] = ginput(2);
close;

% Get measurements from pivot points
[theta_a, ra] = measure_pivot_points(xa,ya);
[theta_b, rb] = measure_pivot_points(xb,yb);

% Plot the matches (no culling)
sift_ubcmatch_plot(Ia,Ib,fa,fb,matches);
title('Matches, no culling');

% Cull based on connector angle
figure;
m_tmp = sift_match_cull_connector_angle(fa,fb,ra,rb,matches,5);
sift_match_plot(Ia,Ib,fa,fb,m_tmp);
title('Matches, culling on connector angle (20)');

% Cull based on rotation and plot
%figure;
%m_tmp = sift_match_cull_rotation(fa,fb,matches,0.2);
%sift_ubcmatch_plot(Ia,Ib,fa,fb,m_tmp);
%title('Matches, culling on rotation (0.2 radians)');

% Cull based on scale and plot (is this working?)
%figure;
%m_tmp = sift_match_cull_scale(fa,fb,matches,0.2);
%sift_ubcmatch_plot(Ia,Ib,fa,fb,m_tmp);
%title('Matches, culling on scale (0.5)');

