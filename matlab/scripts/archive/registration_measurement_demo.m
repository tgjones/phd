clear all;
close all;

T0 = imread('C:\Research\Matlab\images\reebok7\REE007S_l.JPG');
Tt = imread('C:\Research\Matlab\images\reebok7\stage2\scan0072.png');

% Get two points from image T0
figure;
imshow(T0);
title('Please input two points');
[x1,y1] = ginput(2);
close;

% Get two points from image Tt
figure;
imshow(Tt);
title('Please input two points');
[x2,y2] = ginput(2);
close;

[theta1, r1] = measure_pivot_points(x1,y1);
[theta2, r2] = measure_pivot_points(x2,y2);

[similarity_measure, theta_difference, r_difference] = ...
    measure_twin_points_pair(theta1, theta2, r1, r2);
