clear all;
close all;

T0 = imread('C:\Research\Matlab\images\reebok7\REE007S_l.JPG');
Tt = imread('C:\Research\Matlab\images\reebok7\REE007S_l_rotated_scaled.JPG');

[T0f, T0d] = vl_sift(im2single(T0));
[Ttf, Ttd] = vl_sift(im2single(Tt));

[matches, scores] = vl_ubcmatch(T0d, Ttd);

%matches = sift_match_cull_scale(T0f, Ttf, matches, 0.8);
%matches = sift_match_cull_rotation(T0f, Ttf, matches, 3);

[best_tform,best_consensus_set,best_error] ...
    = ransac_spatial_matches(matches,T0f,Ttf,12,1000,30);

Tt_trans = imtransform(Tt,fliptform(best_tform));

subplot(1,3,1); imshow(T0);
subplot(1,3,2); imshow(Tt);
subplot(1,3,3); imshow(Tt_trans);

figure; imshow(T0);
figure; imshow(Tt_trans);

% Use pivot points to measure angles on current 6

% 2 stage process:
%   Get rough locality
%   Split into regions (quadrants) redo/reapply

% Reduce false-positives as much as possible for measuring of wear later