% My second displacement demo, this includes the use of the
% 'sift_wear_forward' command, so the release Mex file must be build and
% present on the the Matlab search path. Also a lot of the stuff from
% 'displacement_demo_1' has been bundled into the function
% 'sift_displacement' so that M file also needs to be on the path. 

clear all;
close all;

% Read reference image
T0 = imread('C:\Research\Matlab\images\FFS039.jpg');

% Read pressure map
P = imread('C:\Research\Matlab\images\P_01_512x512.png');

% Synthetically wear the shoe
Tt = shoe_wear_forward(T0,P,3); % 3 timeblocks = roughly 150k steps

% Calculate displacement figures
[D, T0f, Ttf, T0d, Ttd] = sift_transformation(T0,Tt);

% Calculate the mid-way Cartesian displacement measurement
% Same as: [Ttf(1,:)+(T0f(1,:)-Ttf(1,:))/2; Ttf(2,:)+(T0f(2,:)-Ttf(2,:))/2]
% Which is faster?!
mid = [median([T0f(1,:); Ttf(1,:)]); median([T0f(2,:); Ttf(2,:)])];

% Compute indices of mid-points that lie within the convex hull ROI
h = convhull_poly(~im2bw(T0,graythresh(T0)));
IN = inpolygon(mid(1,:), mid(2,:), h(:,1), h(:,2));

% Cut everything down to size using convex hull indices
D   = D(:,IN);
mid = mid(:,IN);
T0f = T0f(:,IN);
T0d = T0d(:,IN);
Ttf = Ttf(:,IN);
Ttd = Ttd(:,IN);

% Generate X and Y grid coordinates
[X,Y] = meshgrid(linspace(0,size(T0,1),size(T0,1)/8), linspace(0,size(T0,2),size(T0,2)/8));

% Calculate interpolated data for surface plots
Zr = griddata(mid(1,:), mid(2,:), D(1,:), X, Y);
Zs = griddata(mid(1,:), mid(2,:), D(2,:), X, Y);
Zo = griddata(mid(1,:), mid(2,:), D(3,:), X, Y);

% Show source images
figure; 
subplot(1,2,1); imshow(T0); title('Reference (T0)');
subplot(1,2,2); imshow(Tt); title('Worn Image (Tt)');

% Show matched SIFT frames
figure;
subplot(1,2,1); imshow(T0); title('T0 matches'); vl_plotframe(T0f);
subplot(1,2,2); imshow(Tt); title('Tt matches'); vl_plotframe(Ttf);

% Display 3D histogram of number of displacements across the print
figure;
hist3(mid', {0:size(T0,1)/32:size(T0,1) 0:size(T0,2)/32:size(T0,2)});
axis ij;
title('3D Displacement Histogram'); 

% Display surface plot of cartesian displacement
figure;
p = pcolor(X,Y,Zr); 
set(p,'EdgeColor','none');
axis ij;
axis([0 size(T0,2) 0 size(T0,1)]);
title('Cartesian displacement (r)'); 

% Display surface plot of scale displacement
figure;
p = pcolor(X,Y,Zs); 
set(p,'EdgeColor','none');
axis ij;
axis([0 size(T0,2) 0 size(T0,1)]);
title('Scale displacement'); 

% Display surface plot of mean displacement
figure; 
p = pcolor(X,Y,Zo);
set(p,'EdgeColor','none');
axis ij;
axis([0 size(T0,2) 0 size(T0,1)]);
title('Orientation displacement'); 

% Clear useless variables from the workspace
clear ans mid h;