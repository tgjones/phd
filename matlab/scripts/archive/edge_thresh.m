clear all; close all;

I = imread('C:\Research\Imagery\TimeStudy\Subject1\07_Subject1_35km_181010_0001.tif'); I = I(1100:1600,1600:1900);

for i = 1:10
   
    BW = edge(im2bw(I,graythresh(I)),'sobel',i/20);
    
    subplot(2,5,i); 
    imshow(BW);
    title(i/20);
    
end