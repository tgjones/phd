clear all;
close all;

% Read reference images
R1 = imread('C:\Research\Imagery\FFS100_new\FFS039.jpg');
R2 = imread('C:\Research\Imagery\FFS100_new\FFS003.jpg');

% Plot first print
figure(1);
subplot(2,2,1); imshow(R1); title('Reference');
W = simulate_wear(R1,50); subplot(2,2,2); 
imshow(W); title('50,000 steps');
W = simulate_wear(R1,200); subplot(2,2,3); 
imshow(W); title('200,000 steps');
W = simulate_wear(R1,500); subplot(2,2,4); 
imshow(W); title('500,000 steps');

% Plot second print
figure(2);
subplot(2,2,1); imshow(R2); title('Reference');
W = simulate_wear(R2,50); subplot(2,2,2); 
imshow(W); title('50,000 steps');
W = simulate_wear(R2,200); subplot(2,2,3); 
imshow(W); title('200,000 steps');
W = simulate_wear(R2,500); subplot(2,2,4); 
imshow(W); title('500,000 steps');

% Clear useless variables from the workspace
clear W R1 R2;