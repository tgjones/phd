% Load subject 1 ground truth
subject1_xl = zeros(25,18,7);
subject1_xl(:,:,1) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject1.xlsx','01 (Ref)','A1:R25');
subject1_xl(:,:,2) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject1.xlsx','02','A1:R25');
subject1_xl(:,:,3) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject1.xlsx','03','A1:R25');
subject1_xl(:,:,4) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject1.xlsx','04','A1:R25');
subject1_xl(:,:,5) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject1.xlsx','05','A1:R25');
subject1_xl(:,:,6) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject1.xlsx','06','A1:R25');
subject1_xl(:,:,7) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject1.xlsx','07','A1:R25');

% Load subject 2 ground truth
subject2_xl = zeros(25,18,7);
subject2_xl(:,:,1) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject2.xlsx','01 (Ref)','A1:R25');
subject2_xl(:,:,2) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject2.xlsx','02','A1:R25');
subject2_xl(:,:,3) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject2.xlsx','03','A1:R25');
subject2_xl(:,:,4) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject2.xlsx','04','A1:R25');
subject2_xl(:,:,5) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject2.xlsx','05','A1:R25');
subject2_xl(:,:,6) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject2.xlsx','06','A1:R25');
subject2_xl(:,:,7) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject2.xlsx','07','A1:R25');

% Load subject 3 ground truth
subject3_xl = zeros(25,18,7);
subject3_xl(:,:,1) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject3.xlsx','01 (Ref)','A1:R25');
subject3_xl(:,:,2) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject3.xlsx','02','A1:R25');
subject3_xl(:,:,3) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject3.xlsx','03','A1:R25');
subject3_xl(:,:,4) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject3.xlsx','04','A1:R25');
subject3_xl(:,:,5) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject3.xlsx','05','A1:R25');
subject3_xl(:,:,6) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject3.xlsx','06','A1:R25');
subject3_xl(:,:,7) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject3.xlsx','07','A1:R25');

% Load subject 3 ground truth
subject4_xl = zeros(25,18,5);
subject4_xl(:,:,1) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject4.xlsx','01 (Ref)','A1:R25');
subject4_xl(:,:,2) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject4.xlsx','02','A1:R25');
subject4_xl(:,:,3) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject4.xlsx','03','A1:R25');
subject4_xl(:,:,4) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject4.xlsx','04','A1:R25');
subject4_xl(:,:,5) = xlsread('C:\Documents and Settings\12441082\My Documents\My Dropbox\PhD\Ground Truth\Subject4.xlsx','05','A1:R25');

% Normalise all ground truth
subject1_xl = (subject1_xl-2)/3;
subject2_xl = (subject2_xl-2)/3;
subject3_xl = (subject3_xl-2)/3;
subject4_xl = (subject4_xl-2)/3;

% Re-zero zeros
subject1_xl(subject1_xl < 0) = 0;
subject2_xl(subject2_xl < 0) = 0;
subject3_xl(subject3_xl < 0) = 0;
subject4_xl(subject4_xl < 0) = 0;

% Describe function to fit to ground truth data, the function
func = fittype({'exp(-x)','1'},'coefficients',{'a','c'});

subject1_curves = cell(25,18);
subject2_curves = cell(25,18);
subject3_curves = cell(25,18);
subject4_curves = cell(25,18);

% Compute curves for subject 1
for row = 1:25
    for col = 1:18     
        subject1_curves{row,col} = fit([1;2;3;4;5;6;7],reshape(subject1_xl(row,col,:),7,1),func);
    end
end

% Compute curves for subject 2
for row = 1:25
    for col = 1:18     
        subject2_curves{row,col} = fit([1;2;3;4;5;6;7],reshape(subject2_xl(row,col,:),7,1),func);
    end
end

% Compute curves for subject 3
for row = 1:25
    for col = 1:18     
        subject3_curves{row,col} = fit([1;2;3;4;5;6;7],reshape(subject3_xl(row,col,:),7,1),func);
    end
end

% Compure curves for subject 4
for row = 1:25
    for col = 1:18
        subject4_curves{row,col} = fit([1;2;3;4;5],reshape(subject4_xl(row,col,:),5,1),func);
    end
end

clear func row col;