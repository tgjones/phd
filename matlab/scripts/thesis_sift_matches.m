T0a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/01_Subject1_0km_140610_0001.png'));
T1a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/02_Subject1_55km_190710_0001.png'));

[T0a_frames,T0a_descriptors] = vl_sift(im2single(T0a));
[T1a_frames,T1a_descriptors] = vl_sift(im2single(T1a));

m = vl_ubcmatch(T0a_descriptors,T1a_descriptors);

% Get random selection of matches
perm = randperm(size(m,2));

sift_match_plot(T0a,T1a,T0a_frames,T1a_frames,m(:,perm(1:200)));


% Bigger time gap
T4a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/04_Subject1_35km_060910_0001.png'));

[T4a_frames,T4a_descriptors] = vl_sift(im2single(T4a));

m = vl_ubcmatch(T0a_descriptors,T4a_descriptors);
perm = randperm(size(m,2));

sift_match_plot(T0a,T4a,T0a_frames,T4a_frames,m(:,perm(1:200)));


% Not reference image
m = vl_ubcmatch(T1a_descriptors,T4a_descriptors);
perm = randperm(size(m,2));

sift_match_plot(T1a,T4a,T1a_frames,T4a_frames,m(:,perm(1:200)));
