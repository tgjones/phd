block_size = 200;
mask = zeros(block_size,block_size);
for m = 1:block_size
    for n = 1:block_size
        r = sqrt(abs(m-block_size/2)^2+abs(n-block_size/2)^2);
        if r < 3*(block_size/8);
            mask(m,n) = true;
        end
    end
end
          