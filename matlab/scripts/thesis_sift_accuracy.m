T0a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/01_Subject1_0km_140610_0001.png'));
T1a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/02_Subject1_55km_190710_0001.png'));
T4a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/05_Subject1_10km_200910_0001.png'));

[T0a_frames,T0a_descriptors] = vl_sift(im2single(T0a));
[T1a_frames,T1a_descriptors] = vl_sift(im2single(T1a));
[T4a_frames,T4a_descriptors] = vl_sift(im2single(T4a));

m = vl_ubcmatch(T0a_descriptors,T1a_descriptors);
[T0a_a,T0a_o] = sift_match_accuracy(T0a_frames,T1a_frames,m,10,200); 

% Pair up matches based on pixel distance brackets
% Plot a bell curve (or not) 