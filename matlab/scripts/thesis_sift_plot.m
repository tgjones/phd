T0a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/01_Subject1_0km_140610_0001.png'));
T1a = rgb2gray(imread('/Users/Tim/Documents/PhD/Imagery/TimeStudy/pngs/Subject1/02_Subject1_55km_190710_0001.png'));

[T0a_frames,T0a_descriptors] = vl_sift(im2single(T0a));
[T1a_frames,T1a_descriptors] = vl_sift(im2single(T1a));

m = vl_ubcmatch(T0a_descriptors,T1a_descriptors);

imshow(T0a);
perm = randperm(size(T0a_frames,2)) ; 
sel = perm(1:200) ;
h1 = vl_plotframe(T0a_frames(:,sel)) ; 
set(h1,'color','r','linewidth',2) ;
h2 = vl_plotsiftdescriptor(T0a_descriptors(:,sel),T0a_frames(:,sel)) ;  
set(h2,'color','g') ;


imshow(T1a);
perm = randperm(size(T1a_frames,2)) ; 
sel = perm(1:1000) ;
h1 = vl_plotframe(T1a_frames(:,sel)) ; 
set(h1,'color','r','linewidth',1) ;
h2 = vl_plotsiftdescriptor(T1a_descriptors(:,sel),T0a_frames(:,sel)) ;  
set(h2,'color','g') ;
